import './styles/admin.scss';

(() => {
    'use strict'

    window.addEventListener('DOMContentLoaded', () => {
        document.querySelectorAll('form.xhr-form')
            .forEach(form => {
                form.addEventListener('submit', (evt) => {
                    evt.preventDefault();

                    form.querySelector('.alert').classList.add('d-none')
                    form.querySelector('button[type="submit"]').setAttribute('disabled', 'disabled')
                    form.querySelector('.alert-warning').classList.remove('d-none')

                    const xmlHttpRequest = new XMLHttpRequest()

                    xmlHttpRequest.addEventListener('load', (event) => {
                        if (200 === event.target.status) {
                            form.querySelector('.alert-warning').classList.add('d-none')
                            form.querySelector('.alert-success').classList.remove('d-none')
                        } else {
                            form.querySelector('.alert-warning').classList.add('d-none')
                            form.querySelector('.alert-danger').classList.remove('d-none')
                        }
                    })

                    xmlHttpRequest.open('POST', form.getAttribute('action'))
                    xmlHttpRequest.send(new FormData(form))
                })
            })
    })
})();
