import 'leaflet';

import './styles/map.scss';

(() => {
    'use strict'

    const container = document.querySelector('#map')
    if (container) {
        const map = L.map('map').setView([48.00809327013673, -3.128898715467308], 7)
        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map)

        const type = container.getAttribute('type');

        if (type.startsWith('input')) {
            const latitudeInput = document.querySelector("[id$='_latitude']")
            const longitudeInput = document.querySelector("[id$='_longitude']")

            const icon = L.icon({iconUrl: '/balise.webp'})

            let marker
            if (latitudeInput.value !== undefined && longitudeInput.value !== undefined) {
                marker = L.marker([latitudeInput.value, longitudeInput.value], { icon: icon}).addTo(map);
                let latLngs = [ marker.getLatLng() ]
                let markerBounds = L.latLngBounds(latLngs)
                map.fitBounds(markerBounds)
                map.setZoom(10)
            }

            if ('input-editable' === type) {
                map.on('click', function(e) {
                    if (marker !== undefined) {
                        map.removeLayer(marker);
                    }

                    marker = L.marker(e.latlng, { icon: icon}).addTo(map);
                    latitudeInput.value = e.latlng.lat
                    longitudeInput.value = e.latlng.lng
                });
            }
        }
    }
})();