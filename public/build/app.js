(self["webpackChunk"] = self["webpackChunk"] || []).push([["app"],{

/***/ "./assets/app.js":
/*!***********************!*\
  !*** ./assets/app.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! core-js/modules/es.array.for-each.js */ "./node_modules/core-js/modules/es.array.for-each.js");
__webpack_require__(/*! core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");
__webpack_require__(/*! core-js/modules/web.dom-collections.for-each.js */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
__webpack_require__(/*! core-js/modules/es.promise.js */ "./node_modules/core-js/modules/es.promise.js");
(function () {
  'use strict';

  var storedTheme = localStorage.getItem('theme');
  var getPreferredTheme = function getPreferredTheme() {
    if (storedTheme) {
      return storedTheme;
    }
    return window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
  };
  var setTheme = function setTheme(theme) {
    if (theme === 'auto' && window.matchMedia('(prefers-color-scheme: dark)').matches) {
      document.documentElement.setAttribute('data-bs-theme', 'dark');
    } else {
      document.documentElement.setAttribute('data-bs-theme', theme);
    }
  };
  setTheme(getPreferredTheme());
  var showActiveTheme = function showActiveTheme(theme) {
    var focus = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    var themeSwitcher = document.querySelector('#bd-theme');
    if (!themeSwitcher) {
      return;
    }
    var activeThemeIcon = document.querySelector('.theme-icon-active');
    var btnToActive = document.querySelector("[data-bs-theme-value=\"".concat(theme, "\"]"));
    var activeBtn = btnToActive.querySelector('i');
    document.querySelectorAll('[data-bs-theme-value]').forEach(function (element) {
      element.classList.remove('active');
      element.setAttribute('aria-pressed', 'false');
    });
    btnToActive.classList.add('active');
    btnToActive.setAttribute('aria-pressed', 'true');
    activeThemeIcon.innerHTML = activeBtn.outerHTML;
    if (focus) {
      themeSwitcher.focus();
    }
  };
  window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', function () {
    if (storedTheme !== 'light' || storedTheme !== 'dark') {
      setTheme(getPreferredTheme());
    }
  });
  window.addEventListener('DOMContentLoaded', function () {
    showActiveTheme(getPreferredTheme());
    document.querySelectorAll('[data-bs-theme-value]').forEach(function (toggle) {
      toggle.addEventListener('click', function () {
        var theme = toggle.getAttribute('data-bs-theme-value');
        localStorage.setItem('theme', theme);
        setTheme(theme);
        showActiveTheme(theme, true);
      });
    });
  });
  var removeElement = function removeElement(response, id) {
    if (200 === response.status) {
      document.querySelector('#' + id).remove();
    }
  };
  window.addEventListener('DOMContentLoaded', function () {
    document.querySelectorAll('.btn-js-action').forEach(function (btn) {
      btn.addEventListener('click', function (evt) {
        var href = btn.getAttribute('href');
        var id = btn.getAttribute('id');
        evt.preventDefault();
        fetch(href).then(function (response) {
          return removeElement(response, id);
        });
      });
    });
  });
})();

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendors-node_modules_core-js_internals_array-iteration_js-node_modules_core-js_internals_export_js","vendors-node_modules_core-js_modules_es_array_for-each_js-node_modules_core-js_modules_es_obj-d12f93"], () => (__webpack_exec__("./assets/app.js")));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLENBQUMsWUFBTTtFQUNILFlBQVk7O0VBRVosSUFBTUEsV0FBVyxHQUFHQyxZQUFZLENBQUNDLE9BQU8sQ0FBQyxPQUFPLENBQUM7RUFFakQsSUFBTUMsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFpQkEsQ0FBQSxFQUFTO0lBQzVCLElBQUlILFdBQVcsRUFBRTtNQUNiLE9BQU9BLFdBQVc7SUFDdEI7SUFFQSxPQUFPSSxNQUFNLENBQUNDLFVBQVUsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDQyxPQUFPLEdBQUcsTUFBTSxHQUFHLE9BQU87RUFDdkYsQ0FBQztFQUVELElBQU1DLFFBQVEsR0FBRyxTQUFYQSxRQUFRQSxDQUFhQyxLQUFLLEVBQUU7SUFDOUIsSUFBSUEsS0FBSyxLQUFLLE1BQU0sSUFBSUosTUFBTSxDQUFDQyxVQUFVLENBQUMsOEJBQThCLENBQUMsQ0FBQ0MsT0FBTyxFQUFFO01BQy9FRyxRQUFRLENBQUNDLGVBQWUsQ0FBQ0MsWUFBWSxDQUFDLGVBQWUsRUFBRSxNQUFNLENBQUM7SUFDbEUsQ0FBQyxNQUFNO01BQ0hGLFFBQVEsQ0FBQ0MsZUFBZSxDQUFDQyxZQUFZLENBQUMsZUFBZSxFQUFFSCxLQUFLLENBQUM7SUFDakU7RUFDSixDQUFDO0VBRURELFFBQVEsQ0FBQ0osaUJBQWlCLENBQUMsQ0FBQyxDQUFDO0VBRTdCLElBQU1TLGVBQWUsR0FBRyxTQUFsQkEsZUFBZUEsQ0FBSUosS0FBSyxFQUFvQjtJQUFBLElBQWxCSyxLQUFLLEdBQUFDLFNBQUEsQ0FBQUMsTUFBQSxRQUFBRCxTQUFBLFFBQUFFLFNBQUEsR0FBQUYsU0FBQSxNQUFHLEtBQUs7SUFDekMsSUFBTUcsYUFBYSxHQUFHUixRQUFRLENBQUNTLGFBQWEsQ0FBQyxXQUFXLENBQUM7SUFFekQsSUFBSSxDQUFDRCxhQUFhLEVBQUU7TUFDaEI7SUFDSjtJQUVBLElBQU1FLGVBQWUsR0FBR1YsUUFBUSxDQUFDUyxhQUFhLENBQUMsb0JBQW9CLENBQUM7SUFDcEUsSUFBTUUsV0FBVyxHQUFHWCxRQUFRLENBQUNTLGFBQWEsMkJBQUFHLE1BQUEsQ0FBMEJiLEtBQUssUUFBSSxDQUFDO0lBQzlFLElBQU1jLFNBQVMsR0FBR0YsV0FBVyxDQUFDRixhQUFhLENBQUMsR0FBRyxDQUFDO0lBRWhEVCxRQUFRLENBQUNjLGdCQUFnQixDQUFDLHVCQUF1QixDQUFDLENBQUNDLE9BQU8sQ0FBQyxVQUFBQyxPQUFPLEVBQUk7TUFDbEVBLE9BQU8sQ0FBQ0MsU0FBUyxDQUFDQyxNQUFNLENBQUMsUUFBUSxDQUFDO01BQ2xDRixPQUFPLENBQUNkLFlBQVksQ0FBQyxjQUFjLEVBQUUsT0FBTyxDQUFDO0lBQ2pELENBQUMsQ0FBQztJQUVGUyxXQUFXLENBQUNNLFNBQVMsQ0FBQ0UsR0FBRyxDQUFDLFFBQVEsQ0FBQztJQUNuQ1IsV0FBVyxDQUFDVCxZQUFZLENBQUMsY0FBYyxFQUFFLE1BQU0sQ0FBQztJQUNoRFEsZUFBZSxDQUFDVSxTQUFTLEdBQUdQLFNBQVMsQ0FBQ1EsU0FBUztJQUUvQyxJQUFJakIsS0FBSyxFQUFFO01BQ1BJLGFBQWEsQ0FBQ0osS0FBSyxDQUFDLENBQUM7SUFDekI7RUFDSixDQUFDO0VBRURULE1BQU0sQ0FBQ0MsVUFBVSxDQUFDLDhCQUE4QixDQUFDLENBQUMwQixnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsWUFBTTtJQUMvRSxJQUFJL0IsV0FBVyxLQUFLLE9BQU8sSUFBSUEsV0FBVyxLQUFLLE1BQU0sRUFBRTtNQUNuRE8sUUFBUSxDQUFDSixpQkFBaUIsQ0FBQyxDQUFDLENBQUM7SUFDakM7RUFDSixDQUFDLENBQUM7RUFFRkMsTUFBTSxDQUFDMkIsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUUsWUFBTTtJQUM5Q25CLGVBQWUsQ0FBQ1QsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO0lBRXBDTSxRQUFRLENBQUNjLGdCQUFnQixDQUFDLHVCQUF1QixDQUFDLENBQzdDQyxPQUFPLENBQUMsVUFBQVEsTUFBTSxFQUFJO01BQ2ZBLE1BQU0sQ0FBQ0QsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFlBQU07UUFDbkMsSUFBTXZCLEtBQUssR0FBR3dCLE1BQU0sQ0FBQ0MsWUFBWSxDQUFDLHFCQUFxQixDQUFDO1FBQ3hEaEMsWUFBWSxDQUFDaUMsT0FBTyxDQUFDLE9BQU8sRUFBRTFCLEtBQUssQ0FBQztRQUNwQ0QsUUFBUSxDQUFDQyxLQUFLLENBQUM7UUFDZkksZUFBZSxDQUFDSixLQUFLLEVBQUUsSUFBSSxDQUFDO01BQ2hDLENBQUMsQ0FBQztJQUNOLENBQUMsQ0FBQztFQUNWLENBQUMsQ0FBQztFQUVGLElBQU0yQixhQUFhLEdBQUcsU0FBaEJBLGFBQWFBLENBQWFDLFFBQVEsRUFBRUMsRUFBRSxFQUFFO0lBQzFDLElBQUksR0FBRyxLQUFLRCxRQUFRLENBQUNFLE1BQU0sRUFBRTtNQUN6QjdCLFFBQVEsQ0FBQ1MsYUFBYSxDQUFDLEdBQUcsR0FBR21CLEVBQUUsQ0FBQyxDQUFDVixNQUFNLENBQUMsQ0FBQztJQUM3QztFQUNKLENBQUM7RUFFRHZCLE1BQU0sQ0FBQzJCLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFLFlBQU07SUFDOUN0QixRQUFRLENBQUNjLGdCQUFnQixDQUFDLGdCQUFnQixDQUFDLENBQ3RDQyxPQUFPLENBQUMsVUFBQWUsR0FBRyxFQUFJO01BQ1pBLEdBQUcsQ0FBQ1IsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFVBQUNTLEdBQUcsRUFBSztRQUNuQyxJQUFNQyxJQUFJLEdBQUdGLEdBQUcsQ0FBQ04sWUFBWSxDQUFDLE1BQU0sQ0FBQztRQUNyQyxJQUFNSSxFQUFFLEdBQUdFLEdBQUcsQ0FBQ04sWUFBWSxDQUFDLElBQUksQ0FBQztRQUVqQ08sR0FBRyxDQUFDRSxjQUFjLENBQUMsQ0FBQztRQUVwQkMsS0FBSyxDQUFDRixJQUFJLENBQUMsQ0FDTkcsSUFBSSxDQUFDLFVBQUFSLFFBQVE7VUFBQSxPQUFJRCxhQUFhLENBQUNDLFFBQVEsRUFBRUMsRUFBRSxDQUFDO1FBQUEsRUFBQztNQUN0RCxDQUFDLENBQUM7SUFDTixDQUFDLENBQUM7RUFDVixDQUFDLENBQUM7QUFDTixDQUFDLEVBQUUsQ0FBQyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL2Fzc2V0cy9hcHAuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKCgpID0+IHtcclxuICAgICd1c2Ugc3RyaWN0J1xyXG5cclxuICAgIGNvbnN0IHN0b3JlZFRoZW1lID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3RoZW1lJylcclxuXHJcbiAgICBjb25zdCBnZXRQcmVmZXJyZWRUaGVtZSA9ICgpID0+IHtcclxuICAgICAgICBpZiAoc3RvcmVkVGhlbWUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHN0b3JlZFRoZW1lXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gd2luZG93Lm1hdGNoTWVkaWEoJyhwcmVmZXJzLWNvbG9yLXNjaGVtZTogZGFyayknKS5tYXRjaGVzID8gJ2RhcmsnIDogJ2xpZ2h0J1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IHNldFRoZW1lID0gZnVuY3Rpb24gKHRoZW1lKSB7XHJcbiAgICAgICAgaWYgKHRoZW1lID09PSAnYXV0bycgJiYgd2luZG93Lm1hdGNoTWVkaWEoJyhwcmVmZXJzLWNvbG9yLXNjaGVtZTogZGFyayknKS5tYXRjaGVzKSB7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2RhdGEtYnMtdGhlbWUnLCAnZGFyaycpXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNldEF0dHJpYnV0ZSgnZGF0YS1icy10aGVtZScsIHRoZW1lKVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzZXRUaGVtZShnZXRQcmVmZXJyZWRUaGVtZSgpKVxyXG5cclxuICAgIGNvbnN0IHNob3dBY3RpdmVUaGVtZSA9ICh0aGVtZSwgZm9jdXMgPSBmYWxzZSkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHRoZW1lU3dpdGNoZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjYmQtdGhlbWUnKVxyXG5cclxuICAgICAgICBpZiAoIXRoZW1lU3dpdGNoZXIpIHtcclxuICAgICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBhY3RpdmVUaGVtZUljb24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcudGhlbWUtaWNvbi1hY3RpdmUnKVxyXG4gICAgICAgIGNvbnN0IGJ0blRvQWN0aXZlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgW2RhdGEtYnMtdGhlbWUtdmFsdWU9XCIke3RoZW1lfVwiXWApXHJcbiAgICAgICAgY29uc3QgYWN0aXZlQnRuID0gYnRuVG9BY3RpdmUucXVlcnlTZWxlY3RvcignaScpXHJcblxyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ1tkYXRhLWJzLXRoZW1lLXZhbHVlXScpLmZvckVhY2goZWxlbWVudCA9PiB7XHJcbiAgICAgICAgICAgIGVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJylcclxuICAgICAgICAgICAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtcHJlc3NlZCcsICdmYWxzZScpXHJcbiAgICAgICAgfSlcclxuXHJcbiAgICAgICAgYnRuVG9BY3RpdmUuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJylcclxuICAgICAgICBidG5Ub0FjdGl2ZS5zZXRBdHRyaWJ1dGUoJ2FyaWEtcHJlc3NlZCcsICd0cnVlJylcclxuICAgICAgICBhY3RpdmVUaGVtZUljb24uaW5uZXJIVE1MID0gYWN0aXZlQnRuLm91dGVySFRNTFxyXG5cclxuICAgICAgICBpZiAoZm9jdXMpIHtcclxuICAgICAgICAgICAgdGhlbWVTd2l0Y2hlci5mb2N1cygpXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHdpbmRvdy5tYXRjaE1lZGlhKCcocHJlZmVycy1jb2xvci1zY2hlbWU6IGRhcmspJykuYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgKCkgPT4ge1xyXG4gICAgICAgIGlmIChzdG9yZWRUaGVtZSAhPT0gJ2xpZ2h0JyB8fCBzdG9yZWRUaGVtZSAhPT0gJ2RhcmsnKSB7XHJcbiAgICAgICAgICAgIHNldFRoZW1lKGdldFByZWZlcnJlZFRoZW1lKCkpXHJcbiAgICAgICAgfVxyXG4gICAgfSlcclxuXHJcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsICgpID0+IHtcclxuICAgICAgICBzaG93QWN0aXZlVGhlbWUoZ2V0UHJlZmVycmVkVGhlbWUoKSlcclxuXHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtYnMtdGhlbWUtdmFsdWVdJylcclxuICAgICAgICAgICAgLmZvckVhY2godG9nZ2xlID0+IHtcclxuICAgICAgICAgICAgICAgIHRvZ2dsZS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB0aGVtZSA9IHRvZ2dsZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtYnMtdGhlbWUtdmFsdWUnKVxyXG4gICAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCd0aGVtZScsIHRoZW1lKVxyXG4gICAgICAgICAgICAgICAgICAgIHNldFRoZW1lKHRoZW1lKVxyXG4gICAgICAgICAgICAgICAgICAgIHNob3dBY3RpdmVUaGVtZSh0aGVtZSwgdHJ1ZSlcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICB9KVxyXG5cclxuICAgIGNvbnN0IHJlbW92ZUVsZW1lbnQgPSBmdW5jdGlvbiAocmVzcG9uc2UsIGlkKSB7XHJcbiAgICAgICAgaWYgKDIwMCA9PT0gcmVzcG9uc2Uuc3RhdHVzKSB7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyMnICsgaWQpLnJlbW92ZSgpXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgKCkgPT4ge1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5idG4tanMtYWN0aW9uJylcclxuICAgICAgICAgICAgLmZvckVhY2goYnRuID0+IHtcclxuICAgICAgICAgICAgICAgIGJ0bi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIChldnQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBocmVmID0gYnRuLmdldEF0dHJpYnV0ZSgnaHJlZicpXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgaWQgPSBidG4uZ2V0QXR0cmlidXRlKCdpZCcpXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGZldGNoKGhyZWYpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHJlbW92ZUVsZW1lbnQocmVzcG9uc2UsIGlkKSlcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICB9KVxyXG59KSgpO1xyXG4iXSwibmFtZXMiOlsic3RvcmVkVGhlbWUiLCJsb2NhbFN0b3JhZ2UiLCJnZXRJdGVtIiwiZ2V0UHJlZmVycmVkVGhlbWUiLCJ3aW5kb3ciLCJtYXRjaE1lZGlhIiwibWF0Y2hlcyIsInNldFRoZW1lIiwidGhlbWUiLCJkb2N1bWVudCIsImRvY3VtZW50RWxlbWVudCIsInNldEF0dHJpYnV0ZSIsInNob3dBY3RpdmVUaGVtZSIsImZvY3VzIiwiYXJndW1lbnRzIiwibGVuZ3RoIiwidW5kZWZpbmVkIiwidGhlbWVTd2l0Y2hlciIsInF1ZXJ5U2VsZWN0b3IiLCJhY3RpdmVUaGVtZUljb24iLCJidG5Ub0FjdGl2ZSIsImNvbmNhdCIsImFjdGl2ZUJ0biIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJmb3JFYWNoIiwiZWxlbWVudCIsImNsYXNzTGlzdCIsInJlbW92ZSIsImFkZCIsImlubmVySFRNTCIsIm91dGVySFRNTCIsImFkZEV2ZW50TGlzdGVuZXIiLCJ0b2dnbGUiLCJnZXRBdHRyaWJ1dGUiLCJzZXRJdGVtIiwicmVtb3ZlRWxlbWVudCIsInJlc3BvbnNlIiwiaWQiLCJzdGF0dXMiLCJidG4iLCJldnQiLCJocmVmIiwicHJldmVudERlZmF1bHQiLCJmZXRjaCIsInRoZW4iXSwic291cmNlUm9vdCI6IiJ9