"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["map"],{

/***/ "./assets/map.js":
/*!***********************!*\
  !*** ./assets/map.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_string_starts_with_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.string.starts-with.js */ "./node_modules/core-js/modules/es.string.starts-with.js");
/* harmony import */ var core_js_modules_es_string_starts_with_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_starts_with_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_map_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./styles/map.scss */ "./assets/styles/map.scss");




(function () {
  'use strict';

  var container = document.querySelector('#map');
  var map = L.map('map').setView([48.00809327013673, -3.128898715467308], 7);
  L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);
  var type = container.getAttribute('type');
  if (type.startsWith('input')) {
    var latitudeInput = document.querySelector("[id$='_latitude']");
    var longitudeInput = document.querySelector("[id$='_longitude']");
    var icon = L.icon({
      iconUrl: '/balise.webp'
    });
    var marker;
    if (latitudeInput.value !== undefined && longitudeInput.value !== undefined) {
      marker = L.marker([latitudeInput.value, longitudeInput.value], {
        icon: icon
      }).addTo(map);
    }
    if ('input-editable' === type) {
      map.on('click', function (e) {
        if (marker !== undefined) {
          map.removeLayer(marker);
        }
        marker = L.marker(e.latlng, {
          icon: icon
        }).addTo(map);
        latitudeInput.value = e.latlng.lat;
        longitudeInput.value = e.latlng.lng;
      });
    }
  }
  var locationTabBtn = document.querySelector('#nav-location-tab');
  if (locationTabBtn) {
    locationTabBtn.addEventListener('click', function () {
      map.invalidateSize();
    });
  }
})();

/***/ }),

/***/ "./assets/styles/map.scss":
/*!********************************!*\
  !*** ./assets/styles/map.scss ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendors-node_modules_core-js_internals_array-iteration_js-node_modules_core-js_internals_export_js","vendors-node_modules_leaflet_dist_leaflet-src_js-node_modules_leaflet_dist_leaflet_css-node_m-78473b"], () => (__webpack_exec__("./assets/map.js")));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBaUI7QUFFVTtBQUUzQixDQUFDLFlBQU07RUFDSCxZQUFZOztFQUVaLElBQU1BLFNBQVMsR0FBR0MsUUFBUSxDQUFDQyxhQUFhLENBQUMsTUFBTSxDQUFDO0VBRWhELElBQU1DLEdBQUcsR0FBR0MsQ0FBQyxDQUFDRCxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUNFLE9BQU8sQ0FBQyxDQUFDLGlCQUFpQixFQUFFLENBQUMsaUJBQWlCLENBQUMsRUFBRSxDQUFDLENBQUM7RUFDNUVELENBQUMsQ0FBQ0UsU0FBUyxDQUFDLGdEQUFnRCxDQUFDLENBQUNDLEtBQUssQ0FBQ0osR0FBRyxDQUFDO0VBRXhFLElBQU1LLElBQUksR0FBR1IsU0FBUyxDQUFDUyxZQUFZLENBQUMsTUFBTSxDQUFDO0VBRTNDLElBQUlELElBQUksQ0FBQ0UsVUFBVSxDQUFDLE9BQU8sQ0FBQyxFQUFFO0lBQzFCLElBQU1DLGFBQWEsR0FBR1YsUUFBUSxDQUFDQyxhQUFhLENBQUMsbUJBQW1CLENBQUM7SUFDakUsSUFBTVUsY0FBYyxHQUFHWCxRQUFRLENBQUNDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQztJQUVuRSxJQUFNVyxJQUFJLEdBQUdULENBQUMsQ0FBQ1MsSUFBSSxDQUFDO01BQUNDLE9BQU8sRUFBRTtJQUFjLENBQUMsQ0FBQztJQUU5QyxJQUFJQyxNQUFNO0lBQ1YsSUFBSUosYUFBYSxDQUFDSyxLQUFLLEtBQUtDLFNBQVMsSUFBSUwsY0FBYyxDQUFDSSxLQUFLLEtBQUtDLFNBQVMsRUFBRTtNQUN6RUYsTUFBTSxHQUFHWCxDQUFDLENBQUNXLE1BQU0sQ0FBQyxDQUFDSixhQUFhLENBQUNLLEtBQUssRUFBRUosY0FBYyxDQUFDSSxLQUFLLENBQUMsRUFBRTtRQUFFSCxJQUFJLEVBQUVBO01BQUksQ0FBQyxDQUFDLENBQUNOLEtBQUssQ0FBQ0osR0FBRyxDQUFDO0lBQzVGO0lBRUEsSUFBSSxnQkFBZ0IsS0FBS0ssSUFBSSxFQUFFO01BQzNCTCxHQUFHLENBQUNlLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBU0MsQ0FBQyxFQUFFO1FBQ3hCLElBQUlKLE1BQU0sS0FBS0UsU0FBUyxFQUFFO1VBQ3RCZCxHQUFHLENBQUNpQixXQUFXLENBQUNMLE1BQU0sQ0FBQztRQUMzQjtRQUVBQSxNQUFNLEdBQUdYLENBQUMsQ0FBQ1csTUFBTSxDQUFDSSxDQUFDLENBQUNFLE1BQU0sRUFBRTtVQUFFUixJQUFJLEVBQUVBO1FBQUksQ0FBQyxDQUFDLENBQUNOLEtBQUssQ0FBQ0osR0FBRyxDQUFDO1FBQ3JEUSxhQUFhLENBQUNLLEtBQUssR0FBR0csQ0FBQyxDQUFDRSxNQUFNLENBQUNDLEdBQUc7UUFDbENWLGNBQWMsQ0FBQ0ksS0FBSyxHQUFHRyxDQUFDLENBQUNFLE1BQU0sQ0FBQ0UsR0FBRztNQUN2QyxDQUFDLENBQUM7SUFDTjtFQUNKO0VBRUEsSUFBTUMsY0FBYyxHQUFHdkIsUUFBUSxDQUFDQyxhQUFhLENBQUMsbUJBQW1CLENBQUM7RUFDbEUsSUFBSXNCLGNBQWMsRUFBRTtJQUNoQkEsY0FBYyxDQUFDQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsWUFBWTtNQUNqRHRCLEdBQUcsQ0FBQ3VCLGNBQWMsQ0FBQyxDQUFDO0lBQ3hCLENBQUMsQ0FBQztFQUNOO0FBQ0osQ0FBQyxFQUFFLENBQUM7Ozs7Ozs7Ozs7O0FDNUNKIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vYXNzZXRzL21hcC5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvc3R5bGVzL21hcC5zY3NzPzAxM2EiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICdsZWFmbGV0JztcblxuaW1wb3J0ICcuL3N0eWxlcy9tYXAuc2Nzcyc7XG5cbigoKSA9PiB7XG4gICAgJ3VzZSBzdHJpY3QnXG5cbiAgICBjb25zdCBjb250YWluZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjbWFwJylcblxuICAgIGNvbnN0IG1hcCA9IEwubWFwKCdtYXAnKS5zZXRWaWV3KFs0OC4wMDgwOTMyNzAxMzY3MywgLTMuMTI4ODk4NzE1NDY3MzA4XSwgNylcbiAgICBMLnRpbGVMYXllcignaHR0cHM6Ly90aWxlLm9wZW5zdHJlZXRtYXAub3JnL3t6fS97eH0ve3l9LnBuZycpLmFkZFRvKG1hcClcblxuICAgIGNvbnN0IHR5cGUgPSBjb250YWluZXIuZ2V0QXR0cmlidXRlKCd0eXBlJyk7XG5cbiAgICBpZiAodHlwZS5zdGFydHNXaXRoKCdpbnB1dCcpKSB7XG4gICAgICAgIGNvbnN0IGxhdGl0dWRlSW5wdXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiW2lkJD0nX2xhdGl0dWRlJ11cIilcbiAgICAgICAgY29uc3QgbG9uZ2l0dWRlSW5wdXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiW2lkJD0nX2xvbmdpdHVkZSddXCIpXG5cbiAgICAgICAgY29uc3QgaWNvbiA9IEwuaWNvbih7aWNvblVybDogJy9iYWxpc2Uud2VicCd9KVxuXG4gICAgICAgIGxldCBtYXJrZXJcbiAgICAgICAgaWYgKGxhdGl0dWRlSW5wdXQudmFsdWUgIT09IHVuZGVmaW5lZCAmJiBsb25naXR1ZGVJbnB1dC52YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBtYXJrZXIgPSBMLm1hcmtlcihbbGF0aXR1ZGVJbnB1dC52YWx1ZSwgbG9uZ2l0dWRlSW5wdXQudmFsdWVdLCB7IGljb246IGljb259KS5hZGRUbyhtYXApO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCdpbnB1dC1lZGl0YWJsZScgPT09IHR5cGUpIHtcbiAgICAgICAgICAgIG1hcC5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgICAgICAgaWYgKG1hcmtlciAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgICAgIG1hcC5yZW1vdmVMYXllcihtYXJrZXIpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIG1hcmtlciA9IEwubWFya2VyKGUubGF0bG5nLCB7IGljb246IGljb259KS5hZGRUbyhtYXApO1xuICAgICAgICAgICAgICAgIGxhdGl0dWRlSW5wdXQudmFsdWUgPSBlLmxhdGxuZy5sYXRcbiAgICAgICAgICAgICAgICBsb25naXR1ZGVJbnB1dC52YWx1ZSA9IGUubGF0bG5nLmxuZ1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBjb25zdCBsb2NhdGlvblRhYkJ0biA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNuYXYtbG9jYXRpb24tdGFiJylcbiAgICBpZiAobG9jYXRpb25UYWJCdG4pIHtcbiAgICAgICAgbG9jYXRpb25UYWJCdG4uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBtYXAuaW52YWxpZGF0ZVNpemUoKVxuICAgICAgICB9KVxuICAgIH1cbn0pKCk7IiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luXG5leHBvcnQge307Il0sIm5hbWVzIjpbImNvbnRhaW5lciIsImRvY3VtZW50IiwicXVlcnlTZWxlY3RvciIsIm1hcCIsIkwiLCJzZXRWaWV3IiwidGlsZUxheWVyIiwiYWRkVG8iLCJ0eXBlIiwiZ2V0QXR0cmlidXRlIiwic3RhcnRzV2l0aCIsImxhdGl0dWRlSW5wdXQiLCJsb25naXR1ZGVJbnB1dCIsImljb24iLCJpY29uVXJsIiwibWFya2VyIiwidmFsdWUiLCJ1bmRlZmluZWQiLCJvbiIsImUiLCJyZW1vdmVMYXllciIsImxhdGxuZyIsImxhdCIsImxuZyIsImxvY2F0aW9uVGFiQnRuIiwiYWRkRXZlbnRMaXN0ZW5lciIsImludmFsaWRhdGVTaXplIl0sInNvdXJjZVJvb3QiOiIifQ==