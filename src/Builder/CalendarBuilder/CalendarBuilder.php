<?php

declare(strict_types=1);

namespace OrienteeringManager\Builder\CalendarBuilder;

use OrienteeringManager\Entity\Page;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventLocationApiItem;
use Spatie\IcalendarGenerator\Components\Calendar;
use Spatie\IcalendarGenerator\Components\Event;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final readonly class CalendarBuilder
{
    public function __construct(
        private UrlGeneratorInterface $urlGenerator,
    ) {
    }

    /**
     * @param EventApiItem[] $eventsApiItem
     */
    public function build(Page $page, array $eventsApiItem): Calendar
    {
        $calendar = Calendar::create();
        
        foreach ($eventsApiItem as $eventApiItem) {
            $startsAt = $eventApiItem->getDate()->setTimezone(new \DateTimeZone('Europe/Paris'));
            $endsAt = $eventApiItem->getEndDate()?->setTimezone(new \DateTimeZone('Europe/Paris'));

            $url = $eventApiItem->getSite()->getUrl().$this->urlGenerator->generate('app_content_view', [
                    'page' => $page->getSLug(),
                    'id' => $eventApiItem->getId(),
                    'slug' => $eventApiItem->getSlug(),
                ]);

            $event = Event::create()
                ->uniqueIdentifier(sprintf('%d-%s', $eventApiItem->getId(), $eventApiItem->getSlug()))
                ->createdAt($eventApiItem->getCreatedAt()->setTimezone(new \DateTimeZone('Europe/Paris')))
                ->name($eventApiItem->getName())
                ->startsAt($startsAt)
                ->endsAt($endsAt ?? $startsAt)
                ->url($url)
                ->description($url);

            $location = $eventApiItem->getLocation();
            if ($location instanceof EventLocationApiItem && null !== $location->getLatitude() && null !== $location->getLongitude()) {
                $event->address(sprintf("%s,%s", $location->getLatitude(), $location->getLongitude()));
            } elseif ($location instanceof EventLocationApiItem && null !== $location->getName()) {
                $event->address($location->getName());
            }

            $calendar->event($event);
        }
        
        return $calendar;
    }
}