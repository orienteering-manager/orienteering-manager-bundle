<?php

declare(strict_types=1);

namespace OrienteeringManager\Builder\FilterBuilder;

use DateTime;
use OrienteeringManager\Model\OrienteeringManager\Filter\EventApiFilter;
use Symfony\Component\HttpFoundation\Request;

final readonly class EventApiFilterBuilder
{
    public function buildFromRequest(Request $request): EventApiFilter
    {
        $eventApiFilter = new EventApiFilter();
        $eventApiFilter->setItemsPerPage(EventApiFilter::DEFAULT_ITEMS_PER_PAGE);
        $eventApiFilter->setPage($request->query->getInt('page', 1));

        if (null !== $dateBefore = $request->query->get('dateBefore')) {
            try {
                $dateBefore = new DateTime($dateBefore);
                $dateBefore->setTime(0, 0);
                $eventApiFilter->setDateBefore($dateBefore);
            } catch (\Exception) {
            }
        }

        if (null !== $dateAfter = $request->query->get('dateAfter')) {
            try {
                $dateAfter = new DateTime($dateAfter);
                $dateAfter->setTime(0, 0);
                $eventApiFilter->setDateAfter($dateAfter);
            } catch (\Exception) {
            }
        }

        if (!empty($query = $request->query->get('query'))) {
            $eventApiFilter->setQuery($query);
        }

        return $eventApiFilter;
    }
}