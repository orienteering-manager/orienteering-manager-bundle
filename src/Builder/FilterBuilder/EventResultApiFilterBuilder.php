<?php

declare(strict_types=1);

namespace OrienteeringManager\Builder\FilterBuilder;

use OrienteeringManager\Model\OrienteeringManager\Filter\EventResultApiFilter;
use Symfony\Component\HttpFoundation\Request;

final readonly class EventResultApiFilterBuilder
{
    public function buildFromRequest(Request $request): EventResultApiFilter
    {
        $apiFilter = new EventResultApiFilter();
        $apiFilter->setItemsPerPage(EventResultApiFilter::DEFAULT_ITEMS_PER_PAGE);
        $apiFilter->setPage($request->query->getInt('page', 1));

        return $apiFilter;
    }
}