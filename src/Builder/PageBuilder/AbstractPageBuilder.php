<?php

declare(strict_types=1);

namespace OrienteeringManager\Builder\PageBuilder;

use OrienteeringManager\Entity\Page;
use OrienteeringManager\Mapper\SitemapUrlMapper;
use OrienteeringManager\Model\Breadcrumb\BreadcrumbModel;
use OrienteeringManager\Model\Page\PageModelInterface;
use OrienteeringManager\Model\Sitemap\SitemapPageConfiguration;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

abstract readonly class AbstractPageBuilder implements PageBuilderInterface
{
    /**
     * @param Serializer $serializer
     */
    public function __construct(
        protected SerializerInterface $serializer,
        protected UrlGeneratorInterface $urlGenerator,
        protected SitemapUrlMapper $sitemapUrlMapper,
    ) {
    }

    public function supports(string $pageType): bool
    {
        return $this->getBuilderId() === $pageType;
    }

    public function uniqueInstanciation(): bool
    {
        return false;
    }

    abstract protected function getModelClass(): string;

    protected function getSitemapConfiguration(): SitemapPageConfiguration
    {
        return new SitemapPageConfiguration();
    }

    public function build(Page $page): void
    {
        $page->setData(null !== $page->getContent() ?
            $this->serializer->denormalize($page->getContent(), $this->getModelClass(), JsonEncoder::FORMAT, [
                'groups' => [PageModelInterface::GROUP_PAGE_MODEL]
            ]) :
            new ($this->getModelClass())());

        $page->setIsRequired($this->uniqueInstanciation());

        $this->buildBreadcrumb($page);
        $this->buildSitemapUrl($page);
    }

    protected function buildBreadcrumb(Page $page): void
    {
        $page->setBreadcrumb(BreadcrumbModel::createFromPage($page, $this->urlGenerator->generate('app_page', ['slug' => $page->getSlug()])));
    }

    protected function buildSitemapUrl(Page $page): void
    {
        $configuration = $this->getSitemapConfiguration();

        $page->setSitemapUrl($this->sitemapUrlMapper->map(
            $this->urlGenerator->generate(
                'app_page',
                [
                    'slug' => $page->getSlug(),
                ],
                UrlGeneratorInterface::ABSOLUTE_URL,
            ),
            $configuration->getFrequency(),
            $configuration->getPriority(),
        ));
    }

    public function save(PageModelInterface $pageModel, FormInterface $form): void
    {
    }
}
