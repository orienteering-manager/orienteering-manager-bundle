<?php

declare(strict_types=1);

namespace OrienteeringManager\Builder\PageBuilder;

use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\Model\Page\Contact\ContactBlockModel;
use OrienteeringManager\Model\Page\Contact\ContactModel;
use OrienteeringManager\Model\Page\PageModelInterface;
use Symfony\Component\Form\FormInterface;

final readonly class ContactPageBuilder extends AbstractPageBuilder implements PageBuilderInterface
{
    public function getBuilderId(): string
    {
        return PageTypeEnum::CONTACT->value;
    }

    public function uniqueInstanciation(): bool
    {
        return true;
    }

    protected function getModelClass(): string
    {
        return ContactModel::class;
    }

    /**
     * @param ContactModel $pageModel
     */
    public function save(PageModelInterface $pageModel, FormInterface $form): void
    {
        $pageModel->setPersons(array_filter(
            $pageModel->getPersons(),
            fn(ContactBlockModel $contactBlock) => null !== $contactBlock->getRole()
        ));
    }
}
