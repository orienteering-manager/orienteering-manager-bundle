<?php

declare(strict_types=1);

namespace OrienteeringManager\Builder\PageBuilder;

use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\Model\Page\Custom\CustomModel;
use OrienteeringManager\Model\Sitemap\SitemapPageConfiguration;

final readonly class CustomPageBuilder extends AbstractPageBuilder implements PageBuilderInterface
{
    public function getBuilderId(): string
    {
        return PageTypeEnum::CUSTOM->value;
    }

    protected function getModelClass(): string
    {
        return CustomModel::class;
    }

    protected function getSitemapConfiguration(): SitemapPageConfiguration
    {
        $configuration = parent::getSitemapConfiguration();
        $configuration
            ->setPriority(0.75);

        return $configuration;
    }
}
