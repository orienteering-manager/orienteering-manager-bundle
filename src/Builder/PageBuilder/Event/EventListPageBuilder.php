<?php

declare(strict_types=1);

namespace OrienteeringManager\Builder\PageBuilder\Event;

use OrienteeringManager\Builder\PageBuilder\AbstractPageBuilder;
use OrienteeringManager\Builder\PageBuilder\PageBuilderInterface;
use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\Config\Enum\SitemapChangeFrequencyEnum;
use OrienteeringManager\Model\Page\Event\EventListModel;
use OrienteeringManager\Model\Sitemap\SitemapPageConfiguration;

final readonly class EventListPageBuilder extends AbstractPageBuilder implements PageBuilderInterface
{
    public function getBuilderId(): string
    {
        return PageTypeEnum::EVENT_LIST->value;
    }

    public function uniqueInstanciation(): bool
    {
        return true;
    }

    protected function getModelClass(): string
    {
        return EventListModel::class;
    }

    protected function getSitemapConfiguration(): SitemapPageConfiguration
    {
        $configuration = parent::getSitemapConfiguration();
        $configuration
            ->setFrequency(SitemapChangeFrequencyEnum::WEEKLY)
            ->setPriority(0.9);

        return $configuration;
    }
}
