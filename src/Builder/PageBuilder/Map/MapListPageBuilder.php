<?php

declare(strict_types=1);

namespace OrienteeringManager\Builder\PageBuilder\Map;

use OrienteeringManager\Builder\PageBuilder\AbstractPageBuilder;
use OrienteeringManager\Builder\PageBuilder\PageBuilderInterface;
use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\Model\Page\Map\MapListModel;
use OrienteeringManager\Model\Sitemap\SitemapPageConfiguration;

final readonly class MapListPageBuilder extends AbstractPageBuilder implements PageBuilderInterface
{
    public function getBuilderId(): string
    {
        return PageTypeEnum::MAP_LIST->value;
    }

    public function uniqueInstanciation(): bool
    {
        return true;
    }

    protected function getModelClass(): string
    {
        return MapListModel::class;
    }

    protected function getSitemapConfiguration(): SitemapPageConfiguration
    {
        $configuration = parent::getSitemapConfiguration();
        $configuration
            ->setPriority(0.9);

        return $configuration;
    }
}
