<?php

declare(strict_types=1);

namespace OrienteeringManager\Builder\PageBuilder;

use OrienteeringManager\Entity\Page;
use OrienteeringManager\Model\Page\PageModelInterface;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Component\Form\FormInterface;

#[AutoconfigureTag('orienteering_manager.page_builder')]
interface PageBuilderInterface
{
    public function getBuilderId(): string;
    public function uniqueInstanciation(): bool;
    public function build(Page $page): void;
    public function save(PageModelInterface $pageModel, FormInterface $form): void;
    public function supports(string $pageType): bool;
}
