<?php

declare(strict_types=1);

namespace OrienteeringManager\Builder\SitemapBuilder;

use DateTimeImmutable;
use OrienteeringManager\Config\Enum\ModeEnum;
use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\Config\Enum\SitemapChangeFrequencyEnum;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Mapper\SitemapUrlMapper;
use OrienteeringManager\Model\OrienteeringManager\Collection\EventApiCollection;
use OrienteeringManager\Model\OrienteeringManager\Filter\EventApiFilter;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use OrienteeringManager\Model\Sitemap\Sitemap;
use OrienteeringManager\Repository\PageRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final readonly class EventSitemapBuilder implements SitemapBuilderInterface
{
    public function __construct(
        private UrlGeneratorInterface $urlGenerator,
        private OrienteeringManagerDataProvider $dataProvider,
        private PageRepository $pageRepository,
        private SitemapUrlMapper $sitemapUrlMapper,
    ) {
    }

    public function generate(Sitemap $sitemap): void
    {
        $page = $this->pageRepository->findOneBy([
            'type' => PageTypeEnum::EVENT_LIST,
        ]);

        try {
            $eventApiFilter = new EventApiFilter();
            $eventApiFilter->setMode(ModeEnum::VIEW);

            /** @var EventApiItem[] $events */
            $events = $this->dataProvider->getCollection(new EventApiCollection($eventApiFilter))->getItems();
        } catch (OrienteeringManagerException) {
            return;
        }

        foreach ($events as $event) {
            $sitemap->addUrl($this->sitemapUrlMapper->map(
                $this->urlGenerator->generate(
                    'app_content_view',
                    [
                        'page' => $page->getSlug(),
                        'id' => $event->getId(),
                        'slug' => $event->getSlug(),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL,
                ),
                SitemapChangeFrequencyEnum::DAILY,
                1.0,
                $event->getUpdatedAt() ?? $event->getCreatedAt() ?? new DateTimeImmutable(),
            ));

            if ($event->isAllowRegistration()) {
                $sitemap->addUrl($this->sitemapUrlMapper->map(
                    $this->urlGenerator->generate(
                        'app_event_register',
                        [
                            'page' => $page->getSlug(),
                            'id' => $event->getId(),
                            'slug' => $event->getSlug(),
                        ],
                        UrlGeneratorInterface::ABSOLUTE_URL,
                    ),
                    SitemapChangeFrequencyEnum::MONTHLY,
                    0.9,
                ));
            }
        }
    }
}
