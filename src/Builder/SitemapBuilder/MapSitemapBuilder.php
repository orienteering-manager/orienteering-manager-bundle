<?php

declare(strict_types=1);

namespace OrienteeringManager\Builder\SitemapBuilder;

use DateTimeImmutable;
use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\Config\Enum\SitemapChangeFrequencyEnum;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Mapper\SitemapUrlMapper;
use OrienteeringManager\Model\OrienteeringManager\Collection\MapApiCollection;
use OrienteeringManager\Model\OrienteeringManager\Item\MapApiItem;
use OrienteeringManager\Model\Sitemap\Sitemap;
use OrienteeringManager\Repository\PageRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final readonly class MapSitemapBuilder implements SitemapBuilderInterface
{
    public function __construct(
        private UrlGeneratorInterface $urlGenerator,
        private OrienteeringManagerDataProvider $dataProvider,
        private PageRepository $pageRepository,
        private SitemapUrlMapper $sitemapUrlMapper,
    ) {
    }

    public function generate(Sitemap $sitemap): void
    {
        $page = $this->pageRepository->findOneBy([
            'type' => PageTypeEnum::MAP_LIST,
        ]);

        try {
            /** @var MapApiItem[] $maps */
            $maps = $this->dataProvider->getCollection(new MapApiCollection())->getItems();
        } catch (OrienteeringManagerException) {
            return;
        }

        foreach ($maps as $map) {
            $sitemap->addUrl($this->sitemapUrlMapper->map(
                $this->urlGenerator->generate(
                    'app_content_view',
                    [
                        'page' => $page->getSlug(),
                        'id' => $map->getId(),
                        'slug' => $map->getSlug(),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL,
                ),
                SitemapChangeFrequencyEnum::MONTHLY,
                0.8,
                $map->getUpdatedAt() ?? $map->getCreatedAt() ?? new DateTimeImmutable()
            ));
        }
    }
}
