<?php

declare(strict_types=1);

namespace OrienteeringManager\Builder\SitemapBuilder;

use OrienteeringManager\Model\Sitemap\Sitemap;
use OrienteeringManager\Model\Sitemap\Url;
use OrienteeringManager\Repository\PageRepository;

final readonly class PageSitemapBuilder implements SitemapBuilderInterface
{
    public function __construct(
        private PageRepository $pageRepository,
    ) {
    }

    public function generate(Sitemap $sitemap): void
    {
        $pages = $this->pageRepository->findAll();
        foreach ($pages as $page) {
            if (!$page->getSitemapUrl() instanceof Url) {
                continue;
            }

            $sitemap->addUrl($page->getSitemapUrl());
        }
    }
}
