<?php

declare(strict_types=1);

namespace OrienteeringManager\Builder\SitemapBuilder;

use OrienteeringManager\Model\Sitemap\Sitemap;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

#[AutoconfigureTag('orienteering_manager.sitemap_builder')]
interface SitemapBuilderInterface
{
    public function generate(Sitemap $sitemap): void;
}
