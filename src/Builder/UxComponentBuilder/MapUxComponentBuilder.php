<?php

declare(strict_types=1);

namespace OrienteeringManager\Builder\UxComponentBuilder;

use OrienteeringManager\Entity\Page;
use OrienteeringManager\Model\LocationInterface;
use OrienteeringManager\Model\OrienteeringManager\Item\ContentApiItemInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\UX\Map\Bridge\Leaflet\LeafletOptions;
use Symfony\UX\Map\Bridge\Leaflet\Option\TileLayer;
use Symfony\UX\Map\InfoWindow;
use Symfony\UX\Map\Map;
use Symfony\UX\Map\Marker;
use Symfony\UX\Map\Point;

final readonly class MapUxComponentBuilder implements MapUxComponentBuilderInterface
{
    public function __construct(
        private UrlGeneratorInterface $urlGenerator,
    ) {
    }

    /**
     * @param LocationInterface|LocationInterface[]|null $locations
     */
    public function build(LocationInterface|array|null $locations, ?Page $page = null): Map
    {
        $map = (new Map('default'))
            ->center(new Point(48.00809327013673, -3.128898715467308))
            ->zoom(10)
            ->options((new LeafletOptions())
                ->tileLayer(new TileLayer(
                    url: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                    attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                    options: ['maxZoom' => 19]
                ))
            );

        if (null === $locations) {
            return $map;
        }

        if ($locations instanceof LocationInterface) {
            $locations = [$locations];
        }

        foreach ($locations as $index => $location) {
            if (!$location instanceof LocationInterface) {
                unset($locations[$index]);
                continue;
            }

            $marker = $this->createMarker($location, $page);
            if (!$marker instanceof Marker) {
                unset($locations[$index]);
                continue;
            }

            $map
                ->center(Point::fromArray($marker->toArray()['position']))
                ->addMarker($marker);
        }

        return $map
            ->fitBoundsToMarkers(1 < count($locations))
            ->zoom(0 === count($locations) ? 8 : 14);
    }

    private function createMarker(LocationInterface $location, ?Page $page): ?Marker
    {
        $latitude = $location->getLatitude();
        $longitude = $location->getLongitude();

        if (null === $latitude || null === $longitude) {
            return null;
        }

        $infoWindow = null;
        if ($location instanceof ContentApiItemInterface && $page instanceof Page) {
            $contentUrl = $location->getContentUrl();
            $contentUrl['parameters'] += [
                'page' => $page->getSlug(),
            ];

            $infoWindow = new InfoWindow(
                headerContent: $location->getName(),
                content:  sprintf('<a href="%s">Découvrir</a>', $this->urlGenerator->generate(...$contentUrl))
            );
        }

        return new Marker(
            position: new Point($latitude, $longitude),
            title: $location->getName(),
            infoWindow: $infoWindow,
        );
    }
}