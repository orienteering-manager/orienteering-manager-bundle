<?php

namespace OrienteeringManager\Builder\UxComponentBuilder;

use OrienteeringManager\Entity\Page;
use OrienteeringManager\Model\LocationInterface;
use Symfony\UX\Map\Map;

interface MapUxComponentBuilderInterface
{
    public function build(LocationInterface $locations, ?Page $page = null): Map;
}