<?php

declare(strict_types=1);

namespace OrienteeringManager\Command;

use OrienteeringManager\Manager\SitemapManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Filesystem\Filesystem;

#[AsCommand(
    name: 'orienteering-manager:generate:sitemap',
    description: 'Refresh sitemap.xml file',
)]
final class SitemapGeneratorCommand extends Command
{
    public function __construct(
        private readonly SitemapManager $sitemapManager,
        #[Autowire('%kernel.project_dir%/public/')]
        private readonly string $publicDir,
        string $name = null,
    ) {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $filename = sprintf('%s/sitemap.xml', $this->publicDir);

        $filesystem = new Filesystem();
        $filesystem->remove($filename);
        $filesystem->appendToFile($filename, $this->sitemapManager->generate());

        return Command::SUCCESS;
    }
}
