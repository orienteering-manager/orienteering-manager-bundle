<?php

declare(strict_types=1);

namespace OrienteeringManager\Config\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_PROPERTY)]
final readonly class Uploadable
{
}
