<?php

declare(strict_types=1);

namespace OrienteeringManager\Config\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_PARAMETER | Attribute::TARGET_METHOD | Attribute::TARGET_PROPERTY)]
final readonly class UploadableField
{
    public function __construct(
        private array|string|null $nameProperties = null,
        private ?string $targetFolder = null,
        private ?string $urlField = null,
        private bool $enableVersioning = true,
        private ?string $deletionField = null,
    ) {
    }

    public function getNameProperties(): array|string|null
    {
        return $this->nameProperties;
    }

    public function getTargetFolder(): ?string
    {
        return $this->targetFolder;
    }

    public function getUrlField(): ?string
    {
        return $this->urlField;
    }
    
    public function isEnableVersioning(): bool
    {
        return $this->enableVersioning;
    }

    public function getDeletionField(): ?string
    {
        return $this->deletionField;
    }
}
