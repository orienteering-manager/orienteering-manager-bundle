<?php

declare(strict_types=1);

namespace OrienteeringManager\Config\Constant;

final readonly class ColorConstant
{
    final public const string PRIMARY = 'primary';
    final public const string SECONDARY = 'secondary';
    final public const string WARNING = 'warning';
    final public const string INFO = 'info';
    final public const string SUCCESS = 'success';
    final public const string DANGER = 'danger';
    final public const string MUTED = 'muted';
    
    private function __construct()
    {
    }
}