<?php

declare(strict_types=1);

namespace OrienteeringManager\Config\Constant;

final class FormOptionsConstant
{
    public const array FORM_CONTROL_OPTIONS = [
        'attr' => [
            'class' => 'form-control',
        ],
        'label_attr' => [
            'class' => 'form-label text-primary',
        ],
        'row_attr' => [
            'class' => 'mb-2',
        ],
        'help_attr' => [
            'class' => 'form-text text-muted',
        ],
    ];

    public const array FORM_SELECT_OPTIONS = [
        'attr' => [
            'class' => 'form-select',
        ],
        'label_attr' => [
            'class' => 'form-label text-primary',
        ],
        'help_attr' => [
            'class' => 'form-text text-muted',
        ],
        'row_attr' => [
            'class' => 'mb-2',
        ],
    ];

    public const array FORM_CHECKBOX_OPTIONS = [
        'attr' => [
            'class' => 'form-check-input',
        ],
        'label_attr' => [
            'class' => 'form-check-label text-primary',
        ],
        'row_attr' => [
            'class' => 'form-check mb-2',
        ],
        'help_attr' => [
            'class' => 'form-text text-muted',
        ],
    ];
    
    public const array FORM_QUILL_OPTIONS = [
        'attr' => [
            'class' => 'form-control ql-editor',
        ],
        'label_attr' => [
            'class' => 'form-label text-primary',
        ],
        'row_attr' => [
            'class' => 'mb-2',
        ],
        'help_attr' => [
            'class' => 'form-text text-muted',
        ],
        'quill_extra_options' => [
            'placeholder' => '',
        ],
    ];
    
    public static function getFormControlOptions(string $size = null, string $type = 'primary'): array
    {
        return [
            'attr' => [
                'class' => 'form-control',
            ],
            'label_attr' => [
                'class' => "form-label text-$type",
            ],
            'row_attr' => [
                'class' => 'mb-2',
            ],
            'help_attr' => [
                'class' => 'form-text text-muted',
            ],
        ];
    }

    public static function getFormSelectOptions(string $size = null, string $type = 'primary'): array
    {
        return [
            'attr' => [
                'class' => 'form-select',
            ],
            'label_attr' => [
                'class' => "form-label text-$type",
            ],
            'help_attr' => [
                'class' => 'form-text text-muted',
            ],
            'row_attr' => [
                'class' => 'mb-2',
            ],
        ];
    }

    public static function getFormCheckboxOptions(string $size = null, string $type = 'primary'): array
    {
        return [
            'attr' => [
                'class' => 'form-check-input',
            ],
            'label_attr' => [
                'class' => "form-check-label text-$type",
            ],
            'row_attr' => [
                'class' => 'form-check mb-2',
            ],
            'help_attr' => [
                'class' => 'form-text text-muted',
            ],
        ];
    }
}