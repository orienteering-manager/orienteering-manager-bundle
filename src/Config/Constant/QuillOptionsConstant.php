<?php

declare(strict_types=1);

namespace OrienteeringManager\Config\Constant;

use Ehyiah\QuillJsBundle\DTO\Fields\BlockField\ColorField;
use Ehyiah\QuillJsBundle\DTO\Fields\BlockField\HeaderGroupField;
use Ehyiah\QuillJsBundle\DTO\Fields\BlockField\SizeField;
use Ehyiah\QuillJsBundle\DTO\Fields\InlineField\BoldField;
use Ehyiah\QuillJsBundle\DTO\Fields\InlineField\CleanField;
use Ehyiah\QuillJsBundle\DTO\Fields\InlineField\ItalicField;
use Ehyiah\QuillJsBundle\DTO\Fields\InlineField\LinkField;
use Ehyiah\QuillJsBundle\DTO\Fields\InlineField\StrikeField;
use Ehyiah\QuillJsBundle\DTO\Fields\InlineField\UnderlineField;
use Ehyiah\QuillJsBundle\DTO\QuillGroup;

final class QuillOptionsConstant
{
    public static function defaultConfig(): array {
        return [
            QuillGroup::build(
                new HeaderGroupField(HeaderGroupField::HEADER_OPTION_2, HeaderGroupField::HEADER_OPTION_3, HeaderGroupField::HEADER_OPTION_4, HeaderGroupField::HEADER_OPTION_5, HeaderGroupField::HEADER_OPTION_6),
                new SizeField()
            ),
            QuillGroup::build(
                new BoldField(),
                new ItalicField(),
                new UnderlineField(),
                new StrikeField(),
            ),
            QuillGroup::build(
                new ColorField(),
            ),
            QuillGroup::build(
                new LinkField(),
            ),
            QuillGroup::build(
                new CleanField(),
            ),
        ];
    }
}