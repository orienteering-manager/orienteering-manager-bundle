<?php

declare(strict_types=1);

namespace OrienteeringManager\Config\Enum;

enum AdminPageEnum: string
{
    use ToolsEnumTrait;

    case LIST = 'list';
    case ADD = 'add';
    case EDIT = 'edit';
    case EXPORT = 'export';
    case IMPORT = 'import';
}
