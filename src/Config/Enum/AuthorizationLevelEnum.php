<?php

declare(strict_types=1);

namespace OrienteeringManager\Config\Enum;

enum AuthorizationLevelEnum: int
{
    case UNAUTHORIZED = 0;
    case LOCAL = 100;
    case DEPARTMENTAL = 200;
    case REGIONAL = 300;
    case NATIONAL = 400;
    case INTERNATIONAL = 500;
}
