<?php

declare(strict_types=1);

namespace OrienteeringManager\Config\Enum;

enum EventFormatEnum: int
{
    use ToolsEnumTrait;

    case SPECIAL = 1;
    case ORIENTEERING_CAMP = 2;
    case STAGE_RACE = 3;
    case NIGHT = 4;
    case OTHER = 5;
    case SCORE_RACE = 6;
    case RELAY = 7;
    case RAID = 8;
    case SPRINT = 9;
    case LONG = 10;
    case MTBO = 11;
    case TRAINING = 12;
    case ORIENTEERING_SCHOOL = 13;
    case MIDDLE = 14;
}
