<?php

declare(strict_types=1);

namespace OrienteeringManager\Config\Enum;

enum EventRegistrationStatusEnum: string
{
    use ToolsEnumTrait;

    case CREATED = 'created';
    case UPDATED = 'updated';
    case VALIDATED = 'validated';
    case DELETED = 'deleted';
}
