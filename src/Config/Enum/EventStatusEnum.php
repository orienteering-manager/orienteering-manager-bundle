<?php

declare(strict_types=1);

namespace OrienteeringManager\Config\Enum;

enum EventStatusEnum: int
{
    use ToolsEnumTrait;

    case CANCELLED = 1;
    case REPORTED = 2;
}
