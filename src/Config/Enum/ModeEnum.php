<?php

declare(strict_types=1);

namespace OrienteeringManager\Config\Enum;

enum ModeEnum: string
{
    case VIEW = 'view';
    case EDIT = 'edit';
    case DELETE = 'delete';
}
