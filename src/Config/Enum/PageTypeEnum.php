<?php

declare(strict_types=1);

namespace OrienteeringManager\Config\Enum;

enum PageTypeEnum: string implements PageTypeInterface
{
    use ToolsEnumTrait;

    case CUSTOM = 'custom';
    case EVENT_LIST = 'event.list';
    case EVENT_VIEW = 'event.view';
    case EVENT_RESULT_LIST = 'event_result.list';
    case EVENT_RESULT_VIEW = 'event_result.view';
    case MAP_LIST = 'map.list';
    case MAP_VIEW = 'map.view';
    case CONTACT = 'contact';
}
