<?php

namespace OrienteeringManager\Config\Enum;

interface PageTypeInterface
{
    public static function values(): array;
}