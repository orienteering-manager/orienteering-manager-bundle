<?php

declare(strict_types=1);

namespace OrienteeringManager\Config\Enum;

enum SitemapChangeFrequencyEnum: string
{
    use ToolsEnumTrait;

    case ALWAYS = 'always';
    case HOURLY = 'hourly';
    case DAILY = 'daily';
    case WEEKLY = 'weekly';
    case MONTHLY = 'monthly';
    case YEARLY = 'yearly';
    case NEVER = 'never';
}
