<?php

declare(strict_types=1);

namespace OrienteeringManager\Config\Enum;

trait ToolsEnumTrait
{
    public static function values(): array
    {
        $values = [];
        foreach (self::cases() as $case) {
            $values[$case->value] = $case->value;
        }

        return $values;
    }

    public static function names(): array
    {
        return array_column(self::cases(), 'name');
    }
}
