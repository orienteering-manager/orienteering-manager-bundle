<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\Admin;

use OrienteeringManager\Exception\OrienteeringManagerException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractAdminController extends AbstractController
{
    public function __construct(
        protected readonly TranslatorInterface $translator,
    ) {
    }

    protected function addFlashMessage(string $type, string $translationKey, OrienteeringManagerException $exception): void
    {
        $this->addFlash(
            $type,
            $this->translator->trans(
                $translationKey,
                parameters: [
                    '%code%' => $exception->getCode(),
                    '%message%' => $exception->getMessage(),
                    '%object%' => $exception->getObject(),
                ],
                domain: 'admin',
            ));
    }
}
