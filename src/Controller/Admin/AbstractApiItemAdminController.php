<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\Admin;

use OrienteeringManager\Config\Enum\ModeEnum;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Model\Breadcrumb\BreadcrumbModel;
use OrienteeringManager\Model\OrienteeringManager\Filter\ApiFilterInterface;
use OrienteeringManager\Model\OrienteeringManager\Item\ApiItemInterface;
use OrienteeringManager\Repository\PageRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractApiItemAdminController extends AbstractAdminController
{
    protected const int ITEMS_PER_PAGES = 15;

    public function __construct(
        TranslatorInterface $translator,
        protected readonly OrienteeringManagerDataProvider $dataProvider,
        protected readonly PageRepository $pageRepository,
        protected readonly ValidatorInterface $validator,
    ) {
        parent::__construct($translator);
    }

    abstract protected function getType(): string;
    abstract protected function getPageType(): string;
    abstract protected function getCollectionClass(): string;
    abstract protected function getItemClass(): string;
    abstract protected function getFormClass(): string;
    abstract protected function getFilterFormClass(): ?string;
    abstract protected function getFilter(Request $request): ?ApiFilterInterface;

    protected function getTemplateParameters(ApiItemInterface $entity): array
    {
        return [
            'breadcrumb' => $this->generateBreadcrumb($entity),
            'entity' => $entity,
        ];
    }

    #[Route('/', name: 'list')]
    public function list(Request $request): Response
    {
        $filter = $this->getFilter($request);

        $filterFormClass = $this->getFilterFormClass();
        $filterForm = null;

        if (null !== $filterFormClass) {
            $filterForm = $this->createForm($this->getFilterFormClass(), $filter);
            $filterForm->handleRequest($request);
        }

        $itemApiCollection = new ($this->getCollectionClass())($filter);

        $itemsCollection = null;
        try {
            $itemsCollection = $this->dataProvider->getCollection($itemApiCollection);
        } catch (OrienteeringManagerException $e) {
            $this->addFlashMessage('error', "admin.{$this->getType()}.message.list.error", $e);
        }

        return $this->render(sprintf('@OrienteeringManager/admin/%s/list.html.twig', $this->getType()), [
            'type' => $this->getType(),
            'page' => $this->pageRepository->findOneBy([
                'type' => $this->getPageType(),
            ]),
            'breadcrumb' => $this->generateBreadcrumb(),
            'entities' => $itemsCollection?->getItems() ?? [],
            'filter' => $filter,
            'maxPages' => ceil($itemsCollection?->getTotalItems() / self::ITEMS_PER_PAGES),
            'form' => $filterForm?->createView(),
        ]);
    }

    #[Route('/add', name: 'add')]
    #[Route('/{id}/edit', name: 'edit')]
    public function form(Request $request, ?int $id = null): Response
    {
        $entity = $this->getItem($request, $id);
        if (null === $entity) {
            return $this->redirectToRoute(sprintf('app_admin_%s_list', $this->getType()));
        }

        $form = $this->createForm($this->getFormClass(), $entity);
        $form->handleRequest($request);

        $action = null === $id ? 'add' : 'edit';

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->dataProvider->save($entity);
                $this->addFlash('success', $this->translator->trans(
                    "admin.{$this->getType()}.message.$action.success",
                    parameters: ['%entity%' => $entity],
                    domain: 'admin',
                ));

                return $this->redirectToRoute(sprintf("app_admin_%s_list", $this->getType()));
            } catch (OrienteeringManagerException $e) {
                $this->addFlashMessage('error', "admin.{$this->getType()}.message.$action.error", $e);
            }
        }

        return $this->render(sprintf("@OrienteeringManager/admin/%s/%s.html.twig", $this->getType(), $action), array_merge(
            $this->getTemplateParameters($entity),
            [
                'form' => $form->createView(),
            ]
        ));
    }

    #[Route('/{id}/clone', name: 'clone')]
    public function clone(Request $request, int $id): Response
    {
        $entity = clone $this->getItem($request, $id);
        if (null === $entity) {
            return $this->redirectToRoute(sprintf('app_admin_%s_list', $this->getType()));
        }

        $form = $this->createForm($this->getFormClass(), $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->dataProvider->save($entity);
                $this->addFlash('success', $this->translator->trans(
                    "admin.{$this->getType()}.message.add.success",
                    parameters: ['%entity%' => $entity],
                    domain: 'admin',
                ));

                return $this->redirectToRoute(sprintf("app_admin_%s_list", $this->getType()));
            } catch (OrienteeringManagerException $e) {
                $this->addFlashMessage('error', "admin.{$this->getType()}.message.add.error", $e);
            }
        }

        return $this->render(sprintf('@OrienteeringManager/admin/%s/add.html.twig', $this->getType()), array_merge(
            $this->getTemplateParameters($entity),
            [
                'form' => $form->createView(),
            ]
        ));
    }

    #[Route('/{id}/delete', name: 'delete')]
    public function delete(int $id): JsonResponse
    {
        try {
            $this->dataProvider->remove(new ($this->getItemClass())($id));
        } catch (OrienteeringManagerException $e) {
            return new JsonResponse(null, $e->getCode());
        }

        return new JsonResponse();
    }

    protected function generateBreadcrumb(?ApiItemInterface $entity = null): BreadcrumbModel
    {
        $breadcrumb = new BreadcrumbModel();
        $breadcrumb->addItem(
            $this->translator->trans('admin.dashboard.title', domain: 'admin'),
            $this->generateUrl('app_admin')
        );
        $breadcrumb->addItem(
            $this->translator->trans("admin.{$this->getType()}.list.title", domain: 'admin'),
            $this->generateUrl(sprintf('app_admin_%s_list', $this->getType())),
            active: null === $entity
        );

        if (null === $entity) {
            return $breadcrumb;
        }

        if (null === $entity->getId()) {
            $breadcrumb->addItem(
                $this->translator->trans("admin.{$this->getType()}.add.title", domain: 'admin'),
                active: true
            );
        } else {
            $breadcrumb->addItem(
                $this->translator->trans("admin.{$this->getType()}.edit.title", domain: 'admin'),
                active: true
            );
        }

        return $breadcrumb;
    }

    protected function getItem(Request $request, ?int $id = null): ?ApiItemInterface
    {
        if (null === $id) {
            return new ($this->getItemClass())();
        }

        $entity = null;

        try {
            $entity = $this->dataProvider->getItem(new ($this->getItemClass())($id, $this->getFilter($request)),
                ModeEnum::EDIT);
        } catch (OrienteeringManagerException $e) {
            $this->addFlashMessage('error', sprintf('orienteering_manager.error.request.item.%s', $this->getType()), $e);
        }

        return $entity;
    }
}
