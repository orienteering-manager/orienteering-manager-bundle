<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\Admin;

use Exception;
use OrienteeringManager\Config\Enum\AdminPageEnum;
use OrienteeringManager\Entity\EntityInterface;
use OrienteeringManager\Form\ImportType;
use OrienteeringManager\Manager\ExporterManager;
use OrienteeringManager\Manager\ImporterManager;
use OrienteeringManager\Model\Breadcrumb\BreadcrumbModel;
use OrienteeringManager\Repository\EntityRepositoryInterface;
use OrienteeringManager\Repository\PageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractEntityAdminController extends AbstractController
{
    public function __construct(
        protected readonly TranslatorInterface $translator,
        protected readonly EntityRepositoryInterface $repository,
        private readonly ExporterManager $exporterManager,
        private readonly ImporterManager $importerManager,
        private readonly PageRepository $pageRepository,
    ) {
    }

    protected abstract function getEntityName(): string;
    protected abstract function getEntityType(): string;
    protected abstract function getPageType(): ?string;

    #[Route('/', name: 'list')]
    public function list(): Response
    {
        $page = null !== $this->getPageType() ?
            $this->pageRepository->findOneBy([
                'type' => $this->getPageType(),
            ])
            : null;

        return $this->render($this->generateTemplatePath(AdminPageEnum::LIST), [
            'breadcrumb' => $this->generateBreadcrumb(),
            'entities' => $this->repository->findAll(),
            'page' => $page,
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/import', name: 'import')]
    public function import(Request $request): Response
    {
        $form = $this->createForm(ImportType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->importerManager->import($this->getEntityType(), $form->get('csv')->getData());

            return $this->redirectToRoute(sprintf('app_admin_%s_list', $this->getEntityName()));
        }

        return $this->render($this->generateTemplatePath(AdminPageEnum::IMPORT), [
            'breadcrumb' => $this->generateBreadcrumb(),
            'form' => $form,
        ]);
    }

    #[Route('/export', name: 'export')]
    public function export(): Response
    {
        $content = $this->exporterManager->export($this->getEntityType());

        $response = new Response($content);
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            sprintf('%s.csv', $this->getEntityName())
        );
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Encoding', 'UTF-8');
        $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');

        return $response;
    }

    protected function form(AdminPageEnum $adminPage, Request $request, string $formClass, mixed $data): Response
    {
        $form = $this->createForm($formClass, $data);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->handleForm($data, $form);
            $this->repository->save($data, true);

            return $this->redirectToRoute(sprintf('app_admin_%s_list', $this->getEntityName()));
        }

        return $this->render($this->generateTemplatePath($adminPage), [
            'breadcrumb' => $this->generateBreadcrumb($data),
            'form' => $form,
            'entity' => $data,
        ]);
    }

    protected function handleForm(mixed $data, FormInterface $form): void
    {
    }

    protected function generateTemplatePath(AdminPageEnum $adminPage): string
    {
        return sprintf('@OrienteeringManager/admin/%s/%s.html.twig', $this->getEntityName(), $adminPage->value);
    }

    protected function generateBreadcrumb(?EntityInterface $entity = null): BreadcrumbModel
    {
        $breadcrumb = new BreadcrumbModel();
        $breadcrumb->addItem(
            $this->translator->trans('admin.dashboard.title', domain: 'admin'),
            $this->generateUrl('app_admin')
        );
        $breadcrumb->addItem(
            $this->translator->trans("admin.{$this->getEntityName()}.list.title", domain: 'admin'),
            $this->generateUrl(sprintf('app_admin_%s_list', $this->getEntityName())),
            active: null === $entity
        );

        if (null === $entity) {
            return $breadcrumb;
        }

        if (null === $entity->getId()) {
            $breadcrumb->addItem(
                $this->translator->trans("admin.{$this->getEntityName()}.add.title", domain: 'admin'),
                active: true
            );
        } else {
            $breadcrumb->addItem(
                $this->translator->trans("admin.{$this->getEntityName()}.edit.title", domain: 'admin'),
                active: true
            );
        }

        return $breadcrumb;
    }
}
