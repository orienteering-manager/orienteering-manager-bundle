<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\Admin;

use OrienteeringManager\Builder\FilterBuilder\EventApiFilterBuilder;
use OrienteeringManager\Builder\UxComponentBuilder\MapUxComponentBuilder;
use OrienteeringManager\Config\Enum\ModeEnum;
use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Form\Model\OrienteeringManager\Filter\EventFilterType;
use OrienteeringManager\Form\Model\OrienteeringManager\Item\EventType;
use OrienteeringManager\Model\OrienteeringManager\Collection\EventApiCollection;
use OrienteeringManager\Model\OrienteeringManager\Filter\ApiFilterInterface;
use OrienteeringManager\Model\OrienteeringManager\Item\ApiItemInterface;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use OrienteeringManager\Repository\PageRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/event', name: 'app_admin_event_')]
final class EventAdminController extends AbstractApiItemAdminController
{
    public function __construct(
        private readonly MapUxComponentBuilder $mapUxComponentBuilder,
        private readonly EventApiFilterBuilder $eventApiFilterBuilder,
        TranslatorInterface $translator,
        OrienteeringManagerDataProvider $dataProvider,
        PageRepository $pageRepository,
        ValidatorInterface $validator
    ) {
        parent::__construct($translator, $dataProvider, $pageRepository, $validator);
    }

    protected function getType(): string
    {
        return 'event';
    }

    protected function getPageType(): string
    {
        return PageTypeEnum::EVENT_LIST->value;
    }

    protected function getCollectionClass(): string
    {
        return EventApiCollection::class;
    }

    protected function getItemClass(): string
    {
        return EventApiItem::class;
    }

    protected function getFormClass(): string
    {
        return EventType::class;
    }

    protected function getFilterFormClass(): ?string
    {
        return EventFilterType::class;
    }

    protected function getFilter(Request $request): ?ApiFilterInterface
    {
        $eventFilter = $this->eventApiFilterBuilder->buildFromRequest($request);
        $eventFilter->setMode(ModeEnum::EDIT);

        return $eventFilter;
    }

    /**
     * @param EventApiItem $entity
     */
    protected function getTemplateParameters(ApiItemInterface $entity): array
    {
        $parameters = parent::getTemplateParameters($entity);

        $location = $entity->getLocation();
        $parameters['map'] = $this->mapUxComponentBuilder->build($location);

        return $parameters;
    }
}
