<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\Admin;

use DateTime;
use OrienteeringManager\Config\Enum\EventRegistrationStatusEnum;
use OrienteeringManager\Config\Enum\ModeEnum;
use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\Form\Model\OrienteeringManager\Item\EventRegistrationType;
use OrienteeringManager\Model\OrienteeringManager\Collection\EventRegistrationApiCollection;
use OrienteeringManager\Model\OrienteeringManager\Filter\ApiFilterInterface;
use OrienteeringManager\Model\OrienteeringManager\Filter\EventRegistrationApiFilter;
use OrienteeringManager\Model\OrienteeringManager\Item\EventRegistrationApiItem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/admin/event-registration', name: 'app_admin_event_registration_')]
final class EventRegistrationAdminController extends AbstractApiItemAdminController
{
    protected function getType(): string
    {
        return 'event_registration';
    }

    protected function getPageType(): string
    {
        return PageTypeEnum::EVENT_LIST->value;
    }

    protected function getCollectionClass(): string
    {
        return EventRegistrationApiCollection::class;
    }

    protected function getItemClass(): string
    {
        return EventRegistrationApiItem::class;
    }

    protected function getFormClass(): string
    {
        return EventRegistrationType::class;
    }

    protected function getFilterFormClass(): ?string
    {
        return null;
    }

    protected function getFilter(Request $request): ?ApiFilterInterface
    {
        $eventRegistrationApiFilter = new EventRegistrationApiFilter();
        $eventRegistrationApiFilter->setStatus([
            EventRegistrationStatusEnum::CREATED,
            EventRegistrationStatusEnum::UPDATED,
            EventRegistrationStatusEnum::DELETED,
        ]);
        $eventRegistrationApiFilter->setEventDateAfter(new DateTime());
        $eventRegistrationApiFilter->setMode(ModeEnum::EDIT);

        return $eventRegistrationApiFilter;
    }
}
