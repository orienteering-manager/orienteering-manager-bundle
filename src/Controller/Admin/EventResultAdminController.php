<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\Admin;

use OrienteeringManager\Config\Enum\ModeEnum;
use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\Form\Model\OrienteeringManager\Item\EventResultType;
use OrienteeringManager\Model\OrienteeringManager\Collection\EventResultApiCollection;
use OrienteeringManager\Model\OrienteeringManager\Filter\ApiFilterInterface;
use OrienteeringManager\Model\OrienteeringManager\Filter\EventResultApiFilter;
use OrienteeringManager\Model\OrienteeringManager\Item\EventResultApiItem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/admin/event_result', name: 'app_admin_event_result_')]
final class EventResultAdminController extends AbstractApiItemAdminController
{
    protected function getType(): string
    {
        return 'event_result';
    }

    protected function getPageType(): string
    {
        return PageTypeEnum::EVENT_LIST->value;
    }

    protected function getCollectionClass(): string
    {
        return EventResultApiCollection::class;
    }

    protected function getItemClass(): string
    {
        return EventResultApiItem::class;
    }

    protected function getFormClass(): string
    {
        return EventResultType::class;
    }

    protected function getFilterFormClass(): ?string
    {
        return null;
    }

    protected function getFilter(Request $request): ?ApiFilterInterface
    {
        $eventResultFilter = new EventResultApiFilter();
        $eventResultFilter->setItemsPerPage(self::ITEMS_PER_PAGES);
        $eventResultFilter->setPage($request->query->getInt('page', 1));
        $eventResultFilter->setMode(ModeEnum::EDIT);

        return $eventResultFilter;
    }
}
