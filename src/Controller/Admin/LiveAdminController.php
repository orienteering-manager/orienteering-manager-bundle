<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\Admin;

use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Model\Breadcrumb\BreadcrumbModel;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/live', name: 'app_admin_live_')]
final class LiveAdminController extends AbstractController
{
    public function __construct(
        private readonly OrienteeringManagerDataProvider $dataProvider,
        private readonly TranslatorInterface $translator,
    ) {
    }

    /**
     * @throws OrienteeringManagerException
     */
    #[Route('/{id}', name: 'configuration')]
    public function configuration(Request $request, int $id): Response
    {
        /** @var EventApiItem $eventApiItem */
        $eventApiItem = $this->dataProvider->getItem(new EventApiItem($id));

        if ($request->isMethod(Request::METHOD_POST)) {
            $eventResultApiItem = $eventApiItem->getResult();
            $eventResultApiItem->setEvent($eventApiItem);
            $eventResultApiItem->setLiveFile($request->files->get('file'));

            $this->dataProvider->save($eventResultApiItem, true);

            return new JsonResponse(true);
        }

        return $this->render('@OrienteeringManager/admin/live/configuration.html.twig', [
            'breadcrumb' => $this->generateBreadcrumb(),
            'event' => $eventApiItem,
        ]);
    }

    protected function generateBreadcrumb(): BreadcrumbModel
    {
        $breadcrumb = new BreadcrumbModel();
        $breadcrumb->addItem(
            $this->translator->trans('admin.dashboard.title', domain: 'admin'),
            $this->generateUrl('app_admin')
        );
        $breadcrumb->addItem(
            $this->translator->trans("admin.live.configuration.title", domain: 'admin'),
            active: true
        );

        return $breadcrumb;
    }
}
