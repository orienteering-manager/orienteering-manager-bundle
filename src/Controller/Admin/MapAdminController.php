<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\Admin;

use OrienteeringManager\Builder\UxComponentBuilder\MapUxComponentBuilder;
use OrienteeringManager\Config\Enum\ModeEnum;
use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Form\Model\OrienteeringManager\Item\MapType;
use OrienteeringManager\Model\OrienteeringManager\Collection\MapApiCollection;
use OrienteeringManager\Model\OrienteeringManager\Filter\ApiFilterInterface;
use OrienteeringManager\Model\OrienteeringManager\Filter\MapApiFilter;
use OrienteeringManager\Model\OrienteeringManager\Item\ApiItemInterface;
use OrienteeringManager\Model\OrienteeringManager\Item\MapApiItem;
use OrienteeringManager\Repository\PageRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/map', name: 'app_admin_map_')]
final class MapAdminController extends AbstractApiItemAdminController
{
    public function __construct(
        private readonly MapUxComponentBuilder $mapUxComponentBuilder,
        TranslatorInterface $translator,
        OrienteeringManagerDataProvider $dataProvider,
        PageRepository $pageRepository,
        ValidatorInterface $validator
    ) {
        parent::__construct($translator, $dataProvider, $pageRepository, $validator);
    }

    protected function getType(): string
    {
        return 'map';
    }

    protected function getPageType(): string
    {
        return PageTypeEnum::MAP_LIST->value;
    }

    protected function getCollectionClass(): string
    {
        return MapApiCollection::class;
    }

    protected function getItemClass(): string
    {
        return MapApiItem::class;
    }

    protected function getFormClass(): string
    {
        return MapType::class;
    }

    protected function getFilterFormClass(): ?string
    {
        return null;
    }

    protected function getFilter(Request $request): ?ApiFilterInterface
    {
        $mapFilter = new MapApiFilter();
        $mapFilter->setItemsPerPage(self::ITEMS_PER_PAGES);
        $mapFilter->setPage($request->query->getInt('page', 1));
        $mapFilter->setMode(ModeEnum::EDIT);

        return $mapFilter;
    }

    /**
     * @param MapApiItem $entity
     */
    protected function getTemplateParameters(ApiItemInterface $entity): array
    {
        $parameters = parent::getTemplateParameters($entity);

        $parameters['map'] = $this->mapUxComponentBuilder->build($entity);

        return $parameters;
    }
}
