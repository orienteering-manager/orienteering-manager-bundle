<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\Admin;

use Exception;
use OrienteeringManager\Config\Enum\AdminPageEnum;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Form\Entity\PageType;
use OrienteeringManager\Manager\ExporterManager;
use OrienteeringManager\Manager\ImporterManager;
use OrienteeringManager\Manager\PageBuilderManager;
use OrienteeringManager\Repository\PageRepository;
use OrienteeringManager\Security\Voter\AbstractVoter;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/page', name: 'app_admin_page_')]
final class PageEntityAdminController extends AbstractEntityAdminController
{
    public function __construct(
        TranslatorInterface $translator,
        PageRepository $repository,
        ExporterManager $exporterManager,
        ImporterManager $importerManager,
        PageRepository $pageRepository,
        private readonly PageBuilderManager $pageBuilderService,
    ) {
        parent::__construct($translator, $repository, $exporterManager, $importerManager, $pageRepository);
    }

    protected function getEntityName(): string
    {
        return Page::ENTITY_NAME;
    }

    protected function getEntityType(): string
    {
        return Page::class;
    }

    protected function getPageType(): ?string
    {
        return null;
    }

    #[Route('/add', name: 'add')]
    public function add(Request $request): Response
    {
        $page = new Page();
        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->handleForm($page, $form);
            $this->repository->save($page, true);

            return $this->redirectToRoute('app_admin_page_edit', [
                'id' => $page->getId(),
            ]);
        }

        return $this->render($this->generateTemplatePath(AdminPageEnum::ADD), [
            'breadcrumb' => $this->generateBreadcrumb($page),
            'form' => $form,
            'entity' => $page,
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/{id}/edit', name: 'edit')]
    public function edit(Request $request, Page $page): Response
    {
        if (!$this->isGranted(AbstractVoter::EDIT, $page)) {
            throw $this->createAccessDeniedException();
        }

        $this->pageBuilderService->build($page);

        return parent::form(AdminPageEnum::EDIT, $request, PageType::class, $page);
    }

    /**
     * @param Page $data
     * @throws ExceptionInterface
     */
    protected function handleForm(mixed $data, FormInterface $form): void
    {
        parent::handleForm($data, $form);

        $this->pageBuilderService->handle($data, $form);
    }

    #[Route('/{id}/delete', name: 'delete')]
    public function delete(Page $page): JsonResponse
    {
        if (!$this->isGranted(AbstractVoter::DELETE, $page)) {
            throw $this->createAccessDeniedException();
        }

        $this->repository->remove($page, true);

        return new JsonResponse();
    }
}
