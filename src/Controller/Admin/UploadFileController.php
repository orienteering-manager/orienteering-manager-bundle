<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\Admin;

use OrienteeringManager\Manager\FileManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/admin/file')]
final class UploadFileController extends AbstractController
{
    public function __construct(
        private readonly FileManager $fileManager,
    ) {
    }

    #[Route('/upload', name: 'app_admin_file_upload')]
    public function upload(Request $request): Response
    {
        $files = [];
        $type = sprintf('media/%s', $request->query->get('type'));

        foreach ($request->files as $file) {
            $imagePath = $this->fileManager->upload(
                $file,
                $type,
                sprintf('content-%s', uniqid()),
            );
            if (null === $imagePath) {
                continue;
            }

            $files[] = [
                'uploaded' => 1,
                'url' => $imagePath,
            ];
        }

        return new JsonResponse($files);
    }
}
