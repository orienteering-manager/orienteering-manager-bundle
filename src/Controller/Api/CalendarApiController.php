<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\Api;

use OrienteeringManager\Builder\CalendarBuilder\CalendarBuilder;
use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Model\OrienteeringManager\Collection\EventApiCollection;
use OrienteeringManager\Model\OrienteeringManager\Filter\EventApiFilter;
use OrienteeringManager\Repository\PageRepository;
use OrienteeringManager\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/api/calendar', name: 'api_calendar_')]
final class CalendarApiController extends AbstractController
{
    public function __construct(
        private readonly OrienteeringManagerDataProvider $dataProvider,
        private readonly CalendarBuilder $calendarBuilder,
        private readonly PageRepository $pageRepository,
        private readonly UserRepository $userRepository,
    ) {
    }

    /**
     * @throws OrienteeringManagerException
     */
    #[Route('/{id}/events.ics', name: 'export', requirements: ['id' => '\d+'])]
    public function calendarIcs(int $id): Response {
        $page = $this->pageRepository->findOneBy([
            'type' => PageTypeEnum::EVENT_LIST,
        ]);

        $user = $this->userRepository->find($id);
        if (null === $user) {
            throw new $this->createAccessDeniedException();
        }

        $eventApiFilter = new EventApiFilter();
        $eventApiFilter->setFederationMemberId($user->getFederationMember()?->getId());
        $eventsApiItem = $this->dataProvider->getCollection(new EventApiCollection())->getItems();

        $response = new Response($this->calendarBuilder->build($page, $eventsApiItem)->get());
        $response->headers->set('Content-Type', 'text/calendar');
        $response->headers->set('Content-Disposition', 'attachment; filename=events.ics');

        return $response;
    }
}