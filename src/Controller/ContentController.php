<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller;

use OrienteeringManager\Entity\Page;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/{page}', name: 'app_content')]
final class ContentController extends AbstractController
{
    public function __construct(
        #[TaggedIterator('orienteering_manager.content_renderer')]
        private readonly iterable $contentRenderers,
    ) {
    }

    #[Route('/{id}-{slug}/', name: '_view', requirements: ['id' => '\d+'], priority: 2)]
    #[Route('/', name: '_page')]
    #[Route('/{category}/', name: '_category')]
    #[Route(
        '/{category}/{id}-{slug}.html',
        name: '_category_view',
        requirements: ['id' => '\d+'],
        priority: 2,
    )]
    public function view(
        Request $request,
        #[MapEntity(mapping: ['page' => 'slug'])]
        Page $page,
        ?string $category = null,
        ?int $id = null,
    ): Response {
        $childIdentifiers = [];
        if (is_string($category)) {
            $childIdentifiers[] = $category;
        }
        if (is_int($id)) {
            $childIdentifiers[] = $id;
        }

        foreach ($this->contentRenderers as $contentRenderer) {
            if (!$contentRenderer->supports($request, $page, $childIdentifiers)) {
                continue;
            }

            return new Response($contentRenderer->render($request, $page, $childIdentifiers));
        }

        throw $this->createNotFoundException();
    }
}
