<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller;

use OrienteeringManager\Builder\CalendarBuilder\CalendarBuilder;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Form\Model\OrienteeringManager\Collection\EventRegistrationsType;
use OrienteeringManager\Manager\FileManager;
use OrienteeringManager\Model\OrienteeringManager\Collection\EventRegistrationApiCollection;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventFileApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventRegistrationApiItem;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/{page}', name: 'app_event_')]
final class EventController extends AbstractController
{
    public function __construct(
        private readonly OrienteeringManagerDataProvider $dataProvider,
        private readonly FileManager $fileManager,
        private readonly CalendarBuilder $calendarBuilder,
    ) {
    }

    /**
     * @throws OrienteeringManagerException
     */
    #[Route(
        '/{id}-{slug}/{fileId}',
        name: 'download_file',
        requirements: ['id' => '\d+', 'fileId' => '\d+'],
        priority: 2
    )]
    public function download(int $fileId): Response {
        /** @var EventFileApiItem $eventFile */
        $eventFile = $this->dataProvider->getItem(new EventFileApiItem($fileId));

        return $this->fileManager->base64ToResponse($eventFile->getContent(), $eventFile->getName());
    }

    /**
     * @throws OrienteeringManagerException
     */
    #[Route('/{id}-{slug}/inscription.html', name: 'register', requirements: ['id' => '\d+'])]
    public function register(
        Request $request,
        #[MapEntity(mapping: ['page' => 'slug'])]
        Page $page,
        int $id
    ): Response {
        /** @var EventApiItem $event */
        $event = $this->dataProvider->getItem(new EventApiItem($id));
        if (!$this->isGranted('REGISTER', $event)) {
            throw $this->createAccessDeniedException();
        }

        $breadcrumb = $page->getBreadcrumb();
        $breadcrumb->addItem($event->getName(), url: $this->generateUrl('app_content_view', [
            'page' => $page->getSLug(),
            'id' => $event->getId(),
            'slug' => $event->getSlug(),
        ]));
        $breadcrumb->addItem('Inscription', active: true);

        $eventRegistrations = new EventRegistrationApiCollection();
        $eventRegistrations->addItem(
            (new EventRegistrationApiItem())
                ->setEvent($event)
        );
        $form = $this->createForm(EventRegistrationsType::class, $eventRegistrations, [
            'event' => $event,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $registrationsStatus = [];
            foreach ($eventRegistrations->getItems() as $eventRegistration) {
                $savedEventRegistration = clone $eventRegistration;

                $eventRegistration->setEvent($event->getIri());
                $eventRegistration->setFederationMember($eventRegistration->getFederationMemberIri());
                $eventRegistration->setTrack($eventRegistration->getTrackIri());

                try {
                    $registrationsStatus[] = [
                        'status' => 'success',
                        'object' => $this->dataProvider->save($eventRegistration),
                    ];
                } catch (OrienteeringManagerException $e) {
                    $registrationsStatus[] = [
                        'status' => 'error',
                        'object' => $savedEventRegistration,
                        'error' => $e,
                    ];
                }
            }

            return $this->render('@OrienteeringManager/content/registration/confirmation.html.twig', [
                'page' => $page,
                'breadcrumb' => $breadcrumb,
                'event' => $event,
                'registrations' => $registrationsStatus,
            ]);
        }

        return $this->render('@OrienteeringManager/content/registration/form.html.twig', [
            'page' => $page,
            'breadcrumb' => $breadcrumb,
            'event' => $event,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @throws OrienteeringManagerException
     */
    #[Route('/{id}-{slug}/event.ics', name: 'export', requirements: ['id' => '\d+'])]
    public function export(
        #[MapEntity(mapping: ['page' => 'slug'])]
        Page $page,
        int $id,
    ): Response {
        $eventApiItem = $this->dataProvider->getItem(new EventApiItem($id));
        if (!$eventApiItem instanceof EventApiItem) {
            throw $this->createAccessDeniedException();
        }

        $response = new Response($this->calendarBuilder->build($page, [$eventApiItem])->get());
        $response->headers->set('Content-Type', 'text/calendar');
        $response->headers->set('Content-Disposition', sprintf('attachment; filename=%s.ics', $eventApiItem->getName()));

        return $response;
    }
}
