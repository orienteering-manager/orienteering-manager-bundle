<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller;

use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Form\Model\OrienteeringManager\Item\EventRegistrationType;
use OrienteeringManager\Model\OrienteeringManager\Collection\EventRegistrationApiCollection;
use OrienteeringManager\Model\OrienteeringManager\Filter\EventRegistrationApiFilter;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventRegistrationApiItem;
use OrienteeringManager\Security\Voter\AbstractVoter;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

#[Route('/event-registration', name: 'app_event_registration_')]
final class EventRegistrationController extends AbstractController
{
    public function __construct(
        private readonly OrienteeringManagerDataProvider $dataProvider,
    ) {
    }

    /**
     * @throws ClientExceptionInterface
     * @throws OrienteeringManagerException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route(
        '/{id}/export/{format}',
        name: 'export',
        requirements: [
            'id' => '\d+',
        ],
    )]
    public function view(
        int $id,
        string $format,
    ): Response {
        /** @var EventApiItem $event */
        $event = $this->dataProvider->getItem(new EventApiItem($id));

        $eventRegistrationFilter = new EventRegistrationApiFilter();
        $eventRegistrationFilter->setEvent($event->getIri());

        return $this->dataProvider->export(
            new EventRegistrationApiCollection($eventRegistrationFilter),
            $format
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws OrienteeringManagerException
     */
    #[Route('/{page}/{event}-{slug}/modification-inscription/{id}.html', name: 'edit', requirements: ['event' => '\d+'])]
    public function edit(
        Request $request,
        #[MapEntity(mapping: ['page' => 'slug'])]
        Page $page,
        int $id
    ): Response {
        /** @var EventRegistrationApiItem $eventRegistration */
        $eventRegistration = $this->dataProvider->getItem(new EventRegistrationApiItem($id));
        if (!$this->isGranted(AbstractVoter::REGISTER, $eventRegistration->getEvent())) {
            throw $this->createAccessDeniedException();
        }

        $event = $eventRegistration->getEvent();

        $breadcrumb = $page->getBreadcrumb();
        $breadcrumb->addItem($event->getName(), url: $this->generateUrl('app_content_view', [
            'page' => $page->getSLug(),
            'id' => $event->getId(),
            'slug' => $event->getSlug(),
        ]));
        $breadcrumb->addItem('Modification de l\'inscription', active: true);

        $form = $this->createForm(EventRegistrationType::class, $eventRegistration, [
            'event' => $event,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $eventRegistration->setFederationMember($eventRegistration->getFederationMemberIri());
            $eventRegistration->setTrack($eventRegistration->getTrackIri());

            try {
                $registrationsStatus[] = [
                    'status' => 'success',
                    'object' => $this->dataProvider->save($eventRegistration),
                ];
            } catch (OrienteeringManagerException $e) {
                $registrationsStatus[] = [
                    'status' => 'error',
                    'object' => $e->getObject(),
                    'error' => $e,
                ];
            }

            return $this->render('@OrienteeringManager/content/registration/confirmation.html.twig', [
                'page' => $page,
                'event' => $event,
                'registrations' => $registrationsStatus,
            ]);
        }

        return $this->render('@OrienteeringManager/content/registration/edit.html.twig', [
            'page' => $page,
            'event' => $event,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws OrienteeringManagerException
     */
    #[Route('/{page}/{event}-{slug}/suppression-inscription/{id}.html', name: 'delete', requirements: ['event' => '\d+'])]
    public function delete(int $id): JsonResponse
    {
        /** @var EventRegistrationApiItem $eventRegistration */
        $eventRegistration = $this->dataProvider->getItem(new EventRegistrationApiItem($id));
        if (!$this->isGranted(AbstractVoter::DELETE, $eventRegistration)) {
            throw $this->createAccessDeniedException();
        }

        $this->dataProvider->remove($eventRegistration);

        return new JsonResponse();
    }
}
