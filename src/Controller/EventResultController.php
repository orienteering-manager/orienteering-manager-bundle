<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller;

use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventResultApiItem;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/{page}', name: 'app_event_result_')]
final class EventResultController extends AbstractController
{
    public function __construct(
        private readonly OrienteeringManagerDataProvider $dataProvider,
    ) {
    }

    /**
     * @throws OrienteeringManagerException
     */
    #[Route('/{id}-{slug}/resultats.html', name: 'view', requirements: ['id' => '\d+'])]
    public function register(
        #[MapEntity(mapping: ['page' => 'slug'])]
        Page $page,
        int $id
    ): Response {
        /** @var EventApiItem $event */
        $event = $this->dataProvider->getItem(new EventApiItem($id));
        if (!($eventResult = $event->getResult()) instanceof EventResultApiItem) {
            throw $this->createNotFoundException();
        }

        $breadcrumb = $page->getBreadcrumb();
        $breadcrumb->addItem($event->getName(), url: $this->generateUrl('app_content_view', [
            'page' => $page->getSLug(),
            'id' => $event->getId(),
            'slug' => $event->getSlug(),
        ]));
        $breadcrumb->addItem('Résultats', active: true);

        return $this->render($eventResult->getTemplate(), [
            'page' => $page,
            'breadcrumb' => $breadcrumb,
            'item' => $eventResult->setEvent($event),
        ]);
    }
}
