<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller;

use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Model\OrienteeringManager\Collection\FederationMemberApiCollection;
use OrienteeringManager\Model\OrienteeringManager\Filter\FederationMemberApiFilter;
use OrienteeringManager\Model\OrienteeringManager\Item\FederationMemberApiItem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/federation-members', name: 'orienteering_manager_federation_member_')]
final class FederationMemberController extends AbstractController
{
    public function __construct(
        private readonly OrienteeringManagerDataProvider $dataProvider,
    ) {
    }

    #[Route('/autocomplete', name: 'autocomplete', priority: 10)]
    public function register(Request $request): JsonResponse
    {
        $data = [];

        $federationMemberFilter = new FederationMemberApiFilter();
        $federationMemberFilter
            ->setQuery($request->query->get('query'))
            ->setPage($request->query->getInt('page', 1))
        ;

        try {
            $federationMembers = $this->dataProvider->getCollection(new FederationMemberApiCollection($federationMemberFilter))->getItems();
        } catch (OrienteeringManagerException) {}

        /** @var FederationMemberApiItem $federationMember */
        foreach ($federationMembers ?? [] as $federationMember) {
            $data[] = [
                'value' => $federationMember->getIri(),
                'text' => sprintf(
                    '%s - %s %s #%s',
                    $federationMember->getClub()?->getId(),
                    $federationMember->getLastName(),
                    $federationMember->getFirstName(),
                    $federationMember->getId(),
                ),
                'firstName' => $federationMember->getFirstName(),
                'lastName' => $federationMember->getLastName(),
                'cardNumber' => $federationMember->getCardNumber(),
            ];
        }

        return new JsonResponse([
            'results' => $data,
            'next_page' => !empty($data) ? $this->generateUrl('orienteering_manager_federation_member_autocomplete', [
                'query' => $request->query->get('query'),
                'page' => $request->query->getInt('page', 1) + 1
            ]) : false,
        ]);
    }
}
