<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller;

use OrienteeringManager\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

final class HelpController extends AbstractController
{
    public function __construct(
    ) {
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    #[Route('/page/comment-utiliser-application-vikazimut', name: 'app_help_vikazimut', priority: 10)]
    public function helpVikazimut(Request $request, Page $page): Response
    {
        return $this->render('@OrienteeringManager/help/vikazimut.html.twig');
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    #[Route('/page/apprendre-lire-carte', name: 'app_help_learning_read_map', priority: 10)]
    public function helpLearningReadMap(Request $request, Page $page): Response
    {
        return $this->render('@OrienteeringManager/help/learning-read-map.html.twig');
    }
}
