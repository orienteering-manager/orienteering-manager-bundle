<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\LiveComponent\ApiItem;

use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Model\OrienteeringManager\Item\ApiItemInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

abstract class AbstractApiItemLiveComponentController extends AbstractController
{
    use DefaultActionTrait;

    #[LiveProp]
    public ?Page $page = null;

    #[LiveProp]
    public bool $deleted = false;

    public function __construct(
        protected readonly OrienteeringManagerDataProvider $dataProvider,
    ) {
    }

    abstract function getItem(): ApiItemInterface;

    #[LiveAction]
    public function removeItem(): void
    {
        try {
            $this->dataProvider->remove($this->getItem());
            $this->deleted = true;
        } catch (OrienteeringManagerException) {
            $this->deleted = false;
            // @TODO Add error message and display it in twig.
        }
    }
}