<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\LiveComponent\ApiItem;

use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Model\OrienteeringManager\Item\ApiItemInterface;
use OrienteeringManager\Model\OrienteeringManager\Item\EventRegistrationApiItem;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;

#[AsLiveComponent(name: 'AdminEventRegistration', template: '@OrienteeringManager/admin/event_registration/_list_item.html.twig')]
final class AdminEventRegistrationLiveComponentController extends AbstractApiItemLiveComponentController
{
    #[LiveProp(
        useSerializerForHydration: true,
    )]
    public EventRegistrationApiItem $item;

    #[LiveProp]
    public bool $validated = false;

    public function getItem(): ApiItemInterface
    {
        return $this->item;
    }

    #[LiveAction]
    public function validateItem(): void
    {
        try {
            $this->dataProvider->customPatchOperation($this->getItem(), 'validate');
            $this->validated = true;
        } catch (OrienteeringManagerException) {
            // @TODO Add error message and display it in twig.
        }
    }
}
