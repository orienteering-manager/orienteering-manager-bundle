<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\LiveComponent\ApiItem;

use OrienteeringManager\Model\OrienteeringManager\Item\ApiItemInterface;
use OrienteeringManager\Model\OrienteeringManager\Item\MapApiItem;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveProp;

#[AsLiveComponent(name: 'AdminMap', template: '@OrienteeringManager/admin/map/_list_item.html.twig')]
final class AdminMapLiveComponentController extends AbstractApiItemLiveComponentController
{
    #[LiveProp(
        useSerializerForHydration: true,
    )]
    public MapApiItem $item;

    public function getItem(): ApiItemInterface
    {
        return $this->item;
    }
}
