<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\LiveComponent\ApiItem;

use OrienteeringManager\Model\OrienteeringManager\Item\ApiItemInterface;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveProp;

#[AsLiveComponent(name: 'AdminEvent', template: '@OrienteeringManager/admin/event/_list_item.html.twig')]
final class EventLiveComponentController extends AbstractApiItemLiveComponentController
{
    #[LiveProp(
        useSerializerForHydration: true,
    )]
    public EventApiItem $item;

    public function getItem(): ApiItemInterface
    {
        return $this->item;
    }
}
