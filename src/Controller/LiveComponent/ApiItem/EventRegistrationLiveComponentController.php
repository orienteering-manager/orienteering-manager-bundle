<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\LiveComponent\ApiItem;

use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Model\OrienteeringManager\Item\ApiItemInterface;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventRegistrationApiItem;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;

#[AsLiveComponent(name: 'EventRegistration', template: '@OrienteeringManager/content/registration/_list_item.html.twig')]
final class EventRegistrationLiveComponentController extends AbstractApiItemLiveComponentController
{
    #[LiveProp(
        useSerializerForHydration: true,
    )]
    public EventRegistrationApiItem $item;

    #[LiveProp(
        useSerializerForHydration: true,
    )]
    public EventApiItem $event;

    #[LiveProp]
    public bool $validated = false;

    public function getItem(): ApiItemInterface
    {
        return $this->item;
    }

    #[LiveAction]
    public function removeItem(): void
    {
        try {
            $this->dataProvider->remove($this->item);
            $this->deleted = true;
        } catch (OrienteeringManagerException) {
            // @TODO Add error message and display it in twig.
        }
    }
}
