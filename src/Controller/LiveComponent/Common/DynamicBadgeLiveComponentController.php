<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\LiveComponent\Common;

use OrienteeringManager\Config\Constant\ColorConstant;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent(name: 'DynamicBadge', template: '@OrienteeringManager/live_component/common/dynamic_badge.html.twig')]
final class DynamicBadgeLiveComponentController extends AbstractController
{
    use DefaultActionTrait;

    #[LiveProp]
    public string $color = ColorConstant::PRIMARY;
    
    #[LiveProp]
    public string $icon;

    #[LiveProp]
    public string $text;

    #[LiveProp(
        writable: true,
    )]
    public bool $displayText = false;

    #[LiveProp]
    public string $tooltip;

    public function handleClick(): void
    {
        $this->displayText = !$this->displayText;
    }
}
