<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\LiveComponent\Entity;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

abstract class AbstractEntityLiveComponentController extends AbstractController
{
    use DefaultActionTrait;

    #[LiveProp]
    public bool $deleted = false;

    abstract function getEntity(): mixed;

    #[LiveAction]
    abstract public function removeItem(): void;
}