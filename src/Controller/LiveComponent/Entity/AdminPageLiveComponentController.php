<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller\LiveComponent\Entity;

use OrienteeringManager\Entity\Page;
use OrienteeringManager\Repository\PageRepository;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;

#[AsLiveComponent(name: 'AdminPage', template: '@OrienteeringManager/admin/page/_list_item.html.twig')]
final class AdminPageLiveComponentController extends AbstractEntityLiveComponentController
{
    #[LiveProp]
    public Page $entity;

    public function __construct(
        private readonly PageRepository $pageRepository,
    ) {
    }

    public function getEntity(): Page
    {
        return $this->entity;
    }

    #[LiveAction]
    public function removeItem(): void
    {
        $this->pageRepository->remove($this->entity, true);
        $this->deleted = true;
    }
}
