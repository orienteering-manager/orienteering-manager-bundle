<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller;

use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Model\OrienteeringManager\Collection\MapApiCollection;
use OrienteeringManager\Model\OrienteeringManager\Filter\MapApiFilter;
use OrienteeringManager\Model\OrienteeringManager\Item\MapApiItem;
use OrienteeringManager\Model\Page\RootContentPageModelInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/{page}', name: 'app_map_')]
final class MapController extends AbstractController
{
    public function __construct(
        private readonly OrienteeringManagerDataProvider $dataProvider,
    ) {
    }

    /**
     * @throws OrienteeringManagerException
     */
    #[Route(
        '/{city}.html',
        name: 'list_by_city',
        priority: 2
    )]
    public function listByCity(
        #[MapEntity(mapping: ['page' => 'slug'])]
        Page $page,
        string $city,
    ): Response {
        $data = $page->getData();
        if (!$data instanceof RootContentPageModelInterface) {
            throw $this->createNotFoundException();
        }

        $mapApiFilter = new MapApiFilter();
        $mapApiFilter->setCity($city);

        /** @var MapApiItem[] $maps */
        $maps = $this->dataProvider->getCollection(new MapApiCollection($mapApiFilter))->getItems();

        $firstMap = current($maps);
        if (!$firstMap instanceof MapApiItem) {
            throw $this->createNotFoundException();
        }

        $breadcrumb = $page->getBreadcrumb();
        $breadcrumb->addItem($firstMap->getCity(), active: true);

        return $this->render('@OrienteeringManager/content/map/list_by_city.html.twig', [
            'page' => $page,
            'breadcrumb' => $breadcrumb,
            'city' => $maps[0]->getCity(),
            'maps' => $maps,
        ]);
    }
}
