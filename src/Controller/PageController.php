<?php

declare(strict_types=1);

namespace OrienteeringManager\Controller;

use OrienteeringManager\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

final class PageController extends AbstractController
{
    public function __construct(
        #[TaggedIterator('orienteering_manager.content_renderer')]
        private readonly iterable $contentRenderers,
    ) {
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    #[Route('/{slug}/', name: 'app_page')]
    public function view(Request $request, Page $page): Response
    {
        foreach ($this->contentRenderers as $contentRenderer) {
            if (!$contentRenderer->supports($request, $page, null)) {
                continue;
            }

            return new Response($contentRenderer->render($request, $page, null));
        }

        throw $this->createNotFoundException();
    }
}
