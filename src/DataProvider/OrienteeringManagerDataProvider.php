<?php

declare(strict_types=1);

namespace OrienteeringManager\DataProvider;

use OrienteeringManager\Config\Enum\ModeEnum;
use OrienteeringManager\Event\OrienteeringManagerEvents;
use OrienteeringManager\Event\OrienteeringManagerPreSaveEvent;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Manager\OrienteeringManagerInterface;
use OrienteeringManager\Model\OrienteeringManager\Collection\ApiCollectionInterface;
use OrienteeringManager\Model\OrienteeringManager\Collection\ExportableApiCollectionInterface;
use OrienteeringManager\Model\OrienteeringManager\Item\ApiItemInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final readonly class OrienteeringManagerDataProvider
{
    /**
     * @param Serializer $serializer
     */
    public function __construct(
        private SerializerInterface $serializer,
        private OrienteeringManagerInterface $orienteeringManager,
        private EventDispatcherInterface $eventDispatcher,
        private Security $security,
    ) {
    }

    /**
     * @throws OrienteeringManagerException
     */
    public function getCollection(ApiCollectionInterface $collection): ApiCollectionInterface
    {
        $options = [];
        if (null !== $collection->getFilter()) {
            try {
                $options['query'] = $this->serializer->normalize($collection->getFilter());
            } catch (ExceptionInterface $e) {
                throw new OrienteeringManagerException(
                    object: $collection,
                    message: $e->getMessage(),
                    code: $e->getCode(),
                    previous: $e,
                );
            }
        }

        if (null !== $this->security) {
            $user = $this->security->getUser();
            $options['query']['currentUserFederationMemberId'] = $user?->getFederationMember()?->getId();
        }

        $response = $this->orienteeringManager->request(Request::METHOD_GET, $collection->getIri(), $options);

        return $this->serializer->deserialize($response, $collection::class, JsonEncoder::FORMAT);
    }

    /**
     * @throws OrienteeringManagerException
     */
    public function getItem(ApiItemInterface $item, ?ModeEnum $mode = null): ApiItemInterface
    {
        $options = [];
        if (null !== $mode) {
            $options['query'] = [
                'mode' => $mode->value
            ];
        }

        if (null !== $this->security) {
            $user = $this->security->getUser();
            $options['query']['currentUserFederationMemberId'] = $user?->getFederationMember()?->getId();
        }

        try {
            $response = $this->orienteeringManager->request(Request::METHOD_GET, $item->getIri(), $options);
        } catch (OrienteeringManagerException $e) {
            $e->setObject($item);

            throw $e;
        }

        return $this->serializer->deserialize($response, $item::class, JsonEncoder::FORMAT);
    }

    /**
     * @throws OrienteeringManagerException
     */
    public function save(ApiItemInterface $item, bool $partial = false): ApiItemInterface
    {
        try {
            $preSaveEvent = new OrienteeringManagerPreSaveEvent($item);
            $this->eventDispatcher->dispatch($preSaveEvent, OrienteeringManagerEvents::PRE_SAVE);

            $response = $this->orienteeringManager->request(
                null === $item->getId() ? Request::METHOD_POST : Request::METHOD_PUT,
                null === $item->getId() ? sprintf('/api/%s', $item::getApiName()) : $item->getIri(),
                ['body' => $this->serializer->serialize($item, JsonEncoder::FORMAT, [
                    AbstractObjectNormalizer::SKIP_NULL_VALUES => $partial,
                    AbstractNormalizer::GROUPS => [
                        $item::API_NORMALIZER_GROUP,
                    ]
                ])],
            );

            $object = $this->serializer->deserialize($response, $item::class, JsonEncoder::FORMAT);
        } catch (OrienteeringManagerException $e) {
            $e->setObject($item);

            throw $e;
        }

        return $object;
    }

    /**
     * @throws OrienteeringManagerException
     */
    public function remove(ApiItemInterface $item): void
    {
        try {
            $this->orienteeringManager->request(Request::METHOD_DELETE, $item->getIri());
        } catch (OrienteeringManagerException $e) {
            $e->setObject($item);

            throw $e;
        }
    }

    /**
     * @throws ClientExceptionInterface
     * @throws OrienteeringManagerException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function export(ExportableApiCollectionInterface $collection, string $format): Response
    {
        /** @var ResponseInterface $dataResponse */
        $dataResponse = $this->orienteeringManager->request(
            Request::METHOD_GET,
            $collection->getExportIri().$format,
            fullResponse: true,
        );

        $response = new Response($dataResponse->getContent());
        $response->headers->set('Content-Type', $dataResponse->getHeaders()['content-type']);

        return $response;
    }

    /**
     * @throws OrienteeringManagerException
     */
    public function customPutOperation(ApiItemInterface $item, string $action): void
    {
        try {
            $this->orienteeringManager->request(
                Request::METHOD_PUT,
                sprintf('%s/%s', $item->getIri(), $action),
                ['body' => $this->serializer->serialize($item, JsonEncoder::FORMAT, [
                    AbstractObjectNormalizer::SKIP_NULL_VALUES => true
                ])],
            );
        } catch (OrienteeringManagerException $e) {
            $e->setObject($item);

            throw $e;
        }
    }

    /**
     * @throws OrienteeringManagerException
     */
    public function customPatchOperation(ApiItemInterface $item, string $action): void
    {
        try {
            $this->orienteeringManager->request(
                Request::METHOD_PATCH,
                sprintf('%s/%s', $item->getIri(), $action),
                ['body' => $this->serializer->serialize($item, JsonEncoder::FORMAT, [
                    AbstractObjectNormalizer::SKIP_NULL_VALUES => true
                ])],
            );
        } catch (OrienteeringManagerException $e) {
            $e->setObject($item);

            throw $e;
        }
    }
}
