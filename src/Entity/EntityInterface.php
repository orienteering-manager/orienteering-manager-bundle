<?php

declare(strict_types=1);

namespace OrienteeringManager\Entity;

interface EntityInterface
{
    public const string EXPORT_FIELD_GROUPS = 'export.group';

    public function getId(): ?int;
}