<?php

declare(strict_types=1);

namespace OrienteeringManager\Entity;

use Doctrine\ORM\Mapping as ORM;
use OrienteeringManager\Model\Breadcrumb\BreadcrumbModel;
use OrienteeringManager\Model\Page\PageModelInterface;
use OrienteeringManager\Model\Sitemap\Url;
use OrienteeringManager\Repository\PageRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: PageRepository::class)]
#[UniqueEntity('slug')]
#[ORM\HasLifecycleCallbacks]
class Page implements EntityInterface
{
    use SlugTrait;

    final public const string ENTITY_NAME = 'page';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $icon = null;

    #[ORM\Column(length: 255, nullable: false)]
    private ?string $type = null;

    #[ORM\Column(nullable: true)]
    private array $content = [];

    #[ORM\Column(nullable: true)]
    private ?bool $inNavigation = false;

    #[ORM\Column(nullable: true)]
    private ?int $navigationPosition = null;

    private ?PageModelInterface $data = null;
    private ?BreadcrumbModel $breadcrumb = null;
    private ?Url $sitemapUrl = null;
    private bool $isRequired = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getContent(): array
    {
        return $this->content;
    }

    public function setContent(?array $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getData(): ?PageModelInterface
    {
        return $this->data;
    }

    public function setData(?PageModelInterface $data): void
    {
        $this->data = $data;
    }

    public function isInNavigation(): ?bool
    {
        return $this->inNavigation;
    }

    public function setInNavigation(?bool $inNavigation): self
    {
        $this->inNavigation = $inNavigation;

        return $this;
    }

    public function getNavigationPosition(): ?int
    {
        return $this->navigationPosition;
    }

    public function setNavigationPosition(?int $navigationPosition): self
    {
        $this->navigationPosition = $navigationPosition;

        return $this;
    }

    public function getBreadcrumb(): ?BreadcrumbModel
    {
        return $this->breadcrumb;
    }

    public function setBreadcrumb(?BreadcrumbModel $breadcrumb): self
    {
        $this->breadcrumb = $breadcrumb;

        return $this;
    }

    public function getSitemapUrl(): ?Url
    {
        return $this->sitemapUrl;
    }

    public function setSitemapUrl(?Url $sitemapUrl): self
    {
        $this->sitemapUrl = $sitemapUrl;

        return $this;
    }

    public function isRequired(): bool
    {
        return $this->isRequired;
    }

    public function setIsRequired(bool $isRequired): self
    {
        $this->isRequired = $isRequired;

        return $this;
    }
}
