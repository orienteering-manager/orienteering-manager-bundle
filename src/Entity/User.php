<?php

declare(strict_types=1);

namespace OrienteeringManager\Entity;

use OrienteeringManager\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use OrienteeringManager\Model\OrienteeringManager\Item\FederationMemberApiItem;
use OrienteeringManager\Security\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[UniqueEntity(fields: ['email'])]
class User implements UserInterface, PasswordAuthenticatedUserInterface, EntityInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    #[ORM\Column]
    private ?string $password = null;

    #[ORM\Column(nullable: true)]
    private ?string $federationMemberIri = null;
    private ?FederationMemberApiItem $federationMember = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
    }

    public function getFederationMemberIri(): ?string
    {
        return $this->federationMemberIri;
    }

    public function setFederationMemberIri(?string $federationMemberIri): self
    {
        $this->federationMemberIri = $federationMemberIri;

        return $this;
    }

    public function getFederationMember(): ?FederationMemberApiItem
    {
        return $this->federationMember;
    }

    public function setFederationMember(?FederationMemberApiItem $federationMember): void
    {
        $this->federationMember = $federationMember;
    }
}
