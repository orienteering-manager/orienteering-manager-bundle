<?php

namespace OrienteeringManager\Event;

final class OrienteeringManagerEvents
{
    final public const string PRE_SAVE = 'orienteering_manager.pre_save';

    private function __construct()
    {
    }
}