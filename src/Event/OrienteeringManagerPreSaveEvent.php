<?php

namespace OrienteeringManager\Event;

use OrienteeringManager\Model\OrienteeringManager\Item\ApiItemInterface;
use Symfony\Contracts\EventDispatcher\Event;

final class OrienteeringManagerPreSaveEvent extends Event
{
    public function __construct(
        private readonly ApiItemInterface|null $item = null,
    ) {
    }

    public function getItem(): ApiItemInterface
    {
        return $this->item;
    }
}