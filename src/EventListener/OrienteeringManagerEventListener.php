<?php

namespace OrienteeringManager\EventListener;

use OrienteeringManager\Event\OrienteeringManagerEvents;
use OrienteeringManager\Event\OrienteeringManagerPreSaveEvent;
use OrienteeringManager\Processor\Uploadable\UploadableProcessor;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener(event: OrienteeringManagerEvents::PRE_SAVE, method: 'preSave')]
final readonly class OrienteeringManagerEventListener
{
    public function __construct(
        private UploadableProcessor $uploadableProcessor,
    ) {
    }

    public function preSave(OrienteeringManagerPreSaveEvent $event): void
    {
        $this->uploadableProcessor->process($event->getItem());
    }
}