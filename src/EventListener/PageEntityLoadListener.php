<?php

namespace OrienteeringManager\EventListener;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Events;
use Exception;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Manager\PageBuilderManager;

#[AsEntityListener(event: Events::postLoad, method: 'postLoad', entity: Page::class)]
final readonly class PageEntityLoadListener
{
    public function __construct(
        private PageBuilderManager $pageBuilderManager,
    ) {
    }

    /**
     * @throws Exception
     */
    public function postLoad(Page $page): void
    {
        $this->pageBuilderManager->build($page);
    }
}