<?php

declare(strict_types=1);

namespace OrienteeringManager\Exception;

use Exception;
use OrienteeringManager\Model\OrienteeringManager\IriableInterface;
use Throwable;

final class OrienteeringManagerException extends Exception
{
    public function __construct(
        private IriableInterface|null $object = null,
        string $message = '',
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        parent::__construct($message, $code, $previous);
    }
    
    public function getObject(): ?IriableInterface
    {
        return $this->object;
    }

    public function setObject(?IriableInterface $object): void
    {
        $this->object = $object;
    }
}