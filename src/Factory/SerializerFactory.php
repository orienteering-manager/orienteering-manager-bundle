<?php

declare(strict_types=1);

namespace OrienteeringManager\Factory;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\BackedEnumNormalizer;
use Symfony\Component\Serializer\Normalizer\DataUriNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

final class SerializerFactory
{
    private const array CSV_ENCODER_CONTEXT = [
        CsvEncoder::DELIMITER_KEY => ';',
    ];
    private const array DATETIME_NORMALIZER_CONTEXT = [
        DateTimeNormalizer::FORMAT_KEY => 'Y-m-d\TH:i:sP',
    ];

    public static function getClassMetadataFactory(): ClassMetadataFactory
    {
        return new ClassMetadataFactory(new AttributeLoader(new AnnotationReader()));
    }

    public static function createSerializer(ClassMetadataFactoryInterface $classMetadataFactory): SerializerInterface
    {
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);

        $propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->disableExceptionOnInvalidIndex()
            ->disableExceptionOnInvalidPropertyPath()
            ->getPropertyAccessor();

        $encoders = [
            new XmlEncoder(),
            new CsvEncoder(self::CSV_ENCODER_CONTEXT),
            new JsonEncoder(),
        ];

        $normalizers = [
            new BackedEnumNormalizer(),
            new DateTimeNormalizer(self::DATETIME_NORMALIZER_CONTEXT),
            new ArrayDenormalizer(),
            new DataUriNormalizer(),
            new ObjectNormalizer(
                $classMetadataFactory,
                $metadataAwareNameConverter,
                $propertyAccessor,
                self::getPropertyInfoExtractor(),
                null,
                null
            ),
        ];

        return new Serializer($normalizers, $encoders);
    }

    private static function getPropertyInfoExtractor(): PropertyInfoExtractor
    {
        $phpDocExtractor = new PhpDocExtractor();
        $reflectionExtractor = new ReflectionExtractor();

        $listExtractors = [$reflectionExtractor];
        $typeExtractors = [$phpDocExtractor, $reflectionExtractor];
        $descriptionExtractors = [$phpDocExtractor];
        $accessExtractors = [$reflectionExtractor];
        $propertyInitializeExtractors = [$reflectionExtractor];

        return new PropertyInfoExtractor(
            $listExtractors,
            $typeExtractors,
            $descriptionExtractors,
            $accessExtractors,
            $propertyInitializeExtractors
        );
    }
}