<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Entity;

use OrienteeringManager\Builder\PageBuilder\PageBuilderInterface;
use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Entity\Page;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class PageType extends AbstractType
{
    public function __construct(
        #[TaggedIterator('orienteering_manager.page_builder')]
        private readonly iterable $pageBuilders,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $data = $builder->getData();

        $pageTypes = [];
        /** @var PageBuilderInterface $pageBuilder */
        foreach ($this->pageBuilders as $pageBuilder) {
            if ($data?->getType() !== $pageBuilder->getBuilderId() && $pageBuilder->uniqueInstanciation()) {
                continue;
            }

            $pageTypes[$pageBuilder->getBuilderId()] = $pageBuilder->getBuilderId();
        }

        $builder
            ->add('title', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(), [
                'label' => 'admin.common.field.title',
            ]))
            ->add('icon', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(), [
                'required' => false,
                'label' => 'admin.common.field.icon',
                'help' => 'admin.common.help.icon',
                'help_html' => true,
            ]))
            ->add('type', ChoiceType::class, array_merge(FormOptionsConstant::FORM_SELECT_OPTIONS, [
                'disabled' => $data instanceof Page && null !== $data->getId(),
                'label' => 'admin.page.field.type',
                'choices' => $pageTypes,
                'choice_translation_domain' => 'page_builder',
            ]))
            ->add('inNavigation', CheckboxType::class, array_merge(FormOptionsConstant::FORM_CHECKBOX_OPTIONS, [
                'required' => false,
                'label' => 'admin.page.field.inNavigation',
            ]))
            ->add('navigationPosition', IntegerType::class, array_merge(FormOptionsConstant::getFormControlOptions(), [
                'required' => false,
                'label' => 'admin.page.field.navigationPosition',
            ]))
        ;

        if (null !== $data->getId() && null !== $data->getData()) {
            $builder->add('data', $data->getData()->getFormClass(), [
                'label' => 'admin.page.field.content',
                'label_attr' => [
                    'class' => 'form-label h3',
                ],
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
            'translation_domain' => 'admin',
        ]);
    }
}
