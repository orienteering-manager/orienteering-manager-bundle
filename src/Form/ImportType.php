<?php

declare(strict_types=1);

namespace OrienteeringManager\Form;

use OrienteeringManager\Config\Constant\FormOptionsConstant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ImportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('csv', FileType::class, array_merge(FormOptionsConstant::getFormControlOptions(), [
                'label' => 'admin.import.field.file',
                'required' => true,
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'translation_domain' => 'admin',
        ]);
    }
}
