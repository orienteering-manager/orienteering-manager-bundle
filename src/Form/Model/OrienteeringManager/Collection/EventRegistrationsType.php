<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Model\OrienteeringManager\Collection;

use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Form\Model\OrienteeringManager\Item\EventRegistrationType;
use OrienteeringManager\Model\OrienteeringManager\Collection\EventRegistrationApiCollection;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventRegistrationApiItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class EventRegistrationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('items', CollectionType::class, array_merge(FormOptionsConstant::getFormControlOptions(), [
                'required' => true,
                'label' => 'event.field.registrations.label',
                'entry_type' => EventRegistrationType::class,
                'entry_options' => [
                    'attr' => [
                        'class' => 'form-control mb-1',
                    ],
                    'event' => $builder->getOption('event'),
                ],
                'label_attr' => [
                    'class' => 'fw-bold mb-2',
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'prototype_data' => new EventRegistrationApiItem(),
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventRegistrationApiCollection::class,
            'translation_domain' => 'event',
        ]);

        $resolver->setRequired('event');
        $resolver->setAllowedTypes('event', EventApiItem::class);
    }
}
