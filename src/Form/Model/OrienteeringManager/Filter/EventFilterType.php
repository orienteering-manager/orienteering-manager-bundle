<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Model\OrienteeringManager\Filter;

use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Model\OrienteeringManager\Filter\EventApiFilter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class EventFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('query', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(), [
                'label' => 'event.filter.query.label',
                'required' => false,
            ]))
            ->add('dateAfter', DateType::class, array_merge(FormOptionsConstant::getFormControlOptions(), [
                'label' => 'event.filter.dateAfter.label',
                'required' => false,
                'widget' => 'single_text',
            ]))
            ->add('dateBefore', DateType::class, array_merge(FormOptionsConstant::getFormControlOptions(), [
                'label' => 'event.filter.dateBefore.label',
                'required' => false,
                'widget' => 'single_text',
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventApiFilter::class,
            'translation_domain' => 'event',
        ]);
    }
}
