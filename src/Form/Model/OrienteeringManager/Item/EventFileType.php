<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Model\OrienteeringManager\Item;

use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Model\OrienteeringManager\Item\EventFileApiItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

final class EventFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(type: 'secondary'), [
                'label' => 'admin.event.field.file.name',
                'required' => true,
                'constraints' => [new NotBlank(message: 'admin.event.field.file.name.not_blank')],
            ]))
            ->add('file', FileType::class, array_merge(FormOptionsConstant::getFormControlOptions(type: 'secondary'), [
                'label' => 'admin.event.field.file.file',
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventFileApiItem::class,
            'translation_domain' => 'admin',
        ]);
    }
}
