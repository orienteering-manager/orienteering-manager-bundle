<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Model\OrienteeringManager\Item;

use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Model\OrienteeringManager\Item\EventLocationApiItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class EventLocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'label' => 'admin.event.field.location.name',
                'required' => false,
            ]))
            ->add('description', TextareaType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'label' => 'admin.event.field.location.description',
                'required' => false,
            ]))
            ->add('latitude', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h6', type: 'secondary'), [
                'label' => 'admin.event.field.location.latitude',
                'required' => false,
            ]))
            ->add('longitude', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h6', type: 'secondary'), [
                'label' => 'admin.event.field.location.longitude',
                'required' => false,
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventLocationApiItem::class,
            'translation_domain' => 'admin',
        ]);
    }
}
