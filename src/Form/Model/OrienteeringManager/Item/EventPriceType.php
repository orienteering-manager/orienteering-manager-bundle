<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Model\OrienteeringManager\Item;

use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Model\OrienteeringManager\Item\EventPriceApiItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class EventPriceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('amount', NumberType::class, array_merge(FormOptionsConstant::getFormControlOptions(type: 'secondary'), [
                'label' => 'admin.event.field.prices.amount',
                'required' => true,
                'scale' => 2,
            ]))
            ->add('federationMember', CheckboxType::class, array_merge(FormOptionsConstant::FORM_CHECKBOX_OPTIONS, [
                'label' => 'admin.event.field.prices.isFederationMember',
                'required' => false,
            ]))
            ->add('categoryStart', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(type: 'secondary'), [
                'label' => 'admin.event.field.prices.categoryStart',
                'help' => 'admin.event.help.prices.categoryStart',
                'required' => false,
            ]))
            ->add('categoryEnd', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(type: 'secondary'), [
                'label' => 'admin.event.field.prices.categoryEnd',
                'help' => 'admin.event.help.prices.categoryEnd',
                'required' => false,
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventPriceApiItem::class,
            'translation_domain' => 'admin',
        ]);
    }
}
