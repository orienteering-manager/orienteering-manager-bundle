<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Model\OrienteeringManager\Item;

use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventRegistrationApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\FederationMemberApiItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class EventRegistrationType extends AbstractType
{
    public function __construct(
        private readonly UrlGeneratorInterface $urlGenerator,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var EventRegistrationApiItem $registration */
        $registration = $builder->getData();

        /** @var EventApiItem $event */
        $event = $builder->getOption('event');
        $federationMember = $registration?->getFederationMember();

        $federationMembers = [];
        if ($federationMember instanceof FederationMemberApiItem) {
            $label = sprintf(
                '%s - %s %s #%s',
                $federationMember->getClub()?->getId(),
                $federationMember->getLastName(),
                $federationMember->getFirstName(),
                $federationMember->getId(),
            );
            $federationMembers[$label] = $federationMember->getIri();
        }

        if (null === $registration?->getId() || !empty($federationMember)) {
            $builder
                ->add('federationMemberIri', ChoiceType::class, array_merge(FormOptionsConstant::getFormControlOptions(), [
                    'label' => 'registration.field.federationMember.label',
                    'placeholder' => 'registration.field.federationMember.placeholder',
                    'required' => false,
                    'autocomplete' => true,
                    'autocomplete_url' => $this->urlGenerator->generate('orienteering_manager_federation_member_autocomplete'),
                    'choices' => $federationMembers,
                    'disabled' => !empty($federationMember),
                    'attr' => [
                        'data-controller' => 'federation-member-autocomplete',
                    ],
                ]))
                ->get('federationMemberIri')->resetViewTransformers();
        }

        if (null === $registration?->getId() || empty($federationMember)) {
            $builder
                ->add('firstName', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(), [
                    'label' => 'registration.field.firstName.label',
                    'required' => true,
                    'attr' => [
                        'class' => 'form-control input-first-name',
                    ]
                ]))
                ->add('lastName', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(), [
                    'label' => 'registration.field.lastName.label',
                    'required' => true,
                    'attr' => [
                        'class' => 'form-control input-last-name',
                    ]
                ]))
            ;

            $builder
                ->add('notFederationMember', CheckboxType::class, array_merge(FormOptionsConstant::FORM_CHECKBOX_OPTIONS, [
                    'label' => 'registration.field.notFederationMember.label',
                    'required' => false,
                ]));
        }

        $builder
            ->add('cardNumber', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(), [
                'label' => 'registration.field.cardNumber.label',
                'required' => false,
                'attr' => [
                    'class' => 'form-control input-card-number',
                ]
            ]))
            ->add('comment', TextareaType::class, array_merge(FormOptionsConstant::getFormControlOptions(), [
                'label' => 'registration.field.comment.label',
                'required' => false,
            ]))
        ;

        if (!empty($event->getTracks())) {
            $trackChoices = [];
            foreach ($event->getTracks() as $track) {
                $trackChoices[$track->getName()] = $track->getIri();
            }

            $builder->add('trackIri', ChoiceType::class, array_merge(FormOptionsConstant::FORM_SELECT_OPTIONS, [
                'label' => 'registration.field.trackIri.label',
                'required' => !$event->isUseCategory(),
                'choices' => $trackChoices,
                'placeholder' => !$event->isUseCategory() ? 'registration.field.trackIri.placeholder' : null,
            ]));
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventRegistrationApiItem::class,
            'translation_domain' => 'registration',
        ]);

        $resolver->setRequired('event');
        $resolver->setAllowedTypes('event', EventApiItem::class);
    }
}
