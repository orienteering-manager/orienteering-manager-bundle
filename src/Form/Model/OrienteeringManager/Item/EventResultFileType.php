<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Model\OrienteeringManager\Item;

use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Model\OrienteeringManager\Item\EventResultFileApiItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class EventResultFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(type: 'secondary'), [
                'label' => 'admin.event_result.field.file.name',
                'required' => false,
            ]))
            ->add('file', FileType::class, array_merge(FormOptionsConstant::getFormControlOptions(type: 'secondary'), [
                'label' => 'admin.event_result.field.file.file',
                'required' => false,
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventResultFileApiItem::class,
            'translation_domain' => 'admin',
        ]);
    }
}
