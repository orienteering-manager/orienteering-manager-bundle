<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Model\OrienteeringManager\Item;

use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Model\OrienteeringManager\Item\EventResultApiItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class EventResultType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('live', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(size: 'h5'), [
                'label' => 'admin.event.field.result.live',
                'required' => false,
            ]))
            ->add('files', CollectionType::class, array_merge_recursive(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'required' => false,
                'label' => 'admin.event.field.result.files',
                'entry_type' => EventResultFileType::class,
                'entry_options' => [
                    'attr' => [
                        'class' => 'form-control mb-1',
                    ],
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventResultApiItem::class,
            'translation_domain' => 'admin',
        ]);
    }
}
