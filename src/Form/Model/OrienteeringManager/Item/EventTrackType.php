<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Model\OrienteeringManager\Item;

use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Model\OrienteeringManager\Item\EventTrackApiItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class EventTrackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('name', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(type: 'secondary'), [
                'label' => 'admin.event.field.tracks.name',
                'required' => false,
            ]))
            ->add('identifier', IntegerType::class, array_merge(FormOptionsConstant::getFormControlOptions(type: 'secondary'), [
                'label' => 'admin.event.field.tracks.identifier',
                'required' => false,
            ]))
            ->add('length', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(type: 'secondary'), [
                'label' => 'admin.event.field.tracks.length',
                'required' => false,
            ]))
            ->add('elevationGain', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(type: 'secondary'), [
                'label' => 'admin.event.field.tracks.elevationGain',
                'required' => false,
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventTrackApiItem::class,
            'translation_domain' => 'admin',
        ]);
    }
}
