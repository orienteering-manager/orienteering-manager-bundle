<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Model\OrienteeringManager\Item;

use Ehyiah\QuillJsBundle\Form\QuillType;
use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Config\Constant\QuillOptionsConstant;
use OrienteeringManager\Config\Enum\AuthorizationLevelEnum;
use OrienteeringManager\Config\Enum\EventFormatEnum;
use OrienteeringManager\Config\Enum\EventStatusEnum;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'label' => 'admin.common.field.name',
            ]))
            ->add('image', FileType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'label' => 'admin.common.field.image',
                'required' => false,
                'help' => 'admin.event.help.image',
            ]))
            ->add('deleteImage', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(type: 'danger'), [
                'label' => 'admin.common.field.deleteImage',
                'required' => false,
                'help' => 'admin.common.help.deleteImage',
            ]))
            ->add('date', DateTimeType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'label' => 'admin.common.field.date',
                'widget' => 'single_text',
            ]))
            ->add('endDate', DateTimeType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'label' => 'admin.event.field.endDate',
                'widget' => 'single_text',
                'required' => false,
            ]))
            ->add('status', EnumType::class, array_merge(FormOptionsConstant::getFormSelectOptions(size: 'h5'), [
                'label' => 'admin.common.field.status',
                'class' => EventStatusEnum::class,
                'required' => false,
            ]))
            ->add('categories', CollectionType::class, array_merge_recursive(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'required' => false,
                'label' => 'admin.event.field.categories.name',
                'help' => 'admin.event.help.categories',
                'entry_type' => EventCategoryType::class,
                'entry_options' => [
                    'attr' => [
                        'class' => 'form-control mb-1',
                    ],
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
            ]))
            ->add('organizer', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'label' => 'admin.event.field.organizer',
                'required' => false,
            ]))
            ->add('organizerWebsite', UrlType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'label' => 'admin.event.field.organizerWebsite',
                'required' => false,
            ]))
            ->add('format', EnumType::class, array_merge(FormOptionsConstant::getFormSelectOptions(size: 'h5'), [
                'label' => 'admin.common.field.format',
                'class' => EventFormatEnum::class,
                'required' => false,
            ]))
            ->add('level', EnumType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'required' => true,
                'class' => AuthorizationLevelEnum::class,
                'label' => 'admin.common.field.level',
                'help' => 'admin.common.help.level',
            ]))
            ->add('private', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(size: 'h5'), [
                'required' => false,
                'label' => 'admin.common.field.private',
                'help' => 'admin.common.help.private',
            ]))
            ->add('allowRegistration', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(size: 'h5'), [
                'required' => false,
                'label' => 'admin.event.field.allowRegistration',
            ]))
            ->add('registrationNeedValidation', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(size: 'h5'), [
                'required' => false,
                'label' => 'admin.event.field.registrationNeedValidation',
                'help' => 'admin.event.help.registrationNeedValidation',
            ]))
            ->add('registrationDescription', QuillType::class, array_merge(FormOptionsConstant::FORM_QUILL_OPTIONS, [
                'label' => 'admin.event.field.registrationDescription',
                'required' => false,
                'quill_options' => QuillOptionsConstant::defaultConfig(),
            ]))
            ->add('useCategory', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(size: 'h5'), [
                'required' => false,
                'label' => 'admin.event.field.useCategory',
            ]))
            ->add('registrationLimitDate', DateTimeType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'label' => 'admin.event.field.registrationLimitDate',
                'widget' => 'single_text',
                'required' => false,
            ]))
            ->add('registrationUrl', UrlType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'label' => 'admin.event.field.registrationUrl',
                'help' => 'admin.event.help.registrationUrl',
                'required' => false,
            ]))
            ->add('content', QuillType::class, array_merge(FormOptionsConstant::FORM_QUILL_OPTIONS, [
                'label' => 'admin.common.field.content',
                'required' => false,
                'quill_options' => QuillOptionsConstant::defaultConfig(),
            ]))
            ->add('location', EventLocationType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'label_attr' => [
                    'class' => 'd-none',
                ],
                'required' => false,
            ]))
            ->add('tracks', CollectionType::class, array_merge_recursive(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'required' => false,
                'label' => 'admin.event.field.tracks.name',
                'entry_type' => EventTrackType::class,
                'entry_options' => [
                    'attr' => [
                        'class' => 'form-control mb-1',
                    ],
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
            ]))
            ->add('prices', CollectionType::class, array_merge_recursive(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'required' => false,
                'label' => 'admin.event.field.prices.name',
                'entry_type' => EventPriceType::class,
                'entry_options' => [
                    'attr' => [
                        'class' => 'form-control mb-1',
                    ],
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
            ]))
            ->add('files', CollectionType::class, array_merge_recursive(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'required' => false,
                'label' => 'admin.common.field.files.name',
                'entry_type' => EventFileType::class,
                'entry_options' => [
                    'attr' => [
                        'class' => 'form-control mb-1',
                    ],
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
            ]))
            ->add('result', EventResultType::class, [
                'label_attr' => [
                    'class' => 'd-none',
                ],
            ])
            ->add('allowHelpColorTracks', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(), [
                'label' => 'admin.event.field.tracks.allowHelpColorTracks',
                'required' => false,
            ]))
            ->add('allowHelpColorTrackDark', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(), [
                'label' => 'admin.event.field.tracks.allowHelpColorTrackDark',
                'required' => false,
            ]))
            ->add('allowHelpColorTrackPurple', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(), [
                'label' => 'admin.event.field.tracks.allowHelpColorTrackPurple',
                'required' => false,
            ]))
            ->add('allowHelpColorTrackOrange', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(), [
                'label' => 'admin.event.field.tracks.allowHelpColorTrackOrange',
                'required' => false,
            ]))
            ->add('allowHelpColorTrackYellow', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(), [
                'label' => 'admin.event.field.tracks.allowHelpColorTrackYellow',
                'required' => false,
            ]))
            ->add('allowHelpColorTrackGreen', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(), [
                'label' => 'admin.event.field.tracks.allowHelpColorTrackGreen',
                'required' => false,
            ]))
            ->add('allowHelpColorTrackBlue', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(), [
                'label' => 'admin.event.field.tracks.allowHelpColorTrackBlue',
                'required' => false,
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventApiItem::class,
            'translation_domain' => 'admin',
        ]);
    }
}
