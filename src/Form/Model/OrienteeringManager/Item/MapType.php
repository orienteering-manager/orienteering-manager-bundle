<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Model\OrienteeringManager\Item;

use Ehyiah\QuillJsBundle\Form\QuillType;
use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Config\Constant\QuillOptionsConstant;
use OrienteeringManager\Config\Enum\AuthorizationLevelEnum;
use OrienteeringManager\Model\OrienteeringManager\Item\MapApiItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class MapType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'required' => true,
                'label' => 'admin.common.field.name',
            ]))
            ->add('unavailable', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(size: 'h5'), [
                'required' => false,
                'label' => 'admin.map.field.unavailable',
                'help' => 'admin.map.help.unavailable',
            ]))
            ->add('city', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'required' => false,
                'label' => 'admin.map.field.city',
            ]))
            ->add('latitude', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h6', type: 'secondary'), [
                'required' => false,
                'label' => 'admin.common.field.location.latitude',
            ]))
            ->add('longitude', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h6', type: 'secondary'), [
                'required' => false,
                'label' => 'admin.common.field.location.longitude',
            ]))
            ->add('vikazimut', TextType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'required' => false,
                'label' => 'admin.map.field.vikazimut',
            ]))
            ->add('allowHelpUseVikazimut', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(), [
                'label' => 'admin.map.field.allowHelpUseVikazimut',
                'required' => false,
            ]))
            ->add('allowHelpReadMap', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(), [
                'label' => 'admin.map.field.allowHelpReadMap',
                'required' => false,
            ]))
            ->add('content', QuillType::class, array_merge(FormOptionsConstant::FORM_QUILL_OPTIONS, [
                'label' => 'admin.common.field.content',
                'required' => false,
                'quill_options' => QuillOptionsConstant::defaultConfig(),
            ]))
            ->add('image', FileType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'label' => 'admin.common.field.image',
                'required' => false,
            ]))
            ->add('deleteImage', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(type: 'danger'), [
                'label' => 'admin.common.field.deleteImage',
                'required' => false,
                'help' => 'admin.common.help.deleteImage',
            ]))
            ->add('file', FileType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'label' => 'admin.common.field.file',
                'required' => false,
            ]))
            ->add('level', EnumType::class, array_merge(FormOptionsConstant::getFormControlOptions(size: 'h5'), [
                'required' => true,
                'class' => AuthorizationLevelEnum::class,
                'label' => 'admin.common.field.level',
                'help' => 'admin.common.help.level',
            ]))
            ->add('private', CheckboxType::class, array_merge(FormOptionsConstant::getFormCheckboxOptions(size: 'h5'), [
                'required' => false,
                'label' => 'admin.common.field.private',
                'help' => 'admin.common.help.private',
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MapApiItem::class,
            'translation_domain' => 'admin',
        ]);
    }
}
