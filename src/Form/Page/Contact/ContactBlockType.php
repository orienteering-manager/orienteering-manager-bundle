<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Page\Contact;

use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Model\Page\Contact\ContactBlockModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ContactBlockType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('role', TextType::class, array_merge_recursive(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'required' => false,
                'label' => 'admin.page.type.contact.persons.role.label',
            ]))
            ->add('firstName', TextType::class, array_merge_recursive(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'required' => false,
                'label' => 'admin.page.type.contact.persons.firstName.label',
            ]))
            ->add('lastName', TextType::class, array_merge_recursive(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'required' => false,
                'label' => 'admin.page.type.contact.persons.lastName.label',
            ]))
            ->add('email', EmailType::class, array_merge_recursive(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'required' => false,
                'label' => 'admin.page.type.contact.persons.email.label',
            ]))
            ->add('phone', TextType::class, array_merge_recursive(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'required' => false,
                'label' => 'admin.page.type.contact.persons.phone.label',
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ContactBlockModel::class,
            'translation_domain' => 'admin',
        ]);
    }
}
