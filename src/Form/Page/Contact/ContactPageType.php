<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Page\Contact;

use Ehyiah\QuillJsBundle\Form\QuillType;
use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Config\Constant\QuillOptionsConstant;
use OrienteeringManager\Form\SeoDataType;
use OrienteeringManager\Model\Page\Contact\ContactModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ContactPageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('content', QuillType::class, array_merge(FormOptionsConstant::FORM_QUILL_OPTIONS, [
                'label' => 'admin.page.field.content',
                'quill_options' => QuillOptionsConstant::defaultConfig(),
                'required' => false,
            ]))
            ->add('persons', CollectionType::class, array_merge_recursive(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'required' => false,
                'label' => 'admin.page.type.contact.persons.label',
                'entry_type' => ContactBlockType::class,
                'entry_options' => [
                    'attr' => [
                        'class' => 'form-control mb-1',
                    ],
                ],
                'allow_add' => true,
                'prototype' => true,
            ]))
            ->add('seoData', SeoDataType::class, array_merge(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'label' => 'admin.page.seo',
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ContactModel::class,
            'translation_domain' => 'admin',
        ]);
    }
}
