<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Page\Custom;

use Ehyiah\QuillJsBundle\Form\QuillType;
use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Config\Constant\QuillOptionsConstant;
use OrienteeringManager\Form\SeoDataType;
use OrienteeringManager\Model\Page\Custom\CustomModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class CustomPageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('content', QuillType::class, array_merge(FormOptionsConstant::FORM_QUILL_OPTIONS, [
                'label' => 'admin.page.field.content',
                'attr' => [
                    'class' => 'form-control ql-editor',
                ],
                'quill_options' => QuillOptionsConstant::defaultConfig(),
            ]))
            ->add('seoData', SeoDataType::class, array_merge(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'label' => 'admin.page.seo',
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CustomModel::class,
            'translation_domain' => 'admin',
        ]);
    }
}
