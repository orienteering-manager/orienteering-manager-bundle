<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Page\EventResult;

use Ehyiah\QuillJsBundle\Form\QuillType;
use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Config\Constant\QuillOptionsConstant;
use OrienteeringManager\Form\SeoDataType;
use OrienteeringManager\Model\Page\EventResult\EventResultListModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class EventResultListPageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('content', QuillType::class, array_merge(FormOptionsConstant::FORM_QUILL_OPTIONS, [
                'label' => 'admin.page.type.event.list.content.label',
                'quill_options' => QuillOptionsConstant::defaultConfig(),
                'required' => false,
            ]))
            ->add('itemPerPages', IntegerType::class, array_merge(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'label' => 'admin.common.field.itemPerPage',
            ]))
            ->add('seoData', SeoDataType::class, array_merge(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'label' => 'admin.page.seo',
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventResultListModel::class,
            'translation_domain' => 'admin',
        ]);
    }
}
