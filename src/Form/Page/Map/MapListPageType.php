<?php

declare(strict_types=1);

namespace OrienteeringManager\Form\Page\Map;

use Ehyiah\QuillJsBundle\Form\QuillType;
use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Config\Constant\QuillOptionsConstant;
use OrienteeringManager\Form\SeoDataType;
use OrienteeringManager\Model\Page\Map\MapListModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class MapListPageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('content', QuillType::class, array_merge(FormOptionsConstant::FORM_QUILL_OPTIONS, [
                'label' => 'admin.page.type.map.list.content.label',
                'quill_options' => QuillOptionsConstant::defaultConfig(),
                'required' => false,
            ]))
            ->add('title', TextType::class, array_merge_recursive(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'required' => false,
                'label' => 'admin.page.type.map.list.title.label',
            ]))
            ->add('titleVikazimut', TextType::class, array_merge_recursive(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'required' => false,
                'label' => 'admin.page.type.map.list.titleVikazimut.label',
            ]))
            ->add('descriptionVikazimut', TextareaType::class, array_merge_recursive(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'required' => false,
                'label' => 'admin.page.type.map.list.descriptionVikazimut.label',
            ]))
            ->add('linkUrlVikazimut', UrlType::class, array_merge_recursive(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'required' => false,
                'label' => 'admin.page.type.map.list.linkUrlVikazimut.label',
            ]))
            ->add('linkLabelVikazimut', TextType::class, array_merge_recursive(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'required' => false,
                'label' => 'admin.page.type.map.list.linkLabelVikazimut.label',
            ]))
            ->add('seoData', SeoDataType::class, array_merge(FormOptionsConstant::FORM_CONTROL_OPTIONS, [
                'label' => 'admin.page.seo',
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MapListModel::class,
            'translation_domain' => 'admin',
        ]);
    }
}
