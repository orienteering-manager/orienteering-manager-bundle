<?php

declare(strict_types=1);

namespace OrienteeringManager\Form;

use OrienteeringManager\Config\Constant\FormOptionsConstant;
use OrienteeringManager\Model\Page\SeoDataModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class SeoDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('metaDescription', TextareaType::class, array_merge(FormOptionsConstant::getFormControlOptions(), [
                'label' => 'admin.common.field.metaDescription',
                'required' => false,
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SeoDataModel::class,
            'translation_domain' => 'admin',
        ]);
    }
}
