<?php

declare(strict_types=1);

namespace OrienteeringManager\Logger;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class OrienteeringManagerLogger
{
    public function __construct(
        private RequestStack $requestStack,
        private TranslatorInterface $translator,
    ) {
    }

    public function addFlash(string $type, string $id, array $parameters = []): void
    {
        $this->requestStack->getSession()->getFlashBag()->add(
            $type,
            $this->translator->trans(
                sprintf('orienteering_manager.error.%s', $id),
                $parameters,
                'flash_messages',
            ),
        );
    }
}
