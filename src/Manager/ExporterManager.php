<?php

declare(strict_types=1);

namespace OrienteeringManager\Manager;

use Doctrine\ORM\EntityManagerInterface;
use OrienteeringManager\Entity\EntityInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

final readonly class ExporterManager
{
    public function __construct(
        private SerializerInterface $serializer,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function export(string $type): string
    {
        $repository = $this->entityManager->getRepository($type);

        return $this->serializer->serialize($repository->findAll(),CsvEncoder::FORMAT, [
            AbstractNormalizer::GROUPS => [EntityInterface::EXPORT_FIELD_GROUPS],
            CsvEncoder::OUTPUT_UTF8_BOM_KEY => true,
        ]);
    }
}
