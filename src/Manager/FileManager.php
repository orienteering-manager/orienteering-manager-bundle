<?php

declare(strict_types=1);

namespace OrienteeringManager\Manager;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mime\MimeTypes;

final readonly class FileManager
{
    public function __construct(
        #[Autowire('%kernel.project_dir%/public')]
        private string $publicDir,
    ) {
    }

    public function upload(
        ?UploadedFile $file,
        string $fileLocation,
        string $targetFilename,
        ?string $existingFilePath = null,
    ): ?string {
        $this->remove($existingFilePath);

        if (null === $file) {
            return null;
        }

        $filename = sprintf('%s.%s', $targetFilename, $file->guessExtension());

        $file->move(sprintf('%s%s', $this->publicDir, $fileLocation), $filename);

        return sprintf('%s/%s', $fileLocation, $filename);
    }

    public function remove(?string $filePath): void
    {
        if (null === $filePath) {
            return;
        }

        $filesystem = new Filesystem();
        $filesystem->remove($this->publicDir.$filePath);
    }

    /**
     * @throws FileNotFoundException
     */
    public function toResponse(string $filePath, string $baseName): BinaryFileResponse
    {
        $finder = new Finder();
        $finder
            ->files()
            ->in($this->publicDir)
            ->path($filePath)
            ->followLinks();

        foreach ($finder as $file) {
            $response = new BinaryFileResponse($file->getFileInfo());
            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                sprintf('%s.%s', $baseName, $file->getExtension())
            );

            return $response;
        }

        throw new FileNotFoundException();
    }
    
    public function base64ToResponse(string $base64, ?string $filename): Response
    {
        $base64Data = explode(';', $base64);
        if (2 !== count($base64Data)) {
            throw new FileException();
        }

        $mimeType = str_replace('data:', '', $base64Data[0]);
        $extension = MimeTypes::getDefault()->getExtensions($mimeType)[0];

        $response = new Response(base64_decode(str_replace('base64,', '', $base64Data[1])));
        $response->headers->set('Content-Type', $mimeType);
        $response->headers->set('Content-Disposition', sprintf('attachment; filename=%s.%s', $filename, $extension));

        return $response;
    }
}
