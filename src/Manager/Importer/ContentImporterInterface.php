<?php

declare(strict_types=1);

namespace OrienteeringManager\Manager\Importer;

use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Component\HttpFoundation\File\UploadedFile;

#[AutoconfigureTag('orienteering_manager.content_importer')]
interface ContentImporterInterface
{
    public function import(string $type, UploadedFile $file): void;
    public function supports(string $type): bool;
}