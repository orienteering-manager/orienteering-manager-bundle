<?php

declare(strict_types=1);

namespace OrienteeringManager\Manager\Importer;

use Doctrine\ORM\EntityManagerInterface;
use OrienteeringManager\Repository\EntityRepositoryInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\SerializerInterface;

final readonly class EntityImporterManager implements ContentImporterInterface
{
    public function __construct(
        private SerializerInterface $serializer,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function import(string $type, UploadedFile $file): void
    {
        $models = $this->serializer->deserialize($file->getContent(), sprintf('%s[]', $type), CsvEncoder::FORMAT);
        foreach ($models as $model) {
            $this->entityManager->persist($model);
        }

        $this->entityManager->flush();
    }

    public function supports(string $type): bool
    {
        return $this->entityManager->getRepository($type) instanceof EntityRepositoryInterface;
    }
}
