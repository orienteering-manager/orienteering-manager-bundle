<?php

declare(strict_types=1);

namespace OrienteeringManager\Manager;

use Exception;
use OrienteeringManager\Manager\Importer\ContentImporterInterface;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final readonly class ImporterManager
{
    /**
     * @param ContentImporterInterface[] $importerServices
     */
    public function __construct(
        #[TaggedIterator('orienteering_manager.content_importer')]
        private iterable $importerServices,
    ) {
    }

    /**
     * @throws Exception
     */
    public function import(string $type, UploadedFile $file): void
    {
        foreach ($this->importerServices as $importer) {
            if (!$importer->supports($type)) {
                continue;
            }

            $importer->import($type, $file);

            return;
        }

        throw new Exception(sprintf('ImporterService - No ContentImporterInterface found for content of type %s', $type));
    }
}
