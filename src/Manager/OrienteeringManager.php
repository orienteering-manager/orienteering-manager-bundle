<?php

declare(strict_types=1);

namespace OrienteeringManager\Manager;

use OrienteeringManager\Exception\OrienteeringManagerException;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final readonly class OrienteeringManager implements OrienteeringManagerInterface
{
    public function __construct(
        private HttpClientInterface $orienteeringManagerClient,
        private SerializerInterface $serializer,
        private CacheInterface $cache,
        private LoggerInterface $logger,
        #[Autowire('%env(ORIENTEERING_MANAGER_CLIENT_USERNAME)%')]
        private string $orienteeringManagerClientUsername,
        #[Autowire('%env(ORIENTEERING_MANAGER_CLIENT_PASSWORD)%')]
        private string $orienteeringManagerClientPassword,
    ) {
    }

    /**
     * @throws OrienteeringManagerException
     */
    public function request(
        string $method,
        string $uri,
        array $options = [],
        bool $fullResponse = false,
        bool $isRetry = false,
    ): ResponseInterface|string {
        try {
            $response = $this->orienteeringManagerClient->request($method, $uri, array_merge($options, [
                'headers' => [
                    'Authorization' => sprintf('Bearer %s', $this->getToken()),
                    'accept' => 'application/ld+json',
                    'Content-Type' => 'application/ld+json',
                ]
            ]));

            return $fullResponse ?
                $response :
                $response->getContent();
        } catch (TransportExceptionInterface|ServerExceptionInterface|RedirectionExceptionInterface|ClientExceptionInterface|InvalidArgumentException $e) {
            $this->logger->error(sprintf('OrienteeringManager - Error when call %s : %s (status_code=%s, class=%s)', $uri, $e->getMessage(), $e->getCode(), get_class($e)));

            if (Response::HTTP_UNAUTHORIZED === $e->getCode() && !$isRetry) {
                $this->resetToken();

                return $this->request($method, $uri, $options, $fullResponse, true);
            }

            throw new OrienteeringManagerException(
                message: $e->getMessage(),
                code: $e->getCode(),
                previous: $e,
            );
        }
    }

    /**
     * @throws InvalidArgumentException
     */
    private function getToken(): string
    {
        return $this->cache->get('event_client.auth_token', function (ItemInterface $item) {
            $item->expiresAfter(14000);

            $response = $this->orienteeringManagerClient->request(Request::METHOD_POST, '/api/auth', [
                'headers' => [
                    'accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
                'body' => $this->serializer->serialize([
                    'username' => $this->orienteeringManagerClientUsername,
                    'password' => $this->orienteeringManagerClientPassword,
                ], JsonEncoder::FORMAT),
            ]);

            return $response->toArray()['token'];
        });
    }

    private function resetToken(): void
    {
        try {
            $this->cache->delete('event_client.auth_token');
        } catch (InvalidArgumentException) {
        }
    }
}
