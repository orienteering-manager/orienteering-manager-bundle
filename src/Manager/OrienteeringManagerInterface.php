<?php

namespace OrienteeringManager\Manager;

use OrienteeringManager\Exception\OrienteeringManagerException;
use Symfony\Contracts\HttpClient\ResponseInterface;

interface OrienteeringManagerInterface
{
    /**
     * @throws OrienteeringManagerException
     */
    public function request(
        string $method,
        string $uri,
        array $options = [],
        bool $fullResponse = false,
        bool $isRetry = false,
    ): ResponseInterface|string;
}