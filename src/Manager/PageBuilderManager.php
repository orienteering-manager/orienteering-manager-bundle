<?php

declare(strict_types=1);

namespace OrienteeringManager\Manager;

use Exception;
use OrienteeringManager\Builder\PageBuilder\PageBuilderInterface;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Model\Page\PageModelInterface;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

final class PageBuilderManager
{
    private array $loadedPageBuilders = [];

    /**
     * @param Serializer $serializer
     */
    public function __construct(
        private readonly SerializerInterface $serializer,
        #[TaggedIterator('orienteering_manager.page_builder')]
        private readonly iterable $pageBuilders,
    ) {
    }

    /**
     * @throws Exception
     */
    public function build(Page $page): void
    {
        $this->getSupportedPageBuilder($page)->build($page);
    }

    /**
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function handle(Page $page, FormInterface $form): void
    {
        $data = $page->getData();
        if (null === $data) {
            return;
        }

        $this->getSupportedPageBuilder($page)->save($page->getData(), $form->get('data'));

        $page->setContent($this->serializer->normalize($data, $data::class, [
            'groups' => [PageModelInterface::GROUP_PAGE_MODEL]
        ]));
    }

    /**
     * @throws Exception
     */
    private function getSupportedPageBuilder(Page $page): PageBuilderInterface
    {
        if (isset($this->loadedPageBuilders[$page->getType()])) {
            return $this->loadedPageBuilders[$page->getType()];
        }

        /** @var PageBuilderInterface $pageBuilder */
        foreach ($this->pageBuilders as $pageBuilder) {
            if (!$pageBuilder->supports($page->getType())) {
                continue;
            }

            return $this->loadedPageBuilders[$page->getType()] = $pageBuilder;
        }

        throw new Exception(sprintf('PageBuilderService - No PageBuilder found for page of type %s', $page->getType() ?? 'null'));
    }
}
