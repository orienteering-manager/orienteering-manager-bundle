<?php

declare(strict_types=1);

namespace OrienteeringManager\Manager;

use OrienteeringManager\Builder\SitemapBuilder\SitemapBuilderInterface;
use OrienteeringManager\Model\Sitemap\Sitemap;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

final readonly class SitemapManager
{
    /**
     * @param Serializer $serializer
     * @param SitemapBuilderInterface[] $sitemapBuilders
     */
    public function __construct(
        private SerializerInterface $serializer,
        #[TaggedIterator('orienteering_manager.sitemap_builder')]
        private iterable $sitemapBuilders,
    ) {
    }

    public function generate(): string
    {
        $sitemap = new Sitemap();

        foreach ($this->sitemapBuilders as $sitemapBuilder) {
            $sitemapBuilder->generate($sitemap);
        }

        return $this->serializer->serialize($sitemap, XmlEncoder::FORMAT, [
            XmlEncoder::ENCODING => 'UTF-8',
            XmlEncoder::ROOT_NODE_NAME => 'urlset',
        ]);
    }
}
