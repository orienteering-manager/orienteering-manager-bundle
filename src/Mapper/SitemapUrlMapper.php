<?php

namespace OrienteeringManager\Mapper;

use DateTimeImmutable;
use OrienteeringManager\Config\Enum\SitemapChangeFrequencyEnum;
use OrienteeringManager\Model\Sitemap\Url;

final class SitemapUrlMapper
{
    public function map(
        string $url,
        SitemapChangeFrequencyEnum $changeFrequency = SitemapChangeFrequencyEnum::WEEKLY,
        float $priority = 1.0,
        ?DateTimeImmutable $lastUpdate = null,
    ): Url {
        return (new Url())
            ->setLoc($url)
            ->setChangefreq($changeFrequency)
            ->setPriority($priority)
            ->setLastmod($lastUpdate);
    }
}