<?php

declare(strict_types=1);

namespace OrienteeringManager\MessageHandler;

use OrienteeringManager\Manager\SitemapManager;
use OrienteeringManager\Message\SitemapUpdateMessage;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class SitemapUpdateMessageHandler
{
    public function __construct(
        private SitemapManager $sitemapManager,
    ) {
    }

    public function __invoke(SitemapUpdateMessage $sitemapUpdateMessage): void
    {
        $this->sitemapManager->generate();
    }
}
