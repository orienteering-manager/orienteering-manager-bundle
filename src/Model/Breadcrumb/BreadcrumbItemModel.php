<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\Breadcrumb;

final class BreadcrumbItemModel
{
    private string $label;
    private ?string $url = null;
    private bool $active = false;

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}