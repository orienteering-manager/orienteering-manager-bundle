<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\Breadcrumb;

use OrienteeringManager\Entity\Page;

final class BreadcrumbModel
{
    /** @var BreadcrumbItemModel[] $items */
    private array $items = [];

    public function getItems(): array
    {
        usort($this->items, function (BreadcrumbItemModel $a, BreadcrumbItemModel $b) {
            return $a->isActive() <=> $b->isActive();
        });

        return $this->items;
    }

    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    public function addItem(string $label, ?string $url = null, bool $active = false): void
    {
        $this->items[] = (new BreadcrumbItemModel())
            ->setLabel($label)
            ->setUrl($url)
            ->setActive($active);
    }

    public static function createFromPage(Page $page, string $url): BreadcrumbModel
    {
        $breadcrumb = new BreadcrumbModel();
        $breadcrumb->addItem($page->getTitle(), $url);

        return $breadcrumb;
    }
}
