<?php

declare(strict_types=1);

namespace OrienteeringManager\Model;

use OrienteeringManager\Model\Page\PageModelInterface;
use Symfony\Component\Serializer\Annotation\Groups;

trait ItemPaginationPageModelTrait
{
    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    protected int $itemPerPages = 6;

    public function getItemPerPages(): int
    {
        return $this->itemPerPages;
    }

    public function setItemPerPages(int $itemPerPages): void
    {
        $this->itemPerPages = $itemPerPages;
    }
}
