<?php

namespace OrienteeringManager\Model;

interface LocationInterface
{
    public function getName(): ?string;
    public function getLatitude(): ?float;
    public function getLongitude(): ?float;
}