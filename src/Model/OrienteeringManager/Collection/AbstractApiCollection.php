<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Collection;

use OrienteeringManager\Model\OrienteeringManager\Filter\ApiFilterInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractApiCollection implements ApiCollectionInterface
{
    #[SerializedName('hydra:totalItems')]
    protected int $totalItems = 0;

    #[SerializedName('hydra:member')]
    #[Assert\Valid]
    protected array $items = [];
    
    public function __construct(
        private readonly ?ApiFilterInterface $filter = null,
    ) {
    }

    public function getTotalItems(): int
    {
        return $this->totalItems;
    }

    public function setTotalItems(int $totalItems): void
    {
        $this->totalItems = $totalItems;
    }

    abstract function getItems(): array;
    abstract function setItems(array $items): void;

    public function getFilter(): ?ApiFilterInterface
    {
        return $this->filter;
    }
}
