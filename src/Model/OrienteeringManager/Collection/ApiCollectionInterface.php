<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Collection;

use OrienteeringManager\Model\OrienteeringManager\Filter\ApiFilterInterface;
use OrienteeringManager\Model\OrienteeringManager\IriableInterface;

interface ApiCollectionInterface extends IriableInterface
{
    public function getIri(): string;
    public function getType(): string;
    public function getFilter(): ?ApiFilterInterface;
    public function getItems(): array;
    public function getTotalItems(): int;
}