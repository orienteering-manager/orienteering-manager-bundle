<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Collection;

use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;

final class EventApiCollection extends AbstractApiCollection
{
    public function getIri(): string
    {
        return '/api/events';
    }

    public function getType(): string
    {
        return EventApiItem::class;
    }

    /**
     * @return EventApiItem[]
     */
    function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param EventApiItem[] $items
     */
    function setItems(array $items): void
    {
        $this->items = $items;
    }
}
