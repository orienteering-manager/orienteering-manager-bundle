<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Collection;

use OrienteeringManager\Model\OrienteeringManager\Filter\EventRegistrationApiFilter;
use OrienteeringManager\Model\OrienteeringManager\Item\EventRegistrationApiItem;

final class EventRegistrationApiCollection extends AbstractApiCollection implements ExportableApiCollectionInterface
{
    public function getIri(): string
    {
        return '/api/event_registrations';
    }

    public function getExportIri(): string
    {
        /** @var EventRegistrationApiFilter $eventRegistrationFilter */
        $eventRegistrationFilter = $this->getFilter();

        return sprintf('%s/registrations/export/', $eventRegistrationFilter->getEvent());
    }

    public function getType(): string
    {
        return EventRegistrationApiItem::class;
    }

    /**
     * @return EventRegistrationApiItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param EventRegistrationApiItem[] $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    public function addItem(EventRegistrationApiItem $item): void
    {
        $this->items[] = $item;
    }

    public function removeItem(int $index, EventRegistrationApiItem $item): void
    {
        unset($this->items[$index]);
    }
}
