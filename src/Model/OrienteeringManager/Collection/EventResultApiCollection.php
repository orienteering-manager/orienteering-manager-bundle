<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Collection;

use OrienteeringManager\Model\OrienteeringManager\Item\EventResultApiItem;

final class EventResultApiCollection extends AbstractApiCollection
{
    public function getIri(): string
    {
        return '/api/event_results';
    }

    public function getType(): string
    {
        return EventResultApiItem::class;
    }

    /**
     * @return EventResultApiItem[]
     */
    function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param EventResultApiItem[] $items
     */
    function setItems(array $items): void
    {
        $this->items = $items;
    }
}
