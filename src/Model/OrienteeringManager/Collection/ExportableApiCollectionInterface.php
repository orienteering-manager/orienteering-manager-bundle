<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Collection;

interface ExportableApiCollectionInterface
{
    public function getExportIri(): string;
}