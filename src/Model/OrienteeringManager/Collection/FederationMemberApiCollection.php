<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Collection;

use OrienteeringManager\Model\OrienteeringManager\Item\FederationMemberApiItem;

final class FederationMemberApiCollection extends AbstractApiCollection
{
    public function getIri(): string
    {
        return '/api/federation_members';
    }

    public function getType(): string
    {
        return FederationMemberApiItem::class;
    }

    /**
     * @return FederationMemberApiItem[]
     */
    function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param FederationMemberApiItem[] $items
     */
    function setItems(array $items): void
    {
        $this->items = $items;
    }
}
