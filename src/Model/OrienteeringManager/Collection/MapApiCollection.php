<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Collection;

use OrienteeringManager\Model\OrienteeringManager\Item\MapApiItem;

final class MapApiCollection extends AbstractApiCollection
{
    public function getIri(): string
    {
        return '/api/maps';
    }

    public function getType(): string
    {
        return MapApiItem::class;
    }

    /**
     * @return MapApiItem[]
     */
    function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param MapApiItem[] $items
     */
    function setItems(array $items): void
    {
        $this->items = $items;
    }
}
