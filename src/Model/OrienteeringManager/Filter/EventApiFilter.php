<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Filter;

use DateTime;
use OrienteeringManager\Config\Enum\ModeEnum;
use Symfony\Component\Serializer\Annotation\SerializedName;

final class EventApiFilter implements ApiFilterInterface, PaginationApiFilterInterface
{
    public const int DEFAULT_ITEMS_PER_PAGE = 20;

    use PaginationApiFilterTrait;
    use SearchQueryApiFilterTrait;

    #[SerializedName('date[after]')]
    private ?DateTime $dateAfter;
    #[SerializedName('date[before]')]
    private ?DateTime $dateBefore = null;
    private ?int $federationMemberId = null;
    private ModeEnum $mode = ModeEnum::VIEW;

    public function __construct()
    {
        $this->dateAfter = new DateTime();
        $this->dateAfter->setTime(0, 0);
    }

    public function getDateAfter(): ?DateTime
    {
        return $this->dateAfter;
    }

    public function setDateAfter(?DateTime $dateAfter): void
    {
        $this->dateAfter = $dateAfter;
    }

    public function getDateBefore(): ?DateTime
    {
        return $this->dateBefore;
    }

    public function setDateBefore(?DateTime $dateBefore): void
    {
        $this->dateBefore = $dateBefore;
    }

    public function getFederationMemberId(): ?int
    {
        return $this->federationMemberId;
    }

    public function setFederationMemberId(?int $federationMemberId): EventApiFilter
    {
        $this->federationMemberId = $federationMemberId;
        return $this;
    }

    public function getMode(): ?ModeEnum
    {
        return $this->mode;
    }

    public function setMode(?ModeEnum $mode): EventApiFilter
    {
        $this->mode = $mode;
        return $this;
    }

    public function getOrder(): array
    {
        return [
            'date' => 'ASC',
            'name' => 'ASC',
        ];
    }
}
