<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Filter;

use DateTime;
use OrienteeringManager\Config\Enum\EventRegistrationStatusEnum;
use OrienteeringManager\Config\Enum\ModeEnum;
use Symfony\Component\Serializer\Annotation\SerializedName;

final class EventRegistrationApiFilter implements ApiFilterInterface, PaginationApiFilterInterface
{
    use PaginationApiFilterTrait;

    private string|null $event = null;
    #[SerializedName('event.date[after]')]
    private DateTime|null $eventDateAfter = null;
    /**
     * @var EventRegistrationStatusEnum[]|null
     */
    private ?array $status = null;
    private ModeEnum $mode = ModeEnum::VIEW;
    
    public function getEvent(): string|null
    {
        return $this->event;
    }
    
    public function setEvent(?string $event): void
    {
        $this->event = $event;
    }

    public function getEventDateAfter(): DateTime|null
    {
        return $this->eventDateAfter;
    }

    public function setEventDateAfter(DateTime|null $eventDateAfter): void
    {
        $this->eventDateAfter = $eventDateAfter;
    }

    /**
     * @return EventRegistrationStatusEnum[]|null
     */
    public function getStatus(): ?array
    {
        return $this->status;
    }

    /**
     * @param EventRegistrationStatusEnum[]|null $status
     */
    public function setStatus(array|null $status): void
    {
        $this->status = $status;
    }

    public function getMode(): ?ModeEnum
    {
        return $this->mode;
    }

    public function setMode(?ModeEnum $mode): void
    {
        $this->mode = $mode;
    }
}
