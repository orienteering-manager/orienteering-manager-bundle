<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Filter;

use DateTime;
use OrienteeringManager\Config\Enum\EventRegistrationStatusEnum;
use Symfony\Component\Serializer\Annotation\SerializedName;

final class EventRegistrationApiFilterTrait implements ApiFilterInterface, PaginationApiFilterInterface
{
    use PaginationApiFilterTrait;

    private string|null $event = null;
    #[SerializedName('event.date[after]')]
    private DateTime|null $eventDateAfter = null;
    /**
     * @var EventRegistrationStatusEnum[]|null
     */
    private ?array $status = null;
    
    public function getEvent(): string|null
    {
        return $this->event;
    }
    
    public function setEvent(?string $event): void
    {
        $this->event = $event;
    }

    public function getEventDateAfter(): DateTime|null
    {
        return $this->eventDateAfter;
    }

    public function setEventDateAfter(DateTime|null $eventDateAfter): void
    {
        $this->eventDateAfter = $eventDateAfter;
    }

    /**
     * @return EventRegistrationStatusEnum[]|null
     */
    public function getStatus(): ?array
    {
        return $this->status;
    }

    /**
     * @param EventRegistrationStatusEnum[]|null $status
     */
    public function setStatus(array|null $status): void
    {
        $this->status = $status;
    }
}
