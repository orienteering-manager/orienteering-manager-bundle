<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Filter;

use DateTime;
use OrienteeringManager\Config\Enum\ModeEnum;
use Symfony\Component\Serializer\Annotation\SerializedName;

final class EventResultApiFilter implements ApiFilterInterface, PaginationApiFilterInterface
{
    public const int DEFAULT_ITEMS_PER_PAGE = 20;

    use PaginationApiFilterTrait;

    #[SerializedName('event.date[after]')]
    private ?DateTime $dateAfter = null;
    #[SerializedName('event.date[before]')]
    private ?DateTime $dateBefore;
    #[SerializedName('exists[files]')]
    private ?bool $filesExists = null;
    private ModeEnum $mode = ModeEnum::VIEW;

    public function __construct()
    {
        $this->dateBefore = new DateTime();
    }

    public function getDateAfter(): ?DateTime
    {
        return $this->dateAfter;
    }

    public function setDateAfter(?DateTime $dateAfter): void
    {
        $this->dateAfter = $dateAfter;
    }

    public function getDateBefore(): ?DateTime
    {
        return $this->dateBefore;
    }

    public function setDateBefore(?DateTime $dateBefore): void
    {
        $this->dateBefore = $dateBefore;
    }

    public function getFilesExists(): ?bool
    {
        return $this->filesExists;
    }

    public function setFilesExists(?bool $filesExists): EventResultApiFilter
    {
        $this->filesExists = $filesExists;

        return $this;
    }

    public function getMode(): ?ModeEnum
    {
        return $this->mode;
    }

    public function setMode(?ModeEnum $mode): EventResultApiFilter
    {
        $this->mode = $mode;

        return $this;
    }

    public function getOrder(): array
    {
        return [
            'event.date' => 'DESC',
            'event.name' => 'ASC',
        ];
    }
}
