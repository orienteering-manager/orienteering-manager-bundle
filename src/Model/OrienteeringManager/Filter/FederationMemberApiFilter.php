<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Filter;

final class FederationMemberApiFilter implements ApiFilterInterface, PaginationApiFilterInterface
{
    use PaginationApiFilterTrait;
    use SearchQueryApiFilterTrait;

    public function getOrder(): array
    {
        return [
            'lastName' => 'ASC',
            'firstName' => 'ASC',
        ];
    }
}
