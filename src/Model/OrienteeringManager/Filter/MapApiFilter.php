<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Filter;

use OrienteeringManager\Config\Enum\ModeEnum;

final class MapApiFilter implements ApiFilterInterface, PaginationApiFilterInterface
{
    use PaginationApiFilterTrait;

    private ModeEnum $mode = ModeEnum::VIEW;
    private ?string $city = null;

    public function getMode(): ?ModeEnum
    {
        return $this->mode;
    }

    public function setMode(?ModeEnum $mode): MapApiFilter
    {
        $this->mode = $mode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): MapApiFilter
    {
        $this->city = $city;

        return $this;
    }

    public function getOrder(): array
    {
        return [
            'city' => 'ASC',
            'name' => 'ASC',
        ];
    }
}
