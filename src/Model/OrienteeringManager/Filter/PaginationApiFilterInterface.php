<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Filter;

interface PaginationApiFilterInterface
{
    public function setPagination(bool $pagination): void;
    public function setItemsPerPage(?int $itemsPerPage): void;
}