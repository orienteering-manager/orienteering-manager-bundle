<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Filter;

trait PaginationApiFilterTrait
{
    private bool $pagination = false;
    private ?int $itemsPerPage = null;
    private int $page = 1;

    public function isPagination(): bool
    {
        return $this->pagination;
    }

    public function setPagination(bool $pagination): void
    {
        $this->pagination = $pagination;
    }

    public function getItemsPerPage(): ?int
    {
        return $this->itemsPerPage;
    }

    public function setItemsPerPage(?int $itemsPerPage): void
    {
        if (is_int($itemsPerPage)) {
            $this->pagination = true;
            $itemsPerPage = max(1, $itemsPerPage);
        }

        $this->itemsPerPage = $itemsPerPage;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): void
    {
        $this->page = $page;
    }
}
