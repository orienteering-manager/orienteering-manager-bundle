<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Filter;

trait SearchQueryApiFilterTrait
{
    private ?string $query = null;

    public function getQuery(): ?string
    {
        return $this->query;
    }

    public function setQuery(?string $query): static
    {
        $this->query = $query;

        return $this;
    }
}
