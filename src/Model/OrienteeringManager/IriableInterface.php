<?php

namespace OrienteeringManager\Model\OrienteeringManager;

interface IriableInterface
{
    public function getIri(): string|null;
}