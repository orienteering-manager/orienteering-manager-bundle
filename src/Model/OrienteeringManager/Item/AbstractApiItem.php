<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use OrienteeringManager\Model\OrienteeringManager\Filter\ApiFilterInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Attribute\Groups;

abstract class AbstractApiItem implements ApiItemInterface
{
    #[SerializedName('@id')]
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    protected ?string $iri = null;
    
    public function __construct(
        #[Groups([
            ApiItemInterface::API_NORMALIZER_GROUP,
        ])]
        protected int|string|null $id = null,
        private readonly ?ApiFilterInterface $filter = null,
    ) {
        if (null !== $this->id) {
            $this->iri = sprintf('/api/%s/%s', static::getApiName(), static::getId());
        }
    }

    public function getId(): int|string|null
    {
        return $this->id;
    }

    public function setId(int|string $id): void
    {
        $this->id = $id;
    }

    public function getIri(): ?string
    {
        return $this->iri;
    }

    public function setIri(string $iri): self
    {
        $this->iri = $iri;

        return $this;
    }

    public function getFilter(): ?ApiFilterInterface
    {
        return $this->filter;
    }
}