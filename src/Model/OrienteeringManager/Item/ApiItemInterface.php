<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use OrienteeringManager\Model\OrienteeringManager\IriableInterface;

interface ApiItemInterface extends IriableInterface
{
    final public const string API_NORMALIZER_GROUP = 'group.normalizer.api_item';

    public static function getApiName(): string;
    public function getId(): int|string|null;
    public function getIri(): string|null;
}