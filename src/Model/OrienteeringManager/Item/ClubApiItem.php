<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use Symfony\Component\Serializer\Attribute\Groups;

final class ClubApiItem extends AbstractApiItem implements ApiItemInterface
{
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string $name;

    public static function getApiName(): string
    {
        return 'clubs';
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
