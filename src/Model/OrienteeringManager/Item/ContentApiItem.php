<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use OrienteeringManager\Config\Enum\AuthorizationLevelEnum;
use Symfony\Component\Serializer\Attribute\Groups;

abstract class ContentApiItem extends AbstractApiItem
{
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    protected ?SiteApiItem $site = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    protected ?AuthorizationLevelEnum $level = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    protected bool $private = false;

    public function getSite(): ?SiteApiItem
    {
        return $this->site;
    }

    public function setSite(?SiteApiItem $site): void
    {
        $this->site = $site;
    }

    public function getLevel(): ?AuthorizationLevelEnum
    {
        return $this->level;
    }

    public function setLevel(AuthorizationLevelEnum|int|null $level): void
    {
        if (is_int($level)) {
            $level = AuthorizationLevelEnum::tryFrom($level);
        }

        $this->level = $level;
    }

    public function isPrivate(): bool
    {
        return $this->private;
    }

    public function setPrivate(bool $private): void
    {
        $this->private = $private;
    }
}