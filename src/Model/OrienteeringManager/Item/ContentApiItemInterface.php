<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

interface ContentApiItemInterface extends ApiItemInterface
{
    public function getTemplate(): string;
    public function getName(): string;
    public function getItemColor(): string;

    /**
     * @return array{name: string, parameters: array}
     */
    public function getContentUrl(): array;
}