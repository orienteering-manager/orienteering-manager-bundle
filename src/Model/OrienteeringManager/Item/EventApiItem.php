<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use DateInterval;
use DateTime;
use OrienteeringManager\Config\Attribute\Uploadable;
use OrienteeringManager\Config\Attribute\UploadableField;
use OrienteeringManager\Config\Constant\ColorConstant;
use OrienteeringManager\Config\Enum\EventFormatEnum;
use OrienteeringManager\Config\Enum\EventStatusEnum;
use OrienteeringManager\Model\OrienteeringManager\Filter\ApiFilterInterface;
use Stringable;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

#[Uploadable]
final class EventApiItem extends ContentApiItem implements ContentApiItemInterface, Stringable
{
    use MetadataTrait;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string $name = '';

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string|null $imagePath = null;
    
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string $slug;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private DateTime $date;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    #[GreaterThanOrEqual(propertyPath: 'date')]
    private ?DateTime $endDate = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private EventStatusEnum|null $status = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string|null $organizer = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    #[Assert\Url]
    private string|null $organizerWebsite = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private EventFormatEnum|null $format = null;
    
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?EventLocationApiItem $location = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $allowRegistration = false;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $registrationNeedValidation = false;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string|null $registrationDescription = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $useCategory = false;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?DateTime $registrationLimitDate = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $registrationUrl = null;

    /** @var EventRegistrationApiItem[] $registrations */
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private array $registrations;

    /** @var EventTrackApiItem[] $tracks */
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private array $tracks = [];

    /** @var EventPriceApiItem[] $tracks */
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private array $prices;
    
    /** @var EventFileApiItem[] $files */
    #[Uploadable]
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private array $files = [];

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $content = null;

    #[Uploadable]
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?EventResultApiItem $result = null;

    #[UploadableField(
        nameProperties: 'name',
        targetFolder: '/media/event',
        urlField: 'imagePath',
        enableVersioning: true,
        deletionField: 'deleteImage'
    )]
    private ?File $image = null;
    private bool $deleteImage = false;
    
    private bool $currentUserRegistered = false;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $allowHelpColorTracks = false;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $allowHelpColorTrackDark = false;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $allowHelpColorTrackPurple = false;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $allowHelpColorTrackOrange = false;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $allowHelpColorTrackYellow = false;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $allowHelpColorTrackGreen = false;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $allowHelpColorTrackBlue = false;

    /** @var EventCategoryApiItem[] $categories */
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private array $categories;

    public function __construct(
        int|string|null $id = null,
        ?ApiFilterInterface $filter = null,
    ) {
        parent::__construct($id, $filter);

        $this->date = new DateTime('+7 days');
        $this->date->setTime(10, 0);
    }

    public function getTemplate(): string
    {
        return '@OrienteeringManager/content/event/view.html.twig';
    }

    public static function getApiName(): string
    {
        return 'events';
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getImagePath(): ?string
    {
        return $this->imagePath;
    }

    public function setImagePath(?string $imagePath): self
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getEndDate(): ?DateTime
    {
        return $this->endDate;
    }

    public function setEndDate(?DateTime $endDate): void
    {
        $this->endDate = $endDate;
    }
    
    public function getStatus(): ?EventStatusEnum
    {
        return $this->status;
    }
    
    public function setStatus(EventStatusEnum|int|null $status): void
    {
        if (is_int($status)) {
            $status = EventStatusEnum::tryFrom($status);
        }

        $this->status = $status;
    }

    public function getCategories(): array
    {
        return $this->categories;
    }

    public function setCategories(array $categories): void
    {
        $this->categories = $categories;
    }

    public function addCategory(EventCategoryApiItem $category): void
    {
        $this->categories[] = $category;
    }

    public function getOrganizer(): ?string
    {
        return $this->organizer;
    }

    public function setOrganizer(?string $organizer): void
    {
        $this->organizer = $organizer;
    }

    public function getOrganizerWebsite(): ?string
    {
        return $this->organizerWebsite;
    }

    public function setOrganizerWebsite(?string $organizerWebsite): void
    {
        $this->organizerWebsite = $organizerWebsite;
    }

    public function getFormat(): ?EventFormatEnum
    {
        return $this->format;
    }

    public function setFormat(EventFormatEnum|int|null $format): void
    {
        if (is_int($format)) {
            $format = EventFormatEnum::tryFrom($format);
        }

        $this->format = $format;
    }

    public function getLocation(): ?EventLocationApiItem
    {
        return $this->location;
    }

    public function setLocation(?EventLocationApiItem $location): void
    {
        $this->location = $location;
    }

    public function isAllowRegistration(): bool
    {
        return $this->allowRegistration;
    }

    public function setAllowRegistration(bool $allowRegistration): void
    {
        $this->allowRegistration = $allowRegistration;
    }

    public function isRegistrationNeedValidation(): bool
    {
        return $this->registrationNeedValidation;
    }

    public function setRegistrationNeedValidation(bool $registrationNeedValidation): void
    {
        $this->registrationNeedValidation = $registrationNeedValidation;
    }

    public function getRegistrationDescription(): ?string
    {
        return $this->registrationDescription;
    }

    public function setRegistrationDescription(?string $registrationDescription): void
    {
        $this->registrationDescription = $registrationDescription;
    }

    public function isUseCategory(): bool
    {
        return $this->useCategory;
    }

    public function setUseCategory(bool $useCategory): void
    {
        $this->useCategory = $useCategory;
    }

    public function getRegistrationLimitDate(): ?DateTime
    {
        return $this->registrationLimitDate;
    }

    public function setRegistrationLimitDate(?DateTime $registrationLimitDate): void
    {
        $this->registrationLimitDate = $registrationLimitDate;
    }

    public function getRegistrationUrl(): ?string
    {
        return $this->registrationUrl;
    }

    public function setRegistrationUrl(?string $registrationUrl): void
    {
        $this->registrationUrl = $registrationUrl;
    }

    public function getRegistrations(): array
    {
        return $this->registrations;
    }

    public function setRegistrations(array $registrations): void
    {
        $this->registrations = $registrations;
    }

    public function getTracks(): array
    {
        return $this->tracks;
    }

    public function setTracks(array $tracks): void
    {
        $this->tracks = $tracks;
    }

    public function addTrack(EventTrackApiItem $track): void
    {
        $this->tracks[] = $track;
    }

    public function getPrices(): array
    {
        return $this->prices;
    }

    public function setPrices(array $prices): void
    {
        $this->prices = $prices;
    }

    public function getFiles(): array
    {
        return $this->files;
    }

    public function setFiles(array $files): void
    {
        $this->files = $files;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): void
    {
        $this->content = $content;
    }

    public function getResult(): ?EventResultApiItem
    {
        return $this->result;
    }

    public function setResult(?EventResultApiItem $result): void
    {
        $this->result = $result;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(?File $image): void
    {
        $this->image = $image;
    }

    public function isDeleteImage(): bool
    {
        return $this->deleteImage;
    }

    public function setDeleteImage(bool $deleteImage): void
    {
        $this->deleteImage = $deleteImage;
    }

    public function isCurrentUserRegistered(): bool
    {
        return $this->currentUserRegistered;
    }

    public function setCurrentUserRegistered(bool $currentUserRegistered): void
    {
        $this->currentUserRegistered = $currentUserRegistered;
    }
    
    public function getItemColor(): string
    {
        if ($this->getDate() < new DateTime()) {
            return ColorConstant::MUTED;
        }

        if (null !== $this->getStatus()) {
            return match ($this->getStatus()) {
                EventStatusEnum::CANCELLED => ColorConstant::DANGER,
                default => ColorConstant::WARNING,
            };
        }
        
        if (null !== $this->getResult() && $this->getResult()->isLive()) {
            return ColorConstant::SUCCESS;
        }
        
        return ColorConstant::PRIMARY;
    }

    public function getContentUrl(): array
    {
        return [
            'name' => 'app_content_view',
            'parameters' => [
                'id' => $this->id,
                'slug' => $this->slug,
            ],
        ];
    }

    public function __clone(): void
    {
        $this->id = null;
        $this->iri = null;
        $this->date = $this->date->add(new DateInterval('P7D'));
        $this->registrationLimitDate = $this->registrationLimitDate?->add(new DateInterval('P7D'));
        $this->createdAt = null;
        $this->updatedAt = null;
    }

    public function __toString(): string
    {
        return sprintf('%s (#%s)', $this->name, $this->id);
    }

    public function isAllowHelpColorTracks(): bool
    {
        return $this->allowHelpColorTracks;
    }

    public function setAllowHelpColorTracks(bool $allowHelpColorTracks): void
    {
        $this->allowHelpColorTracks = $allowHelpColorTracks;
    }

    public function isAllowHelpColorTrackDark(): bool
    {
        return $this->allowHelpColorTrackDark;
    }

    public function setAllowHelpColorTrackDark(bool $allowHelpColorTrackDark): void
    {
        $this->allowHelpColorTrackDark = $allowHelpColorTrackDark;
    }

    public function isAllowHelpColorTrackPurple(): bool
    {
        return $this->allowHelpColorTrackPurple;
    }

    public function setAllowHelpColorTrackPurple(bool $allowHelpColorTrackPurple): void
    {
        $this->allowHelpColorTrackPurple = $allowHelpColorTrackPurple;
    }

    public function isAllowHelpColorTrackOrange(): bool
    {
        return $this->allowHelpColorTrackOrange;
    }

    public function setAllowHelpColorTrackOrange(bool $allowHelpColorTrackOrange): void
    {
        $this->allowHelpColorTrackOrange = $allowHelpColorTrackOrange;
    }

    public function isAllowHelpColorTrackYellow(): bool
    {
        return $this->allowHelpColorTrackYellow;
    }

    public function setAllowHelpColorTrackYellow(bool $allowHelpColorTrackYellow): void
    {
        $this->allowHelpColorTrackYellow = $allowHelpColorTrackYellow;
    }

    public function isAllowHelpColorTrackGreen(): bool
    {
        return $this->allowHelpColorTrackGreen;
    }

    public function setAllowHelpColorTrackGreen(bool $allowHelpColorTrackGreen): void
    {
        $this->allowHelpColorTrackGreen = $allowHelpColorTrackGreen;
    }

    public function isAllowHelpColorTrackBlue(): bool
    {
        return $this->allowHelpColorTrackBlue;
    }

    public function setAllowHelpColorTrackBlue(bool $allowHelpColorTrackBlue): void
    {
        $this->allowHelpColorTrackBlue = $allowHelpColorTrackBlue;
    }
}
