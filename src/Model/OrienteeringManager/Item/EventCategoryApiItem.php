<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use Stringable;
use Symfony\Component\Serializer\Attribute\Groups;

final class EventCategoryApiItem extends AbstractApiItem implements ApiItemInterface, Stringable
{
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string|null $name = null;

    public static function getApiName(): string
    {
        return 'event_categories';
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function __toString(): string
    {
        return (string) $this->iri;
    }
}
