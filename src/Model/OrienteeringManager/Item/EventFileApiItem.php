<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use OrienteeringManager\Config\Attribute\Uploadable;
use OrienteeringManager\Config\Attribute\UploadableField;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Attribute\Groups;

#[Uploadable]
final class EventFileApiItem extends AbstractApiItem implements ApiItemInterface
{
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string $name;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string $content;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string|null $filePath = null;

    #[UploadableField(
        nameProperties: 'name',
        targetFolder: '/media/event',
        urlField: 'filePath',
        enableVersioning: true,
    )]
    private ?File $file = null;

    public static function getApiName(): string
    {
        return 'event_files';
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getContent(): File|string
    {
        return $this->file ?? $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getFilePath(): string|null
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath): void
    {
        $this->filePath = $filePath;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): void
    {
        $this->file = $file;
    }

}
