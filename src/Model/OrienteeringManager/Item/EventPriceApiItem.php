<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use Stringable;
use Symfony\Component\Serializer\Attribute\Groups;

final class EventPriceApiItem extends AbstractApiItem implements ApiItemInterface, Stringable
{
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private float $amount;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $federationMember;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $categoryStart = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $categoryEnd = null;

    public static function getApiName(): string
    {
        return 'event_prices';
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    public function isFederationMember(): bool
    {
        return $this->federationMember;
    }

    public function setFederationMember(bool $federationMember): void
    {
        $this->federationMember = $federationMember;
    }

    public function getCategoryStart(): ?string
    {
        return $this->categoryStart;
    }

    public function setCategoryStart(?string $categoryStart): void
    {
        $this->categoryStart = $categoryStart;
    }

    public function getCategoryEnd(): ?string
    {
        return $this->categoryEnd;
    }

    public function setCategoryEnd(?string $categoryEnd): void
    {
        $this->categoryEnd = $categoryEnd;
    }

    public function __toString(): string
    {
        return (string) $this->iri;
    }
}
