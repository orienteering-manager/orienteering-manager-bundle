<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use OrienteeringManager\Config\Enum\EventRegistrationStatusEnum;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

final class EventRegistrationApiItem extends AbstractApiItem implements ApiItemInterface
{
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private FederationMemberApiItem|string|null $federationMember = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $federationMemberIri = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $notFederationMember = false;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $firstName = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $lastName = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $cardNumber = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private EventTrackApiItem|string|null $track = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $trackIri = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $comment = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private EventApiItem|string|null $event = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private EventRegistrationStatusEnum $status;

    public static function getApiName(): string
    {
        return 'event_registrations';
    }

    public function getFederationMember(): FederationMemberApiItem|string|null
    {
        return $this->federationMember;
    }

    public function setFederationMember(FederationMemberApiItem|string|null $federationMember): void
    {
        $this->federationMember = $federationMember;

        if ($federationMember instanceof FederationMemberApiItem) {
            $this->federationMemberIri = $federationMember->getIri();
        }
    }

    public function getFederationMemberIri(): ?string
    {
        return $this->federationMemberIri;
    }

    public function setFederationMemberIri(?string $federationMemberIri): void
    {
        $this->federationMemberIri = $federationMemberIri;
    }

    public function isNotFederationMember(): bool
    {
        return $this->notFederationMember;
    }

    public function setNotFederationMember(bool $notFederationMember): void
    {
        $this->notFederationMember = $notFederationMember;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getCardNumber(): ?string
    {
        return $this->cardNumber;
    }

    public function setCardNumber(?string $cardNumber): void
    {
        $this->cardNumber = $cardNumber;
    }

    public function getTrack(): EventTrackApiItem|string|null
    {
        return $this->track;
    }

    public function setTrack(EventTrackApiItem|string|null $track): void
    {
        $this->track = $track;

        if ($track instanceof EventTrackApiItem) {
            $this->trackIri = $track->getIri();
        }
    }

    public function getTrackIri(): ?string
    {
        return $this->trackIri;
    }

    public function setTrackIri(?string $trackIri): void
    {
        $this->trackIri = $trackIri;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    public function getEvent(): EventApiItem|string|null
    {
        return $this->event;
    }

    public function setEvent(EventApiItem|string|null $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getStatus(): EventRegistrationStatusEnum
    {
        return $this->status;
    }

    public function setStatus(EventRegistrationStatusEnum $status): self
    {
        $this->status = $status;

        return $this;
    }

    #[Assert\Callback]
    public function validate(ExecutionContextInterface $context): void
    {
        if (null === $this->federationMember && null === $this->federationMemberIri && false === $this->notFederationMember) {
            $context->buildViolation('registration.field.federationMember.error')
                ->atPath('federationMemberIri')
                ->addViolation();
        }
    }

    public function __toString(): string
    {
        return $this->federationMember instanceof FederationMemberApiItem ?
            sprintf('%s %s', $this->federationMember->getFirstName(), $this->federationMember->getLastName()) :
            sprintf('%s %s', $this->firstName, $this->lastName);
    }
}
