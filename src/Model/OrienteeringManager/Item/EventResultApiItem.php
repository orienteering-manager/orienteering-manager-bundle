<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use OrienteeringManager\Config\Attribute\Uploadable;
use OrienteeringManager\Config\Attribute\UploadableField;
use OrienteeringManager\Config\Constant\ColorConstant;
use Stringable;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Attribute\Groups;

#[Uploadable]
final class EventResultApiItem extends AbstractApiItem implements ContentApiItemInterface, Stringable
{
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $live;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string|null $liveFilePath = null;

    /** @var EventResultFileApiItem[] $files */
    #[Uploadable]
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private array $files = [];

    private ?EventApiItem $event = null;

    #[UploadableField(
        nameProperties: ['event.id', 'event.name'],
        targetFolder: '/media/result',
        urlField: 'liveFilePath',
        enableVersioning: false,
    )]
    private ?File $liveFile = null;

    public function getTemplate(): string
    {
        return '@OrienteeringManager/content/event_result/view.html.twig';
    }

    public function getName(): string
    {
        return $this->getEventName();
    }

    public static function getApiName(): string
    {
        return 'event_results';
    }

    public function isLive(): bool
    {
        return $this->live;
    }

    public function setLive(bool $live): void
    {
        $this->live = $live;
    }

    public function getLiveFilePath(): string|null
    {
        return $this->liveFilePath;
    }

    public function setLiveFilePath(?string $liveFilePath): void
    {
        $this->liveFilePath = $liveFilePath;
    }

    public function getFiles(): array
    {
        return $this->files;
    }

    public function setFiles(array $files): void
    {
        $this->files = $files;
    }

    public function getEvent(): ?EventApiItem
    {
        return $this->event;
    }

    public function setEvent(?EventApiItem $event): EventResultApiItem
    {
        $this->event = $event;

        return $this;
    }

    public function getLiveFile(): ?File
    {
        return $this->liveFile;
    }

    public function setLiveFile(?File $liveFile): void
    {
        $this->liveFile = $liveFile;
    }

    public function getEventName(): ?string
    {
        return $this->event?->getName();
    }

    public function getItemColor(): string
    {
        return is_string($this->liveFilePath) && empty($this->files) ?
            ColorConstant::SUCCESS
            : ColorConstant::PRIMARY;
    }

    public function getContentUrl(): array
    {
        return [
            'name' => 'app_content_view',
            'parameters' => [
                'id' => $this->id,
                'slug' => $this->event?->getSlug(),
            ],
        ];
    }

    public function __toString(): string
    {
        return sprintf('%s (#%s)', $this->event?->getName() ?? 'Undefined', $this->id);
    }
}
