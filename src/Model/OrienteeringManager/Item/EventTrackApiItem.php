<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use Stringable;
use Symfony\Component\Serializer\Attribute\Groups;

final class EventTrackApiItem extends AbstractApiItem implements ApiItemInterface, Stringable
{
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?int $identifier = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string $name;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $length = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $elevationGain = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?int $numberRegistrations = 0;

    public static function getApiName(): string
    {
        return 'event_tracks';
    }

    public function getIdentifier(): ?int
    {
        return $this->identifier;
    }

    public function setIdentifier(?int $identifier): void
    {
        $this->identifier = $identifier;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getLength(): ?string
    {
        return $this->length;
    }

    public function setLength(?string $length): void
    {
        $this->length = $length;
    }

    public function getElevationGain(): ?string
    {
        return $this->elevationGain;
    }

    public function setElevationGain(?string $elevationGain): void
    {
        $this->elevationGain = $elevationGain;
    }

    public function getNumberRegistrations(): ?int
    {
        return $this->numberRegistrations;
    }

    public function setNumberRegistrations(?int $numberRegistrations): void
    {
        $this->numberRegistrations = $numberRegistrations;
    }

    public function __toString(): string
    {
        return (string) $this->iri;
    }
}
