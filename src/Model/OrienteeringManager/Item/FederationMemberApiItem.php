<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use Symfony\Component\Serializer\Attribute\Groups;

final class FederationMemberApiItem extends AbstractApiItem implements ApiItemInterface
{
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string $firstName;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string $lastName;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $cardNumber = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string $category;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?ClubApiItem $club = null;

    public static function getApiName(): string
    {
        return 'federation_members';
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getCardNumber(): ?string
    {
        return $this->cardNumber;
    }

    public function setCardNumber(?string $cardNumber): void
    {
        $this->cardNumber = $cardNumber;
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function setCategory(string $category): void
    {
        $this->category = $category;
    }

    public function getClub(): ?ClubApiItem
    {
        return $this->club;
    }

    public function setClub(?ClubApiItem $club): void
    {
        $this->club = $club;
    }
}
