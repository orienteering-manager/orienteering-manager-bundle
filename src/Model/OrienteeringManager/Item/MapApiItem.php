<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use OrienteeringManager\Config\Attribute\Uploadable;
use OrienteeringManager\Config\Attribute\UploadableField;
use OrienteeringManager\Config\Constant\ColorConstant;
use OrienteeringManager\Model\LocationInterface;
use Stringable;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\String\Slugger\AsciiSlugger;

#[Uploadable]
final class MapApiItem extends ContentApiItem implements ContentApiItemInterface, Stringable, LocationInterface
{
    use MetadataTrait;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string $name = '';

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $unavailable;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string|null $city = null;
    private string|null $citySlug = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private float|null $latitude = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private float|null $longitude = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string|null $vikazimut = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $allowHelpUseVikazimut = false;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private bool $allowHelpReadMap = false;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string $slug;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string|null $filePath = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string|null $imagePath = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private string|null $content = null;

    #[UploadableField(
        nameProperties: 'name',
        targetFolder: '/media/map',
        urlField: 'filePath',
        enableVersioning: true,
    )]
    private ?File $file = null;

    #[UploadableField(
        nameProperties: 'name',
        targetFolder: '/media/map',
        urlField: 'imagePath',
        enableVersioning: true,
        deletionField: 'deleteImage'
    )]
    private ?File $image = null;
    private bool $deleteImage = false;

    public function getTemplate(): string
    {
        return '@OrienteeringManager/content/map/view.html.twig';
    }

    public static function getApiName(): string
    {
        return 'maps';
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function isUnavailable(): bool
    {
        return $this->unavailable;
    }

    public function setUnavailable(bool $unavailable): void
    {
        $this->unavailable = $unavailable;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    public function getCitySlug(): ?string
    {
        return $this->citySlug ?? (new AsciiSlugger())->slug($this->getCity())->lower()->toString();
    }

    public function setCitySlug(?string $citySlug): void
    {
        $this->citySlug = $citySlug;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    /**
     * @todo Change type to float after MEP.
     */
    public function setLatitude(float|string|null $latitude): void
    {
        $this->latitude = floatval($latitude);
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    /**
     * @todo Change type to float after MEP.
     */
    public function setLongitude(float|string|null $longitude): void
    {
        $this->longitude = floatval($longitude);
    }

    public function getVikazimut(): ?string
    {
        return $this->vikazimut;
    }

    public function setVikazimut(?string $vikazimut): void
    {
        $this->vikazimut = $vikazimut;
    }

    public function isAllowHelpUseVikazimut(): bool
    {
        return $this->allowHelpUseVikazimut;
    }

    public function setAllowHelpUseVikazimut(bool $allowHelpUseVikazimut): void
    {
        $this->allowHelpUseVikazimut = $allowHelpUseVikazimut;
    }

    public function isAllowHelpReadMap(): bool
    {
        return $this->allowHelpReadMap;
    }

    public function setAllowHelpReadMap(bool $allowHelpReadMap): void
    {
        $this->allowHelpReadMap = $allowHelpReadMap;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    public function getFilePath(): string|null
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath): void
    {
        $this->filePath = $filePath;
    }

    public function getImagePath(): ?string
    {
        return $this->imagePath;
    }

    public function setImagePath(?string $imagePath): self
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): void
    {
        $this->file = $file;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(?File $image): void
    {
        $this->image = $image;
    }

    public function isDeleteImage(): bool
    {
        return $this->deleteImage;
    }

    public function setDeleteImage(bool $deleteImage): void
    {
        $this->deleteImage = $deleteImage;
    }

    public function getItemColor(): string
    {
        return ColorConstant::PRIMARY;
    }

    public function getContentUrl(): array
    {
        return [
            'name' => 'app_content_view',
            'parameters' => [
                'id' => $this->id,
                'slug' => $this->slug,
            ],
        ];
    }

    public function __clone(): void
    {
        $this->id = null;
        $this->iri = null;
        $this->createdAt = null;
        $this->updatedAt = null;
    }

    public function __toString(): string
    {
        return sprintf('%s (#%s)', $this->name, $this->id);
    }
}
