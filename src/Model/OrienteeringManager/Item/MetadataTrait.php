<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use DateTimeInterface;
use Symfony\Component\Serializer\Annotation\SerializedPath;
use Symfony\Component\Serializer\Attribute\Groups;

trait MetadataTrait
{
    #[SerializedPath('[metadata][createdAt]')]
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    protected ?DateTimeInterface $createdAt = null;

    #[SerializedPath('[metadata][updatedAt]')]
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    protected ?DateTimeInterface $updatedAt = null;

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
