<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\OrienteeringManager\Item;

use Stringable;
use Symfony\Component\Serializer\Attribute\Groups;

final class SiteApiItem implements Stringable
{
    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?int $id = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $name = null;

    #[Groups([
        ApiItemInterface::API_NORMALIZER_GROUP,
    ])]
    private ?string $url = null;

    private bool $current = false;

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    public function isCurrent(): bool
    {
        return $this->current;
    }

    public function setCurrent(bool $current): void
    {
        $this->current = $current;
    }

    public function __toString(): string
    {
        return (string) $this->name;
    }
}
