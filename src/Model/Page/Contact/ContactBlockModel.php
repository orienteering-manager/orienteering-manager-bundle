<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\Page\Contact;

use OrienteeringManager\Model\Page\PageModelInterface;
use Symfony\Component\Serializer\Annotation\Groups;

final class ContactBlockModel
{
    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $role = null;
    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $firstName = null;
    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $lastName = null;
    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $email = null;
    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $phone = null;

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(?string $role): void
    {
        $this->role = $role;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }
}
