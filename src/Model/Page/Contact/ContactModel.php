<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\Page\Contact;

use OrienteeringManager\Form\Page\Contact\ContactPageType;
use OrienteeringManager\Model\Page\PageModelInterface;
use OrienteeringManager\Model\Page\SeoContentPageModelInterface;
use OrienteeringManager\Model\Page\SeoDataModel;
use OrienteeringManager\Model\SeoContentPageModelTrait;
use Symfony\Component\Serializer\Annotation\Groups;

final class ContactModel implements PageModelInterface, SeoContentPageModelInterface
{
    use SeoContentPageModelTrait;

    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $content = null;

    /**
     * @var ContactBlockModel[]
     */
    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private array $persons = [];

    public function __construct()
    {
        $this->seoData = new SeoDataModel();
    }

    public function getFormClass(): string
    {
        return ContactPageType::class;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return ContactBlockModel[]
     */
    public function getPersons(): array
    {
        return $this->persons;
    }

    /**
     * @param ContactBlockModel[] $persons
     */
    public function setPersons(array $persons): void
    {
        $this->persons = $persons;
    }

    public function addInformationBlock(ContactBlockModel $person): void
    {
        $this->persons[] = $person;
    }
}
