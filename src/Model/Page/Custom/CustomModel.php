<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\Page\Custom;

use OrienteeringManager\Form\Page\Custom\CustomPageType;
use OrienteeringManager\Model\Page\PageModelInterface;
use OrienteeringManager\Model\Page\SeoContentPageModelInterface;
use OrienteeringManager\Model\Page\SeoDataModel;
use OrienteeringManager\Model\SeoContentPageModelTrait;
use Symfony\Component\Serializer\Annotation\Groups;

final class CustomModel implements PageModelInterface, SeoContentPageModelInterface
{
    use SeoContentPageModelTrait;

    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $content = null;

    public function __construct()
    {
        $this->seoData = new SeoDataModel();
    }

    public function getFormClass(): string
    {
        return CustomPageType::class;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): void
    {
        $this->content = $content;
    }
}
