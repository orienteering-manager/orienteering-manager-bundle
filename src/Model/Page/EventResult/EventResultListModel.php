<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\Page\EventResult;

use OrienteeringManager\Form\Page\EventResult\EventResultListPageType;
use OrienteeringManager\Model\ItemPaginationPageModelTrait;
use OrienteeringManager\Model\OrienteeringManager\Item\EventResultApiItem;
use OrienteeringManager\Model\Page\PageModelInterface;
use OrienteeringManager\Model\Page\RootContentPageModelInterface;
use OrienteeringManager\Model\Page\SeoDataModel;
use OrienteeringManager\Model\Page\SeoContentPageModelInterface;
use OrienteeringManager\Model\SeoContentPageModelTrait;
use Symfony\Component\Serializer\Annotation\Groups;

final class EventResultListModel implements RootContentPageModelInterface, SeoContentPageModelInterface
{
    use SeoContentPageModelTrait;
    use ItemPaginationPageModelTrait;

    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $content = null;

    public function __construct()
    {
        $this->seoData = new SeoDataModel();
    }

    public function getFormClass(): string
    {
        return EventResultListPageType::class;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): void
    {
        $this->content = $content;
    }
    
    public function getSubResourceType(): string
    {
        return EventResultApiItem::class;
    }
}
