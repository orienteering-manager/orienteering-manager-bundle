<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\Page\Map;

use OrienteeringManager\Form\Page\Map\MapListPageType;
use OrienteeringManager\Model\OrienteeringManager\Item\MapApiItem;
use OrienteeringManager\Model\Page\PageModelInterface;
use OrienteeringManager\Model\Page\RootContentPageModelInterface;
use OrienteeringManager\Model\Page\SeoContentPageModelInterface;
use OrienteeringManager\Model\Page\SeoDataModel;
use OrienteeringManager\Model\SeoContentPageModelTrait;
use Symfony\Component\Serializer\Annotation\Groups;

final class MapListModel implements RootContentPageModelInterface, SeoContentPageModelInterface
{
    use SeoContentPageModelTrait;

    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $content = null;
    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $title = null;
    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $titleVikazimut = null;
    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $descriptionVikazimut = null;
    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $linkUrlVikazimut = null;
    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $linkLabelVikazimut = null;

    public function __construct()
    {
        $this->seoData = new SeoDataModel();
    }

    public function getFormClass(): string
    {
        return MapListPageType::class;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): void
    {
        $this->content = $content;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getTitleVikazimut(): ?string
    {
        return $this->titleVikazimut;
    }

    public function setTitleVikazimut(?string $titleVikazimut): void
    {
        $this->titleVikazimut = $titleVikazimut;
    }

    public function getDescriptionVikazimut(): ?string
    {
        return $this->descriptionVikazimut;
    }

    public function setDescriptionVikazimut(?string $descriptionVikazimut): void
    {
        $this->descriptionVikazimut = $descriptionVikazimut;
    }

    public function getLinkUrlVikazimut(): ?string
    {
        return $this->linkUrlVikazimut;
    }

    public function setLinkUrlVikazimut(?string $linkUrlVikazimut): void
    {
        $this->linkUrlVikazimut = $linkUrlVikazimut;
    }

    public function getLinkLabelVikazimut(): ?string
    {
        return $this->linkLabelVikazimut;
    }

    public function setLinkLabelVikazimut(?string $linkLabelVikazimut): void
    {
        $this->linkLabelVikazimut = $linkLabelVikazimut;
    }

    public function getSubResourceType(): string
    {
        return MapApiItem::class;
    }
}
