<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\Page;

interface PageModelInterface
{
    public const string GROUP_PAGE_MODEL = 'serializer.group.page_model';

    public function getFormClass(): string;
}
