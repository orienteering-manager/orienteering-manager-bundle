<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\Page;

interface RootContentPageModelInterface extends PageModelInterface
{
    public function getSubResourceType(): string;
}
