<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\Page;

interface SeoContentPageModelInterface extends PageModelInterface
{
    public function getSeoData(): SeoDataModel;
    public function setSeoData(SeoDataModel $seoData): static;
}
