<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\Page;

use Symfony\Component\Serializer\Annotation\Groups;

final class SeoDataModel
{
    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    private ?string $metaDescription = null;

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }
}
