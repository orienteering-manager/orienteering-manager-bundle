<?php

declare(strict_types=1);

namespace OrienteeringManager\Model;

use OrienteeringManager\Model\Page\PageModelInterface;
use OrienteeringManager\Model\Page\SeoDataModel;
use Symfony\Component\Serializer\Annotation\Groups;

trait SeoContentPageModelTrait
{
    #[Groups([PageModelInterface::GROUP_PAGE_MODEL])]
    protected ?SeoDataModel $seoData = null;

    public function getSeoData(): SeoDataModel
    {
        return $this->seoData;
    }

    public function setSeoData(SeoDataModel $seoDataModel): static
    {
        $this->seoData = $seoDataModel;

        return $this;
    }
}
