<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\Sitemap;

use Symfony\Component\Serializer\Annotation\SerializedName;

final class Sitemap
{
    /**
     * @var Url[]
     */
    private array $url = [];

    #[SerializedName('@xmlns')]
    public function getXmlns(): string
    {
        return 'https://www.sitemaps.org/schemas/sitemap/0.9';
    }

    public function getUrl(): array
    {
        return $this->url;
    }

    public function addUrl(Url $url): void
    {
        $this->url[] = $url;
    }
}
