<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\Sitemap;

use OrienteeringManager\Config\Enum\SitemapChangeFrequencyEnum;

final class SitemapPageConfiguration
{
    private SitemapChangeFrequencyEnum $frequency = SitemapChangeFrequencyEnum::MONTHLY;
    private float $priority = 0.5;

    public function getFrequency(): SitemapChangeFrequencyEnum
    {
        return $this->frequency;
    }

    public function setFrequency(SitemapChangeFrequencyEnum $frequency): SitemapPageConfiguration
    {
        $this->frequency = $frequency;
        return $this;
    }

    public function getPriority(): float
    {
        return $this->priority;
    }

    public function setPriority(float $priority): SitemapPageConfiguration
    {
        $this->priority = $priority;
        return $this;
    }
}
