<?php

declare(strict_types=1);

namespace OrienteeringManager\Model\Sitemap;

use DateTimeImmutable;
use OrienteeringManager\Config\Enum\SitemapChangeFrequencyEnum;

final class Url
{

    private string $loc;

    private ?DateTimeImmutable $lastmod = null;

    private ?SitemapChangeFrequencyEnum $changefreq = null;

    private float $priority;

    public function getLoc(): string
    {
        return $this->loc;
    }

    public function setLoc(string $loc): self
    {
        $this->loc = $loc;

        return $this;
    }

    public function getLastmod(): ?DateTimeImmutable
    {
        return $this->lastmod;
    }

    public function setLastmod(?DateTimeImmutable $lastmod): self
    {
        $this->lastmod = $lastmod;

        return $this;
    }

    public function getChangefreq(): SitemapChangeFrequencyEnum
    {
        return $this->changefreq;
    }

    public function setChangefreq(SitemapChangeFrequencyEnum $changefreq): self
    {
        $this->changefreq = $changefreq;

        return $this;
    }

    public function getPriority(): float
    {
        return $this->priority;
    }

    public function setPriority(float $priority): self
    {
        $this->priority = $priority;

        return $this;
    }
}
