<?php

declare(strict_types=1);

namespace OrienteeringManager\Processor\Uploadable;

final readonly class UploadableArrayProcessor implements UploadableProcessorInterface
{
    public function __construct(
        private UploadableProcessor $uploadableProcessor,
    ) {
    }

    public function supports(mixed $subject): bool
    {
        return is_array($subject) || $subject instanceof \Traversable;
    }

    /**
     * @param array $subject
     */
    public function process(mixed $subject): void
    {
        foreach ($subject as $element) {
            $this->uploadableProcessor->process($element);
        }
    }
}
