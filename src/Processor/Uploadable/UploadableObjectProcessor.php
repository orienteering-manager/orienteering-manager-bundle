<?php

declare(strict_types=1);

namespace OrienteeringManager\Processor\Uploadable;

use OrienteeringManager\Config\Attribute\Uploadable;
use OrienteeringManager\Config\Attribute\UploadableField;
use ReflectionClass;
use ReflectionException;

final readonly class UploadableObjectProcessor implements UploadableProcessorInterface
{
    public function __construct(
        private UploadableProcessor $uploadableProcessor,
    ) {
    }

    public function supports(mixed $subject): bool
    {
        return is_object($subject);
    }

    /**
     * @param object $subject
     * @throws ReflectionException
     */
    public function process(mixed $subject): void
    {
        $reflectionClass = new ReflectionClass($subject);
        $classAttributes = $reflectionClass->getAttributes(Uploadable::class);
        if (empty($classAttributes)) {
            return;
        }

        // Class has Uploadable Attribute, process each fields of this class.
        $properties = $reflectionClass->getProperties();
        foreach ($properties as $property) {
            // If property has Uploadable attribute, process subresource class.
            $propertyAttributes = $property->getAttributes(Uploadable::class);
            if (!empty($propertyAttributes)) {
                $this->uploadableProcessor->process($property->getValue($subject));
                continue;
            }

            // Property has UploadableField Attribute, manage File upload.
            $propertyAttributes = $property->getAttributes(UploadableField::class);
            if (null === $propertyAttribute = array_pop($propertyAttributes)) {
                continue;
            }

            $this->uploadableProcessor->uploadFile($subject, $reflectionClass, $property, $propertyAttribute);
        }
    }
}
