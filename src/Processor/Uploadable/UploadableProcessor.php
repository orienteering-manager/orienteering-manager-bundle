<?php

declare(strict_types=1);

namespace OrienteeringManager\Processor\Uploadable;

use OrienteeringManager\Manager\FileManager;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\PropertyAccess\Exception\InvalidPropertyPathException;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

final readonly class UploadableProcessor
{
    public function __construct(
        private FileManager $fileManager,
        #[TaggedIterator('orienteering_manager.uploadable_processor')]
        private iterable $uploadableProcessors,
    ) {
    }

    public function process(mixed $subresource): void
    {
        foreach ($this->uploadableProcessors as $uploadableProcessor) {
            if (!$uploadableProcessor->supports($subresource)) {
                continue;
            }
            
            $uploadableProcessor->process($subresource);

            return;
        }
    }

    /**
     * @throws ReflectionException
     * @throws UnexpectedTypeException
     */
    public function uploadFile(
        mixed $subject,
        ReflectionClass $reflectionClass,
        ReflectionProperty $reflectionProperty,
        ReflectionAttribute $reflectionAttribute,
    ): void {
        $config = $reflectionAttribute->getArguments();

        $file = $reflectionProperty->getValue($subject);
        if (null === $file) {
            if (is_string($config['deletionField'] ?? null) && true === $reflectionClass->getProperty($config['deletionField'])->getValue($subject)) {
                $this->fileManager->remove($reflectionClass->getProperty($config['urlField'])->getValue($subject));
                $reflectionClass->getProperty($config['urlField'])->setValue($subject, null);
            }

            return;
        }

        if (!$file instanceof UploadedFile) {
            throw new UnexpectedTypeException($file, UploadedFile::class);
        }

        $filename = $this->buildFilename(
            $subject,
            $reflectionClass,
            $config['nameProperties'] ?? [],
            $config['enableVersioning']
        );

        $path = $this->fileManager->upload(
            $file,
            $config['targetFolder'] ?? '',
            $filename,
            $reflectionClass->getProperty($config['urlField'])->getValue($subject),
        );

        $reflectionProperty->setValue($subject, null);
        $reflectionClass->getProperty($config['urlField'])->setValue($subject, $path);
    }

    /**
     * @throws ReflectionException
     */
    private function buildFilename(
        mixed $subject,
        ReflectionClass $reflectionClass,
        array|string $nameProperties,
        bool $enableVersioning
    ): string {
        if (is_string($nameProperties)) {
            $nameProperties = [$nameProperties];
        }

        $namePropertiesValues = [];
        foreach ($nameProperties as $nameProperty) {
            $namePropertiesValues[] = $this->retrievePropertyValue($subject, $reflectionClass, $nameProperty);
        }
        
        if ($enableVersioning) {
            $namePropertiesValues[] = uniqid();
        }

        $slugger = new AsciiSlugger();

        return $slugger->slug(implode('-', $namePropertiesValues))->lower()->toString();
    }

    /**
     * @throws ReflectionException
     */
    private function retrievePropertyValue(mixed $subject, ReflectionClass $reflectionClass, string $nameProperty): string
    {
        if (str_contains($nameProperty, '.')) {
            $namePropertyExploded = explode('.', $nameProperty);
            $firstNameProperty = array_shift($namePropertyExploded);
            if (!$reflectionClass->hasProperty($firstNameProperty)) {
                throw new InvalidPropertyPathException(sprintf('"%s" property does not exist.', $firstNameProperty));
            }

            $reflectionProperty = $reflectionClass->getProperty($firstNameProperty);

            return $this->retrievePropertyValue(
                $reflectionProperty->getValue($subject),
                new ReflectionClass($reflectionProperty->getValue($subject)),
                implode('.', $namePropertyExploded),
            );
        }

        if (!$reflectionClass->hasProperty($nameProperty)) {
            throw new InvalidPropertyPathException(sprintf('"%s" property does not exist.', $nameProperty));
        }

        return (string) $reflectionClass->getProperty($nameProperty)->getValue($subject);
    }
}
