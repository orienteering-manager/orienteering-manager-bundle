<?php

declare(strict_types=1);

namespace OrienteeringManager\Processor\Uploadable;

use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

#[AutoconfigureTag(name: 'orienteering_manager.uploadable_processor')]
interface UploadableProcessorInterface
{
    public function process(mixed $subject): void;
    
    public function supports(mixed $subject): bool;
}
