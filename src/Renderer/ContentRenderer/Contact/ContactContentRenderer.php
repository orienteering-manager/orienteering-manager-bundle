<?php

declare(strict_types=1);

namespace OrienteeringManager\Renderer\ContentRenderer\Contact;

use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Renderer\ContentRenderer\ContentRendererInterface;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

final readonly class ContactContentRenderer implements ContentRendererInterface
{
    public function __construct(
        private Environment $twig,
    ) {
    }

    public function supports(Request $request, Page $page, array $childIdentifiers): bool
    {
        return PageTypeEnum::CONTACT->value === $page->getType();
    }

    public function render(Request $request, Page $page, mixed $childIdentifiers): string
    {
        return $this->twig->render(
            '@OrienteeringManager/content/page/contact.html.twig',
            [
                'page' => $page,
            ]
        );
    }
}