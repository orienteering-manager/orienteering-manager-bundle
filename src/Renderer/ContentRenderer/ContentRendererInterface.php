<?php

declare(strict_types=1);

namespace OrienteeringManager\Renderer\ContentRenderer;

use OrienteeringManager\Entity\Page;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

#[AutoconfigureTag('orienteering_manager.content_renderer')]
interface ContentRendererInterface
{
    public function supports(Request $request, Page $page, array $childIdentifiers): bool;

    /**
     * @throws NotFoundHttpException
     */
    public function render(Request $request, Page $page, array $childIdentifiers): string;
}