<?php

declare(strict_types=1);

namespace OrienteeringManager\Renderer\ContentRenderer\Custom;

use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Renderer\ContentRenderer\ContentRendererInterface;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

final readonly class CustomContentRenderer implements ContentRendererInterface
{
    public function __construct(
        private Environment $twig,
    ) {
    }

    public function supports(Request $request, Page $page, array $childIdentifiers): bool
    {
        return PageTypeEnum::CUSTOM->value === $page->getType();
    }

    public function render(Request $request, Page $page, mixed $childIdentifiers): string
    {
        return $this->twig->render(
            '@OrienteeringManager/content/page/custom.html.twig',
            [
                'page' => $page,
            ]
        );
    }
}