<?php

declare(strict_types=1);

namespace OrienteeringManager\Renderer\ContentRenderer\Event;

use OrienteeringManager\Builder\FilterBuilder\EventApiFilterBuilder;
use OrienteeringManager\Config\Enum\ModeEnum;
use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Form\Model\OrienteeringManager\Filter\EventFilterType;
use OrienteeringManager\Logger\OrienteeringManagerLogger;
use OrienteeringManager\Model\OrienteeringManager\Collection\EventApiCollection;
use OrienteeringManager\Model\Page\Event\EventListModel;
use OrienteeringManager\Renderer\ContentRenderer\ContentRendererInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

final readonly class EventListContentRenderer implements ContentRendererInterface
{
    public function __construct(
        private Environment $twig,
        private FormFactoryInterface $formFactory,
        private OrienteeringManagerDataProvider $dataProvider,
        private EventApiFilterBuilder $eventApiFilterBuilder,
        private OrienteeringManagerLogger $logger,
    ) {
    }

    public function supports(Request $request, Page $page, array $childIdentifiers): bool
    {
        return PageTypeEnum::EVENT_LIST->value === $page->getType() && 0 === count($childIdentifiers);
    }

    public function render(Request $request, Page $page, mixed $childIdentifiers): string
    {
        $itemPerPages = 9;

        $pageModel = $page->getData();
        if ($pageModel instanceof EventListModel) {
            $itemPerPages = $pageModel->getItemPerPages();
        }

        $eventFilter = $this->eventApiFilterBuilder->buildFromRequest($request);
        $eventFilter->setMode(ModeEnum::VIEW);
        $eventFilter->setItemsPerPage($itemPerPages);

        $form = $this->formFactory->create(EventFilterType::class, $eventFilter);
        $form->handleRequest($request);

        $eventApiCollection = new EventApiCollection($eventFilter);

        $eventsCollection = null;
        try {
            $eventsCollection = $this->dataProvider->getCollection($eventApiCollection);
        } catch (OrienteeringManagerException $e) {
            $this->logger->addFlash('error', 'request.collection.event', [
                '%code%' => $e->getCode(),
                '%message%' => $e->getMessage(),
                '%iri%' => $eventApiCollection->getIri(),
            ]);
        }

        if ($request->headers->has('Turbo-Frame')) {
            return $this->twig->render('@OrienteeringManager/content/event/_list.html.twig', [
                'page' => $page,
                'events' => $eventsCollection?->getItems() ?? [],
                'currentPage' => $eventFilter->getPage() ?? 1,
                'maxPages' => ceil($eventsCollection?->getTotalItems() / $itemPerPages),
                'form' => $form->createView(),
                'query' => $eventFilter->getQuery(),
                'dateBefore' => $eventFilter->getDateBefore()?->format('Y-m-d'),
                'dateAfter' => $eventFilter->getDateAfter()?->format('Y-m-d'),
            ]);
        }

        return $this->twig->render(
            '@OrienteeringManager/content/event/list.html.twig',
            [
                'page' => $page,
                'events' => $eventsCollection?->getItems() ?? [],
                'currentPage' => $eventFilter->getPage() ?? 1,
                'maxPages' => ceil($eventsCollection?->getTotalItems() / $itemPerPages),
                'form' => $form->createView(),
                'query' => $eventFilter->getQuery(),
                'dateBefore' => $eventFilter->getDateBefore()?->format('Y-m-d'),
                'dateAfter' => $eventFilter->getDateAfter()?->format('Y-m-d'),
            ]
        );
    }
}