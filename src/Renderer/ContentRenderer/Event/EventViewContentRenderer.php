<?php

declare(strict_types=1);

namespace OrienteeringManager\Renderer\ContentRenderer\Event;

use OrienteeringManager\Builder\UxComponentBuilder\MapUxComponentBuilderInterface;
use OrienteeringManager\Config\Enum\ModeEnum;
use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Model\Breadcrumb\BreadcrumbModel;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventResultApiItem;
use OrienteeringManager\Renderer\ContentRenderer\ContentRendererInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;
use OrienteeringManager\Repository\PageRepository;

final readonly class EventViewContentRenderer implements ContentRendererInterface
{
    public function __construct(
        private Environment $twig,
        private OrienteeringManagerDataProvider $dataProvider,
        private MapUxComponentBuilderInterface $mapBuilder,
        private UrlGeneratorInterface $urlGenerator,
        private PageRepository $pageRepository,
    ) {
    }

    public function supports(Request $request, Page $page, array $childIdentifiers): bool
    {
        return PageTypeEnum::EVENT_LIST->value === $page->getType() && 1 === count($childIdentifiers);
    }

    public function render(Request $request, Page $page, mixed $childIdentifiers): string
    {
        /** @var EventApiItem $item */
        $item = $this->dataProvider->getItem(new EventApiItem(current($childIdentifiers)), ModeEnum::VIEW);

        $map = null;
        if (null !== $item->getLocation()?->getLatitude() && null !== $item->getLocation()?->getLongitude()) {
            $map = $this->mapBuilder->build($item->getLocation());
        }

        if ($item->getResult() instanceof EventResultApiItem && (0 < \count($item->getResult()?->getFiles()) || $item->getResult()?->isLive())) {
            $pageEventResultList = $this->pageRepository->findOneBy(['type' => PageTypeEnum::EVENT_RESULT_LIST]);
        }

        return $this->twig->render(
            '@OrienteeringManager/content/event/view.html.twig',
            [
                'page' => $page,
                'pageEventResultList' => $pageEventResultList ?? null,
                'breadcrumb' => $this->getBreadcrumb($page, $item),
                'item' => $item,
                'map' => $map,
            ]
        );
    }

    private function getBreadcrumb(Page $page, ?EventApiItem $item = null): BreadcrumbModel
    {
        $breadcrumb = $page->getBreadcrumb();

        if (null === $item) {
            return $breadcrumb;
        }

        $breadcrumb->addItem($item->getName(), $this->urlGenerator->generate('app_content_view', [
            'page' => $page->getSlug(),
            'slug' => $item->getSlug(),
            'id' => $item->getId(),
        ]), true);

        return $breadcrumb;
    }
}