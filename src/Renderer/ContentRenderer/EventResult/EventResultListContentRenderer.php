<?php

declare(strict_types=1);

namespace OrienteeringManager\Renderer\ContentRenderer\EventResult;

use OrienteeringManager\Builder\FilterBuilder\EventResultApiFilterBuilder;
use OrienteeringManager\Config\Enum\ModeEnum;
use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Logger\OrienteeringManagerLogger;
use OrienteeringManager\Model\OrienteeringManager\Collection\EventResultApiCollection;
use OrienteeringManager\Model\Page\EventResult\EventResultListModel;
use OrienteeringManager\Renderer\ContentRenderer\ContentRendererInterface;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

final readonly class EventResultListContentRenderer implements ContentRendererInterface
{
    public function __construct(
        private Environment $twig,
        private OrienteeringManagerDataProvider $dataProvider,
        private OrienteeringManagerLogger $logger,
        private EventResultApiFilterBuilder $eventResultApiFilterBuilder,
    ) {
    }

    public function supports(Request $request, Page $page, array $childIdentifiers): bool
    {
        return PageTypeEnum::EVENT_RESULT_LIST->value === $page->getType() && 0 === count($childIdentifiers);
    }

    public function render(Request $request, Page $page, mixed $childIdentifiers): string
    {
        $itemPerPages = 9;

        $pageModel = $page->getData();
        if ($pageModel instanceof EventResultListModel) {
            $itemPerPages = $pageModel->getItemPerPages();
        }

        $eventResultFilter = $this->eventResultApiFilterBuilder->buildFromRequest($request);
        $eventResultFilter->setMode(ModeEnum::VIEW);
        $eventResultFilter->setItemsPerPage($itemPerPages);
        $eventResultFilter->setFilesExists(true);

        $eventResultApiCollection = new EventResultApiCollection($eventResultFilter);

        try {
            $eventResultsCollection = $this->dataProvider->getCollection($eventResultApiCollection);
        } catch (OrienteeringManagerException $e) {
            $this->logger->addFlash('error', 'request.collection.event_result', [
                '%code%' => $e->getCode(),
                '%message%' => $e->getMessage(),
                '%iri%' => $eventResultApiCollection->getIri(),
            ]);
        }

        return $this->twig->render(
            '@OrienteeringManager/content/event_result/list.html.twig',
            [
                'page' => $page,
                'results' => $eventResultsCollection?->getItems() ?? [],
                'currentPage' => $eventResultFilter->getPage() ?? 1,
                'maxPages' => ceil($eventResultsCollection?->getTotalItems() / $itemPerPages),
            ]
        );
    }
}