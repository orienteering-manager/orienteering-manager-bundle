<?php

declare(strict_types=1);

namespace OrienteeringManager\Renderer\ContentRenderer\EventResult;

use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Model\Breadcrumb\BreadcrumbModel;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventResultApiItem;
use OrienteeringManager\Renderer\ContentRenderer\ContentRendererInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

final readonly class EventResultViewContentRenderer implements ContentRendererInterface
{
    public function __construct(
        private Environment $twig,
        private OrienteeringManagerDataProvider $dataProvider,
        private UrlGeneratorInterface $urlGenerator,
    ) {
    }

    public function supports(Request $request, Page $page, array $childIdentifiers): bool
    {
        return PageTypeEnum::EVENT_RESULT_LIST->value === $page->getType() && 1 === count($childIdentifiers);
    }

    protected function getBreadcrumb(Page $page, ?EventApiItem $item = null): BreadcrumbModel
    {
        $breadcrumb = $page->getBreadcrumb();

        if (null === $item) {
            return $breadcrumb;
        }

        $breadcrumb->addItem($item->getName(), $this->urlGenerator->generate('app_content_view', [
            'page' => $page->getSlug(),
            'slug' => $item->getSlug(),
            'id' => $item->getId(),
        ]), true);

        return $breadcrumb;
    }

    public function render(Request $request, Page $page, mixed $childIdentifiers): string
    {
        /** @var EventApiItem $event */
        $event = $this->dataProvider->getItem(new EventApiItem(end($childIdentifiers)));
        if (!($eventResult = $event->getResult()) instanceof EventResultApiItem) {
            throw new NotFoundHttpException();
        }

        return $this->twig->render(
            '@OrienteeringManager/content/event_result/view.html.twig',
            [
                'page' => $page,
                'breadcrumb' => $this->getBreadcrumb($page, $event),
                'item' => $eventResult->setEvent($event),
            ]
        );
    }
}