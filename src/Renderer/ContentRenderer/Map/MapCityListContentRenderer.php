<?php

declare(strict_types=1);

namespace OrienteeringManager\Renderer\ContentRenderer\Map;

use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Logger\OrienteeringManagerLogger;
use OrienteeringManager\Model\OrienteeringManager\Collection\MapApiCollection;
use OrienteeringManager\Model\OrienteeringManager\Filter\MapApiFilter;
use OrienteeringManager\Model\OrienteeringManager\Item\MapApiItem;
use OrienteeringManager\Renderer\ContentRenderer\ContentRendererInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Twig\Environment;

final readonly class MapCityListContentRenderer implements ContentRendererInterface
{
    public function __construct(
        private Environment $twig,
        private OrienteeringManagerDataProvider $dataProvider,
        private OrienteeringManagerLogger $logger,
    ) {
    }

    public function supports(Request $request, Page $page, array $childIdentifiers): bool
    {
        return PageTypeEnum::MAP_LIST->value === $page->getType() && 1 === count($childIdentifiers);
    }

    public function render(Request $request, Page $page, array $childIdentifiers): string
    {
        $mapApiFilter = new MapApiFilter();
        $mapApiFilter->setCity(end($childIdentifiers));

        $mapApiCollection = new MapApiCollection($mapApiFilter);

        try {
            /** @var MapApiItem[] $maps */
            $maps = $this->dataProvider->getCollection($mapApiCollection)->getItems();
        } catch (OrienteeringManagerException $e) {
            $this->logger->addFlash('error', 'request.collection.map', [
                '%code%' => $e->getCode(),
                '%message%' => $e->getMessage(),
                '%iri%' => $mapApiCollection->getIri(),
            ]);
        }

        $firstMap = current($maps);
        if (!$firstMap instanceof MapApiItem) {
            throw new NotFoundHttpException();
        }

        $breadcrumb = $page->getBreadcrumb();
        $breadcrumb->addItem($firstMap->getCity(), active: true);

        return $this->twig->render(
            '@OrienteeringManager/content/map/list_by_city.html.twig',
            [
                'page' => $page,
                'breadcrumb' => $breadcrumb,
                'city' => $maps[0]->getCity(),
                'maps' => $maps,
            ]
        );
    }
}