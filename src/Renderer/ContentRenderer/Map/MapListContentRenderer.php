<?php

declare(strict_types=1);

namespace OrienteeringManager\Renderer\ContentRenderer\Map;

use OrienteeringManager\Builder\UxComponentBuilder\MapUxComponentBuilder;
use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Exception\OrienteeringManagerException;
use OrienteeringManager\Logger\OrienteeringManagerLogger;
use OrienteeringManager\Model\OrienteeringManager\Collection\MapApiCollection;
use OrienteeringManager\Model\OrienteeringManager\Filter\MapApiFilter;
use OrienteeringManager\Model\OrienteeringManager\Item\MapApiItem;
use OrienteeringManager\Renderer\ContentRenderer\ContentRendererInterface;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

final readonly class MapListContentRenderer implements ContentRendererInterface
{
    public function __construct(
        private Environment $twig,
        private OrienteeringManagerDataProvider $dataProvider,
        private OrienteeringManagerLogger $logger,
        private MapUxComponentBuilder $mapUxComponentBuilder,
    ) {
    }

    public function supports(Request $request, Page $page, array $childIdentifiers): bool
    {
        return PageTypeEnum::MAP_LIST->value === $page->getType() && 0 === count($childIdentifiers);
    }

    public function render(Request $request, Page $page, array $childIdentifiers): string
    {
        $mapApiCollection = new MapApiCollection(new MapApiFilter());

        try {
            /** @var MapApiItem[] $mapCollection */
            $items = $this->dataProvider->getCollection($mapApiCollection)->getItems();

            $maps = [];
            foreach ($items as $map) {
                $maps[$map->getCity()][] = $map;
            }
        } catch (OrienteeringManagerException $e) {
            $this->logger->addFlash('error', 'request.collection.map', [
                '%code%' => $e->getCode(),
                '%message%' => $e->getMessage(),
                '%iri%' => $mapApiCollection->getIri(),
            ]);
        }

        return $this->twig->render(
            '@OrienteeringManager/content/map/list.html.twig',
            [
                'page' => $page,
                'map' => $this->mapUxComponentBuilder->build($items, $page),
                'cityMaps' => $maps ?? [],
            ]
        );
    }
}