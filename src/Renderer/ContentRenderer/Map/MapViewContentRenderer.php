<?php

declare(strict_types=1);

namespace OrienteeringManager\Renderer\ContentRenderer\Map;

use OrienteeringManager\Builder\UxComponentBuilder\MapUxComponentBuilderInterface;
use OrienteeringManager\Config\Enum\ModeEnum;
use OrienteeringManager\Config\Enum\PageTypeEnum;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Entity\Page;
use OrienteeringManager\Model\Breadcrumb\BreadcrumbModel;
use OrienteeringManager\Model\OrienteeringManager\Item\MapApiItem;
use OrienteeringManager\Renderer\ContentRenderer\ContentRendererInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

final readonly class MapViewContentRenderer implements ContentRendererInterface
{
    public function __construct(
        private Environment $twig,
        private OrienteeringManagerDataProvider $dataProvider,
        private MapUxComponentBuilderInterface $mapBuilder,
        private UrlGeneratorInterface $urlGenerator,
    ) {
    }

    public function supports(Request $request, Page $page, array $childIdentifiers): bool
    {
        return PageTypeEnum::MAP_LIST->value === $page->getType() && 2 === count($childIdentifiers);
    }

    public function render(Request $request, Page $page, mixed $childIdentifiers): string
    {
        /** @var MapApiItem $item */
        $item = $this->dataProvider->getItem(new MapApiItem(end($childIdentifiers)), ModeEnum::VIEW);

        $map = null;
        if (null !== $item->getLatitude() && null !== $item->getLongitude()) {
            $map = $this->mapBuilder->build($item);
        }

        return $this->twig->render(
            '@OrienteeringManager/content/map/view.html.twig',
            [
                'page' => $page,
                'breadcrumb' => $this->getBreadcrumb($page, $item),
                'item' => $item,
                'map' => $map,
            ]
        );
    }

    private function getBreadcrumb(Page $page, ?MapApiItem $item = null): BreadcrumbModel
    {
        $breadcrumb = $page->getBreadcrumb();

        if (null === $item) {
            return $breadcrumb;
        }

        $breadcrumb->addItem($item->getCity(), $this->urlGenerator->generate('app_content_category', [
            'page' => $page->getSlug(),
            'category' => $item->getCitySlug(),
        ]));

        $breadcrumb->addItem($item->getName(), $this->urlGenerator->generate('app_content_view', [
            'page' => $page->getSlug(),
            'slug' => $item->getSlug(),
            'id' => $item->getId(),
        ]), true);

        return $breadcrumb;
    }
}