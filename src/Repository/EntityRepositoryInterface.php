<?php

declare(strict_types=1);

namespace OrienteeringManager\Repository;

interface EntityRepositoryInterface
{
    public function save(mixed $entity, bool $flush = false): void;

    public function remove(mixed $entity, bool $flush = false): void;
}
