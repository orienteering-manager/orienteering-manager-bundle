<?php

declare(strict_types=1);

namespace OrienteeringManager\ScheduleProvider;

use OrienteeringManager\Message\SitemapUpdateMessage;
use Symfony\Component\Scheduler\Attribute\AsSchedule;
use Symfony\Component\Scheduler\RecurringMessage;
use Symfony\Component\Scheduler\Schedule;
use Symfony\Component\Scheduler\ScheduleProviderInterface;

#[AsSchedule('async')]
final readonly class SitemapUpdaterScheduleProvider implements ScheduleProviderInterface
{
    public function getSchedule(): Schedule
    {
        return (new Schedule())->add(
            RecurringMessage::every('2 hours', new SitemapUpdateMessage())
        );
    }
}
