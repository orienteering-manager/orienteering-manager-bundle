<?php

declare(strict_types=1);

namespace OrienteeringManager\Security;

use OrienteeringManager\Model\OrienteeringManager\Item\FederationMemberApiItem;
use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;

interface UserInterface extends BaseUserInterface
{
    public function getFederationMember(): ?FederationMemberApiItem;
    public function setFederationMember(?FederationMemberApiItem $federationMember): void;
}
