<?php

declare(strict_types=1);

namespace OrienteeringManager\Security\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;

abstract class AbstractVoter extends Voter
{
    final public const string EDIT = 'EDIT';
    final public const string DELETE = 'DELETE';
    final public const string REGISTER = 'REGISTER';

    public function __construct(
        private readonly string $entityClass,
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        return in_array($attribute, [self::EDIT, self::DELETE, self::REGISTER])
            && $subject instanceof $this->entityClass;
    }
}
