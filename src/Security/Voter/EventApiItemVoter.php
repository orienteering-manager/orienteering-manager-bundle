<?php

declare(strict_types=1);

namespace OrienteeringManager\Security\Voter;

use DateTime;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final class EventApiItemVoter extends AbstractVoter
{
    public function __construct() {
        parent::__construct(EventApiItem::class);
    }

    /**
     * @param EventApiItem $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::EDIT, self::DELETE => $user instanceof UserInterface && in_array('ROLE_ADMIN', $user->getRoles()),
            self::REGISTER => $subject->isAllowRegistration() && null === $subject->getStatus() && new DateTime() < $subject->getRegistrationLimitDate(),
            default => false,
        };

    }
}
