<?php

namespace OrienteeringManager\Twig;

use OrienteeringManager\Config\Constant\ColorConstant;
use OrienteeringManager\Model\OrienteeringManager\Item\ContentApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\SiteApiItem;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class OrienteeringManagerExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('contentMainColor', $this->getContentMainColor(...)),
            new TwigFunction('contentMediaUrl', $this->getContentMediaUrl(...)),
        ];
    }

    public function getContentMainColor(object $subject): string
    {
        return $subject instanceof ContentApiItem ?
            $subject->getItemColor()
            : ColorConstant::PRIMARY;
    }

    public function getContentMediaUrl(string $path, ?SiteApiItem $site): string
    {
        return !$site instanceof SiteApiItem || $site->isCurrent() ?
            $path
            : rtrim((string) $site->getUrl(), '/').$path;
    }
}