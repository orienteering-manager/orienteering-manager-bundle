<?php

namespace OrienteeringManager\Tests\Builder\FilterBuilder;

use OrienteeringManager\Builder\FilterBuilder\EventApiFilterBuilder;
use OrienteeringManager\Model\OrienteeringManager\Filter\EventApiFilter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class EventApiFilterBuilderTest extends TestCase
{
    private EventApiFilterBuilder $builder;

    protected function setUp(): void
    {
        parent::setUp();

        $this->builder = new EventApiFilterBuilder();
    }

    /**
     * @dataProvider buildFromRequestDataProvider
     */
    public function testBuildFromRequest(
        Request $request,
        EventApiFilter $eventApiFilterExpected,
    ): void {
        $eventApiFilter = $this->builder->buildFromRequest($request);

        $this->assertInstanceOf(EventApiFilter::class, $eventApiFilter);
        $this->assertEquals($eventApiFilterExpected, $eventApiFilter);
    }

    public function buildFromRequestDataProvider(): \Generator
    {
        $now = new \DateTime('now');
        $now->setTime(0, 0);

        $eventApiFilter = new EventApiFilter();
        $eventApiFilter->setItemsPerPage(EventApiFilter::DEFAULT_ITEMS_PER_PAGE);
        $eventApiFilter->setDateAfter($now);
        $eventApiFilter->setPage(1);
        $eventApiFilter->setPagination(true);

        yield 'Case with empty request query' => [
            'request' => new Request([]),
            'eventApiFilter' => $eventApiFilter,
        ];

        $eventApiFilter = new EventApiFilter();
        $eventApiFilter->setItemsPerPage(EventApiFilter::DEFAULT_ITEMS_PER_PAGE);
        $eventApiFilter->setDateAfter($now);
        $eventApiFilter->setPage(1);
        $eventApiFilter->setPagination(true);

        yield 'Case with page=1 request query only' => [
            'request' => new Request(['page' => 1]),
            'eventApiFilter' => $eventApiFilter,
        ];

        $eventApiFilter = new EventApiFilter();
        $eventApiFilter->setItemsPerPage(EventApiFilter::DEFAULT_ITEMS_PER_PAGE);
        $eventApiFilter->setDateAfter($now);
        $eventApiFilter->setPage(3);
        $eventApiFilter->setPagination(true);

        yield 'Case with page=3 request query only' => [
            'request' => new Request(['page' => 3]),
            'eventApiFilter' => $eventApiFilter,
        ];

        $eventApiFilter = new EventApiFilter();
        $eventApiFilter->setItemsPerPage(EventApiFilter::DEFAULT_ITEMS_PER_PAGE);
        $eventApiFilter->setDateBefore(new \DateTime('2025-01-15 00:00:00'));
        $eventApiFilter->setDateAfter(new \DateTime('2025-01-01 00:00:00'));
        $eventApiFilter->setPage(2);
        $eventApiFilter->setPagination(true);

        yield 'Case default' => [
            'request' => new Request([
                'dateBefore' => '2025-01-15',
                'dateAfter' => '2025-01-01',
                'page' => '2',
            ]),
            'eventApiFilter' => $eventApiFilter,
        ];
    }
}
