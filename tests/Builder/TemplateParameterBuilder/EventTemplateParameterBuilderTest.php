<?php

namespace OrienteeringManager\Tests\Builder\TemplateParameterBuilder;

use OrienteeringManager\Builder\TemplateParameterBuilder\EventTemplateParameterBuilder;
use OrienteeringManager\Builder\UxComponentBuilder\MapUxComponentBuilderInterface;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventLocationApiItem;
use PHPUnit\Framework\TestCase;
use Symfony\UX\Map\Map;

class EventTemplateParameterBuilderTest extends TestCase
{
    private EventTemplateParameterBuilder $builder;
    private MapUxComponentBuilderInterface $mapUxComponentBuilder;

    protected function setUp(): void
    {
        parent::setUp();

        $this->builder = new EventTemplateParameterBuilder(
            $this->mapUxComponentBuilder = $this->createMock(MapUxComponentBuilderInterface::class)
        );
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testBuild(
        EventApiItem $eventApiItem,
        ?Map $map,
        array $parametersExpected,
    ): void {
        if ($map instanceof Map) {
            $this->mapUxComponentBuilder
                ->expects($this->once())
                ->method('build')
                ->willReturn($map);
        } else {
            $this->mapUxComponentBuilder
                ->expects($this->never())
                ->method('build');
        }

        $parameters = $this->builder->build($eventApiItem);

        $this->assertSame($parametersExpected, $parameters);
    }

    public function buildDataProvider(): \Generator
    {
        yield 'Case with null location' => [
            'eventApiItem' => new EventApiItem(),
            'map' => null,
            'parameters' => [
                'map' => null,
            ],
        ];

        $eventLocationApiItem = new EventLocationApiItem();
        $eventLocationApiItem->setLongitude(1.5);

        $eventApiItem = new EventApiItem();
        $eventApiItem->setLocation($eventLocationApiItem);

        yield 'Case with latitude null' => [
            'eventApiItem' => $eventApiItem,
            'map' => null,
            'parameters' => [
                'map' => null,
            ],
        ];

        $eventLocationApiItem = new EventLocationApiItem();
        $eventLocationApiItem->setLatitude(1.5);

        $eventApiItem = new EventApiItem();
        $eventApiItem->setLocation($eventLocationApiItem);

        yield 'Case with longitude null' => [
            'eventApiItem' => $eventApiItem,
            'map' => null,
            'parameters' => [
                'map' => null,
            ],
        ];

        $eventLocationApiItem = new EventLocationApiItem();
        $eventLocationApiItem->setLatitude(1.5);
        $eventLocationApiItem->setLongitude(1.5);

        $eventApiItem = new EventApiItem();
        $eventApiItem->setLocation($eventLocationApiItem);

        $map = new Map();

        yield 'Case default' => [
            'eventApiItem' => $eventApiItem,
            'map' => $map,
            'parameters' => [
                'map' => $map,
            ],
        ];
    }
}
