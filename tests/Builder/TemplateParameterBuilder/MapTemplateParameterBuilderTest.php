<?php

namespace OrienteeringManager\Tests\Builder\TemplateParameterBuilder;

use OrienteeringManager\Builder\TemplateParameterBuilder\MapTemplateParameterBuilder;
use OrienteeringManager\Builder\UxComponentBuilder\MapUxComponentBuilderInterface;
use OrienteeringManager\Model\OrienteeringManager\Item\MapApiItem;
use PHPUnit\Framework\TestCase;
use Symfony\UX\Map\Map;

class MapTemplateParameterBuilderTest extends TestCase
{
    private MapTemplateParameterBuilder $builder;
    private MapUxComponentBuilderInterface $mapUxComponentBuilder;

    protected function setUp(): void
    {
        parent::setUp();

        $this->builder = new MapTemplateParameterBuilder(
            $this->mapUxComponentBuilder = $this->createMock(MapUxComponentBuilderInterface::class)
        );
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testBuild(
        MapApiItem $mapApiItem,
        ?Map $map,
        array $parametersExpected,
    ): void {
        if ($map instanceof Map) {
            $this->mapUxComponentBuilder
                ->expects($this->once())
                ->method('build')
                ->willReturn($map);
        } else {
            $this->mapUxComponentBuilder
                ->expects($this->never())
                ->method('build');
        }

        $parameters = $this->builder->build($mapApiItem);

        $this->assertSame($parametersExpected, $parameters);
    }

    public function buildDataProvider(): \Generator
    {
        yield 'Case with null location' => [
            'mapApiItem' => new MapApiItem(),
            'map' => null,
            'parameters' => [
                'map' => null,
            ],
        ];

        $mapApiItem = new MapApiItem();
        $mapApiItem->setLongitude(1.5);

        yield 'Case with latitude null' => [
            'mapApiItem' => $mapApiItem,
            'map' => null,
            'parameters' => [
                'map' => null,
            ],
        ];

        $mapApiItem = new MapApiItem();
        $mapApiItem->setLatitude(1.5);

        yield 'Case with longitude null' => [
            'mapApiItem' => $mapApiItem,
            'map' => null,
            'parameters' => [
                'map' => null,
            ],
        ];

        $mapApiItem = new MapApiItem();
        $mapApiItem->setLatitude(1.5);
        $mapApiItem->setLongitude(1.5);

        $map = new Map();

        yield 'Case default' => [
            'mapApiItem' => $mapApiItem,
            'map' => $map,
            'parameters' => [
                'map' => $map,
            ],
        ];
    }
}
