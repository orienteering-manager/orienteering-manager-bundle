<?php

namespace OrienteeringManager\Tests\Builder\UxComponentBuilder;

use OrienteeringManager\Builder\UxComponentBuilder\MapUxComponentBuilder;
use OrienteeringManager\Model\LocationInterface;
use PHPUnit\Framework\TestCase;
use Symfony\UX\Map\Map;

class MapUxComponentBuilderTest extends TestCase
{
    private MapUxComponentBuilder $builder;

    protected function setUp(): void
    {
        parent::setUp();

        $this->builder = new MapUxComponentBuilder();
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testBuild(
        ?LocationInterface $location,
    ): void {
        $map = $this->builder->build($location);

        $this->assertInstanceOf(Map::class, $map);
    }

    public function buildDataProvider(): \Generator
    {
        yield 'Case location is null' => [
            'location' => null,
        ];

        $location = $this->createMock(LocationInterface::class);
        $location->method('getLatitude')->willReturn(null);
        $location->method('getLongitude')->willReturn(null);

        yield 'Case with latitude and longitude null' => [
            'location' => $location,
        ];

        $location = $this->createMock(LocationInterface::class);
        $location->method('getLatitude')->willReturn(null);
        $location->method('getLongitude')->willReturn(1.2);

        yield 'Case with latitude null' => [
            'location' => $location,
        ];

        $location = $this->createMock(LocationInterface::class);
        $location->method('getLatitude')->willReturn(2.3);
        $location->method('getLongitude')->willReturn(1.8);

        yield 'Case with longitude null' => [
            'location' => $location,
        ];


        $location = $this->createMock(LocationInterface::class);
        $location->method('getLatitude')->willReturn(2.5);
        $location->method('getLongitude')->willReturn(-4.0);

        yield 'Case default' => [
            'location' => $location,
        ];
    }
}
